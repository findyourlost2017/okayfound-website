import { useState, useEffect } from "react";
import _ from "lodash";
import Head from "next/head";
import moment from "moment";
import { useRouter } from "next/router";
import Layout from "../src/components/layout";
import {
  CalendarHeader,
  CalendarContent,
  CalendarConfirmModal,
} from "../src/components/calendar";
import { getSession, useSession } from "next-auth/client";
import API from "../src/utils/api";

function CalendarScreen() {
  const router = useRouter();
  const [session, loading] = useSession();
  const [loadingPage, setLoadingPage] = useState(false);
  const [list, setList] = useState();
  const [cancelVisible, setCancelVisible] = useState();
  const [currentDate, setCurrentDate] = useState(moment().format("YYYY-MM-DD"));

  const deleteTimetable = async (id) => {
    const api = new API(session.token);
    const response = await api.deleteTimetable({ id });
    if (response && response.ok) {
      fetchTimetable(currentDate);
    }
  };

  const onDeleteConfirm = () => {
    deleteTimetable(cancelVisible);
    setCancelVisible();
  };

  const onChangeDate = (selected) => {
    setCurrentDate(selected);
  };

  const fetchTimetable = async (selectedDate) => {
    setLoadingPage(true);
    const api = new API(session.token);
    const response = await api.fetchTimetable({
      selectedDate,
    });
    setLoadingPage(false);
    if (response && response.ok) {
      setList(response.data.markedDates);
    }
  };

  useEffect(() => {
    if (!loading)
      if (!session || (session && !session.member)) {
        router.push("/");
      }
  }, [session, loading]);

  useEffect(() => {
    if (!loading) fetchTimetable(currentDate);
  }, [loading, currentDate]);

  return (
    <Layout session={session} hideFooter>
      <Head>
        <meta charSet="utf-8" />
        <title>睇樓行程 - 租屋台 香港免佣租屋平台</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="shortcut icon" href="/favicon.ico" />
      </Head>
      <CalendarHeader currentDate={currentDate} onChangeDate={onChangeDate} />
      <CalendarContent
        loading={loadingPage}
        list={list}
        currentDate={currentDate}
        setCancelVisible={setCancelVisible}
      />
      {cancelVisible && (
        <CalendarConfirmModal
          onDeleteConfirm={onDeleteConfirm}
          closeModal={() => setCancelVisible()}
        />
      )}
    </Layout>
  );
}

export default CalendarScreen;

export async function getServerSideProps(context) {
  const session = await getSession(context);

  if (!session) {
    context.res.writeHead(302, { Location: "/" });
    context.res.end();
    return { props: {} };
  }

  return {
    props: {},
  };
}
