import { useEffect } from "react";
import Head from "next/head";
import { getSession, useSession } from "next-auth/client";
import { useRouter } from "next/router";
import Layout from "../src/components/layout";
import { AccountInfo, AccountNavbar } from "../src/components/account";

const PageAccount = ({ session }) => {
  const router = useRouter();
  const [sessionClient, loading] = useSession();

  useEffect(() => {
    if (!loading)
      if (!sessionClient || (sessionClient && !sessionClient.member)) {
        router.push("/");
      }
  }, [sessionClient, loading]);

  return (
    <Layout session={session}>
      <Head>
        <meta charSet="utf-8" />
        <title>帳號資料 - 租屋台 香港免佣租屋平台</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="shortcut icon" href="/favicon.ico" />
      </Head>
      <div className="nc-CommonLayoutProps bg-neutral-50 dark:bg-neutral-900">
        <AccountNavbar />

        <div className="container pt-14 sm:pt-20 pb-24 lg:pb-32">
          <AccountInfo member={session?.member} token={session?.token} />
        </div>
      </div>
    </Layout>
  );
};

export default PageAccount;

export async function getServerSideProps(context) {
  const session = await getSession(context);

  if (!session) {
    context.res.writeHead(302, { Location: "/" });
    context.res.end();
    return { props: {} };
  }

  return {
    props: { session },
  };
}
