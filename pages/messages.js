import { useEffect, useState, useRef } from "react";
import Head from "next/head";
import { v4 as uuidv4 } from "uuid";
import _ from "lodash";
import moment from "moment";
import { toast } from "react-toastify";
import { getSession, useSession } from "next-auth/client";
import { useRouter } from "next/router";
import NotificationMessage from "../src/components/common/notification-message";
import resizeFile from "../src/utils/resizeFile";
import Layout from "../src/components/layout";
import { MessagesList, MessagesContent } from "../src/components/messages";
import appConfig from "../src/utils/appConfig";
import API from "../src/utils/api";

const MessagesPage = ({ selectedChat, session }) => {
  const router = useRouter();
  const [page, setPage] = useState(1);
  const [list, setList] = useState([]);
  const [loadingPage, setLoadingPage] = useState(true);
  const [messages, setMessages] = useState([]);
  const [sessionClient, loading] = useSession();
  const [currentChat, setCurrentChat] = useState(selectedChat);
  const [timetable, setTimetable] = useState(null);
  const [input, setInput] = useState("");
  const inputRef = useRef();
  const scrollContentRef = useRef();

  //   reset chat badge to server
  const resetChatBadge = async () => {
    const api = new API(session.token);
    await api.resetChatBadge({ friend_id: currentChat.friend_id });
  };

  //   get friend list (left)
  const fetchFriendList = async (page) => {
    setLoadingPage(true);
    const api = new API(session?.token);
    const response = await api.fetchFriendList({
      page,
    });
    setLoadingPage(false);
    if (response && response.ok) {
      if (page === 1) setList(response.data.list ? response.data.list : []);
      else if (response.data.list) {
        setList(list.concat(response.data.list));
      } else if (page === 1 && !response.data.list) setList([]);
    }
  };

  //   get chat message (right)
  const fetchChatMessage = async (propertyID) => {
    setTimetable();
    const api = new API(session.token);
    const response = await api.fetchChatMessage({
      property_id: propertyID,
    });
    if (response && response.ok) {
      setMessages(response.data.messages ? response.data.messages : []);
      setTimetable(response.data.timetable);
    }
  };

  //   send chat message for text
  const sendMessageText = async (input, messageID) => {
    const api = new API(session.token);
    const response = await api.sendChatMessage({
      _id: messageID,
      user: {
        _id: session.member.member_id,
        member_name: session.member.member_name,
      },
      friend_id: currentChat.friend_id,
      text: input,
      message_type: appConfig.MESSAGE_TYPE.text,
      sender_id: session.member.member_id,
      property_id: currentChat.property_id,
      property_title: currentChat.property.title,
      thumbnail: currentChat.property.thumbnail,
      receiver_id: `${currentChat.other_member_id}`,
      created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
    });
    if (response && response.ok) {
      setMessages((state) =>
        state.map((item) => {
          if (item._id === messageID) {
            item.loading = false;
          }
          return { ...item };
        }),
      );
    } else {
      toast.warn("網絡出現錯誤，無法傳送訊息", {
        position: "top-center",
      });
      setMessages((state) => _.filter(state, (item) => item._id != messageID));
    }
  };
  //   send chat message for photo
  const sendMessagePhoto = async (file, messageID) => {
    const api = new API(session.token);
    const formData = new FormData();

    const image = await resizeFile(file);
    formData.append("file", image);

    const fileData = {
      _id: messageID,
      user: {
        _id: session.member.member_id,
        member_name: session.member.member_name,
      },
      friend_id: currentChat.friend_id,
      text: null,
      message_type: appConfig.MESSAGE_TYPE.image,
      sender_id: session.member.member_id,
      property_id: currentChat.property_id,
      property_title: currentChat.property.title,
      thumbnail: currentChat.property.thumbnail,
      receiver_id: `${currentChat.other_member_id}`,
      created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
    };

    for (const k in fileData) {
      if (k === "user") {
        formData.append(k, JSON.stringify(fileData[k]));
      } else formData.append(k, fileData[k] ? fileData[k] : null);
    }

    const response = await api.sendChatFile(formData);
    if (response && response.ok) {
      setMessages((state) =>
        state.map((item) => {
          if (item._id === messageID) {
            item.loading = false;
            item.text = response.data.photo_url;
          }
          return { ...item };
        }),
      );
    } else {
      toast.warn("網絡出現錯誤，無法傳送訊息", {
        position: "top-center",
      });
      setMessages((state) => _.filter(state, (item) => item._id != messageID));
    }
  };

  //   send chat message for both
  const onSendMessage = (input, messageType) => {
    const messageID = uuidv4();
    const data = {
      _id: messageID,
      text: messageType === appConfig.MESSAGE_TYPE.text && input,
      loading: true,
      message_type: messageType,
      created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
      user: {
        _id: session.member.member_id,
        member_name: session.member.member_name,
      },
      sender_id: session.member.member_id,
    };

    setList((state) =>
      state.map((item) => {
        if (item.friend_id === currentChat.friend_id) {
          item.message = data;
        }
        return { ...item };
      }),
    );

    setMessages((state) => [...state, data]);
    if (messageType === appConfig.MESSAGE_TYPE.text) {
      sendMessageText(input, messageID);
    } else if (messageType === appConfig.MESSAGE_TYPE.image) {
      sendMessagePhoto(input, messageID);
    }
  };

  //   when click friend item
  const onShowMessageContent = (selected) => {
    setCurrentChat(selected);
    setList((state) =>
      state.map((item) => {
        if (item.friend_id === selected.friend_id) {
          item.unread_count = 0;
        }
        return { ...item };
      }),
    );
  };

  //   socket receive new message
  const onReceiveMessage = (newMessage) => {
    setList((state) =>
      state.map((item) => {
        if (item.friend_id === newMessage.friend_id) {
          item.message = newMessage;
          item.unread_count += 1;
        }
        return { ...item };
      }),
    );

    setCurrentChat((state) => {
      if (!state || (state && state.friend_id !== newMessage.friend_id)) {
        toast(<NotificationMessage item={newMessage} />, {
          toastId: newMessage._id,
          closeButton: true,
          hideProgressBar: true,
        });
      }

      return state;
    });
  };

  //   fetch more friend
  const onShowMoreFriend = () => {
    setPage(page + 1);
    fetchFriendList(page + 1);
  };

  const onAddTimetable = async (date) => {
    const api = new API(session.token);
    const response = await api.submitTimetable({
      date,
      friend_id: currentChat.friend_id,
      property_id: currentChat.property_id,
    });

    if (response && response.ok) {
      router.push(`/calendar`);
    }
  };

  //   init call when click friend
  useEffect(() => {
    if (currentChat) {
      fetchChatMessage(currentChat.property_id);
      resetChatBadge();
    }

    return () => currentChat && resetChatBadge();
  }, [currentChat]);

  useEffect(() => {
    scrollContentRef?.current?.scrollTo({
      top: scrollContentRef.current.scrollHeight,
      behavior: "smooth",
    });
  }, [messages]);

  useEffect(() => {
    if (!loading)
      if (!sessionClient || (sessionClient && !sessionClient.member)) {
        router.push("/");
      }
  }, [sessionClient, loading]);

  // call server when page init
  useEffect(() => {
    if (!loading) {
      document.body.classList.add("overflow-hidden");
      fetchFriendList(1);
      return () => document.body.classList.remove("overflow-hidden");
    }
  }, [loading]);

  return (
    <Layout session={session} hideFooter onReceiveMessage={onReceiveMessage}>
      <Head>
        <meta charSet="utf-8" />
        <title>聊天界面 - Okayfound 租屋台 香港免佣租屋平台</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="shortcut icon" href="/favicon.ico" />
      </Head>
      <div className="flex h-[calc(100vh-102px)]">
        {session?.member && (
          <MessagesList
            list={list}
            loading={loadingPage}
            member={session?.member}
            currentChat={currentChat}
            setCurrentChat={setCurrentChat}
            onShowMoreFriend={onShowMoreFriend}
            onShowMessageContent={onShowMessageContent}
          />
        )}
        <div
          className={`flex w-full md:w-6/12 lg:w-3/5 xl:w-2/3 bg-white ${
            !currentChat && "hidden md:block"
          }`}
        >
          {currentChat && session?.member && (
            <MessagesContent
              inputRef={inputRef}
              member={session?.member}
              currentChat={currentChat}
              timetable={timetable}
              input={input}
              setInput={setInput}
              messages={messages}
              fetchChatMessage={fetchChatMessage}
              onSendMessage={onSendMessage}
              scrollContentRef={scrollContentRef}
              onAddTimetable={onAddTimetable}
            />
          )}
        </div>
      </div>
    </Layout>
  );
};

export default MessagesPage;

export async function getServerSideProps(context) {
  const session = await getSession(context);

  if (!session) {
    context.res.writeHead(302, { Location: "/" });
    context.res.end();
    return { props: {} };
  }

  let selectedChat = null;
  const api = new API(session && session.token);
  const response =
    context.query.property_id &&
    (await api.fetchChatMessage({
      property_id: context.query.property_id,
    }));

  if (response && response.ok) {
    selectedChat = {
      friend_id: response.data.friendship.friend_id,
      property_id: context.query.property_id,
      property: response.data.property,
      other_member_id: response.data.friendship.other_member_id,
    };
  }

  return {
    props: { session, selectedChat },
  };
}
