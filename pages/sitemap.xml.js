import _ from "lodash";

function generateSiteMap(property) {
  return `<?xml version="1.0" encoding="UTF-8"?>
   <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
     <url>
       <loc>https://www.okayfound.com/agreement</loc>
       <changefreq>monthly</changefreq>
       <priority>0.9</priority>
     </url>
     <url>
       <loc>https://www.okayfound.com/list?property_type=0</loc>
       <changefreq>weekly</changefreq>
       <priority>0.9</priority>
     </url>
      <url>
       <loc>https://www.okayfound.com/list?property_type=2</loc>
       <changefreq>weekly</changefreq>
       <priority>0.9</priority>
     </url>
      <url>
       <loc>https://www.okayfound.com/about</loc>
       <changefreq>monthly</changefreq>
       <priority>0.9</priority>
     </url>
    ${property
      ?.map(({ property_id }) => {
        return `
       <url>
           <loc>${`https://www.okayfound.com/property/${property_id}`}</loc>
           <changefreq>weekly</changefreq>
       </url>
     `;
      })
      .join("")}
     </urlset>
 `;
}

function SiteMap() {
  // getServerSideProps will do the heavy lifting
}

export async function getServerSideProps({ res }) {
  const request = await fetch(
    "https://api.okayfound.com/property/fetchPropertySimple",
  );
  const result = await request.json();

  // We generate the XML sitemap with the posts data
  const sitemap = generateSiteMap(result?.list);

  res.setHeader("Content-Type", "text/xml");
  // we send the XML to the browser
  res.write(sitemap);
  res.end();

  return {
    props: {},
  };
}

export default SiteMap;
