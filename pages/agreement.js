import { useState } from "react";
import Head from "next/head";
import { useSession } from "next-auth/client";
import Layout from "../src/components/layout";
import {
  AgreementStepper,
  AgreementContent,
  AgreementFooter,
} from "../src/components/agreement";

function Agreement() {
  const [currentStep, setCurrentStep] = useState(0);
  const [type, setType] = useState(1);
  const [formData, setFormData] = useState();
  const [session] = useSession();

  const submitAgreement = async (event) => {
    event.preventDefault();
    const keys = {};
    for (const key of event.target.elements) {
      keys[key.name] = key.value;
    }
    setFormData(keys);
  };

  return (
    <Layout session={session}>
      <Head>
        <meta charSet="utf-8" />
        <title>下載租約 | 租屋台</title>

        <meta property="og:title" content="下載租約 | 租屋台" key="ogtitle" />
        <meta
          property="og:site_name"
          content={"下載租約 | 租屋台"}
          key="ogsitename"
        />

        <meta
          name="description"
          content="租屋台為你免費建立一個專業租約樣本, 包括車位租約, 住宅租約。所有格式均為PDF版本可以免費下載"
          key="desc"
        />
        <meta
          name="og:description"
          property="og:description"
          content="租屋台為你免費建立一個專業租約樣本, 包括車位租約, 住宅租約。所有格式均為PDF版本可以免費下載"
          key="ogdesc"
        />

        <meta property="og:image" content="/images/icon.png" />
        <meta property="og:locale" content="zh_HK" />

        <link rel="icon" href="/favicon.ico" />
        <link rel="shortcut icon" href="/favicon.ico" />
      </Head>
      <div className="nc-CheckOutPage" data-nc-id="CheckOutPage">
        <main className="container mt-11 mb-24 lg:mb-32 flex flex-col-reverse lg:flex-row">
          <div className="w-full lg:pr-10 ">
            <div className="w-full flex flex-col sm:rounded-2xl sm:border border-neutral-200 dark:border-neutral-700 space-y-8 px-0 sm:p-6 xl:p-8">
              <h2 className="text-3xl lg:text-4xl font-semibold">租約範本</h2>
              <div className="border-b border-neutral-200 dark:border-neutral-700" />

              {/* Stepper */}
              <form onSubmit={submitAgreement}>
                <div className="p-5">
                  <AgreementStepper currentStep={currentStep} />
                  <AgreementContent
                    currentStep={currentStep}
                    type={type}
                    setType={setType}
                    formData={formData}
                  />
                  <AgreementFooter
                    currentStep={currentStep}
                    setCurrentStep={setCurrentStep}
                  />
                </div>
              </form>
            </div>
          </div>
        </main>
      </div>
    </Layout>
  );
}

export default Agreement;
