import { getSession } from "next-auth/client";
import Layout from "../src/components/layout";
import { AccountListing, AccountNavbar } from "../src/components/account";

const PageMyListing = ({ session }) => {
  return (
    <Layout session={session}>
      <div className="nc-CommonLayoutProps bg-neutral-50 dark:bg-neutral-900">
        <AccountNavbar />

        <div className="container pt-14 sm:pt-20 pb-24 lg:pb-32">
          <AccountListing token={session.token} member={session.member} />
        </div>
      </div>
    </Layout>
  );
};

export default PageMyListing;

export async function getServerSideProps(context) {
  const session = await getSession(context);

  if (!session) {
    context.res.writeHead(302, { Location: "/" });
    context.res.end();
    return { props: {} };
  }

  return {
    props: { session },
  };
}
