import { getSession } from "next-auth/client";
import Layout from "../src/components/layout";

const PrivacyScreen = ({ session }) => {
  return (
    <Layout session={session}>
      <div className="nc-PageSingle pt-8 pb-8 lg:pb-16 lg:pt-16 ">
        <div className="max-w-screen-md mx-auto space-y-5">
          <span className="block text-base text-neutral-500 md:text-lg dark:text-neutral-400 pb-1">
            本公司承諾遵守《個人資料(私隱)條例》(下稱&quot;條例&quot;)的責任及要求，屬下員工亦必須對本公司所收集及/或儲存及/或使用的所有個人資料保密，並致力妥善保存和遵照條例規定的責任及要求。
            <br />
            <br />
            <span className="font-bold">收集個人資料之目的</span>
            <br />
            <br />
            本公司收集個人資料後，只用於與本程式資料有直接關係的用途。閣下如希望收取這些資料，請提供個人資料。
            <br />
            <br />
            <span className="font-bold">個人資料種類</span>
            <br />
            <br />
            本公司收集的個人資料帳戶在開立帳號，需向本公司提供的資料但不限於下列各項：
            <br />
            1. 閣下的姓名
            <br />
            2. 聯絡電話/電郵地址等資料 <br />
            3. 閣下的性別/年齡 <br />
            4. 閣下對於分租服務的喜好個人資料
            <br />
            <br />
            <span className="font-bold">個人資料的保留</span>
            <br />
            <br />
            本公司會根據法例於收集個人資料最少保存5年後，銷毀所有的個人資料，或得到閣下訂明同意繼續使用外，本公司只會限於其需要有關資料的時間內保留個人資料。
            <br />
            <br />
            <span className="font-bold">查詢及更正個人資料</span>
            <br />
            <br />
            根據條例，除當中載明的例外情況之外，各顧客有權作出以下事項：
            <br />
            1. 查核本公司是否持有關於自己的個人資料，並且取得該等資料的副本
            <br />
            2. 要求本公司更正關於自己的不準確個人資料
            <br />
            3.
            確知本公司關於個人資料的政策和常規，並且獲告知本公司持有的個人資料性質
            <br /> <br />
            根據條例之規定，本公司有權為受理資料查核要求而收取合理費用
          </span>
        </div>
      </div>
    </Layout>
  );
};

export default PrivacyScreen;

export async function getServerSideProps(context) {
  const session = await getSession(context);

  return {
    props: { session },
  };
}
