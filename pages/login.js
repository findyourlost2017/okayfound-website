import Head from "next/head";
import { signin, getSession } from "next-auth/client";
import Layout from "../src/components/layout";

const PageLogin = ({ session }) => {
  return (
    <Layout session={session}>
      <Head>
        <meta charSet="utf-8" />
        <title>登入 - Okayfound 租屋台 香港免佣租屋平台</title>
        <link rel="icon" href={"./images/icon.png"} />
      </Head>
      <div className="nc-PageLogin" data-nc-id="PageLogin">
        <div className="container mb-24 lg:mb-32">
          <h2 className="my-20 flex items-center text-3xl leading-[115%] md:text-5xl md:leading-[115%] font-semibold text-neutral-900 dark:text-neutral-100 justify-center">
            登入
          </h2>
          <div className="max-w-md mx-auto space-y-6">
            <div className="grid gap-3">
              {/* Facebook */}
              <a
                onClick={() => signin("facebook")}
                className="cursor-pointer nc-will-change-transform flex w-full rounded-lg bg-primary-50 dark:bg-neutral-800 px-4 py-3 transform transition-transform sm:px-6 hover:translate-y-[-2px]"
              >
                <img src="./images/Facebook.svg" />
                <h3 className="flex-grow text-center text-sm font-medium text-neutral-700 dark:text-neutral-300 sm:text-sm">
                  使用Facebook帳號登入
                </h3>
              </a>
              {/* Google */}
              <a
                onClick={() => signin("google")}
                className="cursor-pointer nc-will-change-transform flex w-full rounded-lg bg-orange-50 dark:bg-neutral-800 px-4 py-3 transform transition-transform sm:px-6 hover:translate-y-[-2px]"
              >
                <img src="./images/Google.svg" />
                <h3 className="flex-grow text-center text-sm font-medium text-neutral-700 dark:text-neutral-300 sm:text-sm">
                  使用Google帳號登入
                </h3>
              </a>
              {/* Apple */}
              {/* <a
                onClick={() => signin("apple")}
                className="cursor-pointer nc-will-change-transform flex w-full rounded-lg bg-fuchsia-50 dark:bg-neutral-800 px-4 py-3 transform transition-transform sm:px-6 hover:translate-y-[-2px]"
              >
                <img src="./images/Apple.svg" />
                <h3 className="flex-grow text-center text-sm font-medium text-neutral-700 dark:text-neutral-300 sm:text-sm">
                  使用Apple帳號登入
                </h3>
              </a> */}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default PageLogin;

export async function getServerSideProps(context) {
  const session = await getSession(context);

  return {
    props: { session },
  };
}
