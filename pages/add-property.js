import { useState, useEffect } from "react";
import { getSession, useSession } from "next-auth/client";
import { toast } from "react-toastify";
import moment from "moment";
import { useRouter } from "next/router";
import Layout from "../src/components/layout";
import {
  AddPropertyType,
  AddPropertyArea,
  AddPropertyParking,
  AddPropertyFacility,
  AddPropertyPhoto,
  AddPropertyPlan,
  AddPropertyLocation,
  AddPropertyTitle,
  PageAddPropertyPreview,
} from "../src/components/add-property";
import ButtonSecondary from "../src/components/button/button-secondary";
import ButtonPrimary from "../src/components/button/button-primary";
import appConfig from "../src/utils/appConfig";
import API from "../src/utils/api";
import resizeFile from "../src/utils/resizeFile";

const AddPropertyPage = ({ data }) => {
  const router = useRouter();
  const [session, loadingSession] = useSession();
  const [activeTab, setActiveTab] = useState(1);
  const [propertyID, setPropertyID] = useState(
    data?.property_id ? data?.property_id : null,
  );
  const [propertyType, setPropertyType] = useState(
    data?.property_type ? data.property_type : appConfig.PROPERTY_TYPE["住宅"],
  );
  const [propertyCommission, setPropertyCommission] = useState(
    data?.is_commission ? data.is_commission : "N",
  );
  const [minimalStart, setMinimalStart] = useState(
    data?.minimal_start
      ? moment(data?.minimal_start).format("YYYY-MM-DD")
      : moment().format("YYYY-MM-DD"),
  );
  const [propertyUsage, setPropertyUsage] = useState(
    data?.property_usage ? data.property_usage : "whole",
  );
  const [rentPrice, setRentPrice] = useState(data?.price ? data.price : "");
  const [contractType, setContractType] = useState(
    data?.contract_type ? data?.contract_type : ["long"],
  );
  const [saleableArea, setSaleableArea] = useState(
    data?.saleable_feet ? data?.saleable_feet : "",
  );
  const [roomNumber, setRoomNumber] = useState(data?.room ? data?.room : "");
  const [bathNumber, setBathNumber] = useState(
    data?.number_of_bath_room ? data?.number_of_bath_room : 1,
  );
  const [allowPet, setAllowPet] = useState(
    data?.allow_pet ? data?.allow_pet : 0,
  );
  const [facility, setFacility] = useState(
    data?.facilities ? data?.facilities : [],
  );
  const [photoList, setPhotoList] = useState(data?.photos ? data?.photos : []);
  const [isPrePaid, setIsPrePaid] = useState(data?.prepay_month ? true : false);
  const [prePaidMonth, setPrePaidMonth] = useState(
    data?.prepay_month ? data?.prepay_month : "",
  );
  const [prePaidRentDiscount, setPrePaidRentDiscount] = useState(
    data?.prepay_discount ? data?.prepay_discount : "",
  );
  const [isBestTalent, setIsBestTalent] = useState(
    data?.best_talent === "Y" ? "Y" : "N",
  );
  const [contractReducePrice, setContractReducePrice] = useState(
    data?.contract_reduce_price ? data?.contract_reduce_price : "",
  );
  const [propertyFloor, setPropertyFloor] = useState(
    data?.floor ? data?.floor : "",
  );
  const [propertyAge, setPropertyAge] = useState(
    data?.age_of_building ? data?.age_of_building : "",
  );
  const [propertyBuildingName, setPropertyBuildingName] = useState(
    data?.estate_name ? data?.estate_name : "",
  );
  const [propertySchoolNet, setPropertySchoolNet] = useState(
    data?.school ? data?.school : "",
  );
  const [propertyTitle, setPropertyTitle] = useState(
    data?.title ? data?.title : "",
  );
  const [propertyDesc, setPropertyDesc] = useState(
    data?.description ? data?.description : "",
  );
  const [propertyEstateID, setPropertyEstateID] = useState(
    data?.estate_id ? data?.estate_id : null,
  );
  const [photoLoading, setPhotoLoading] = useState(false);
  const [visitTime, setVisitTime] = useState(
    data?.visit_time ? data?.visit_time : "",
  );
  const [parkingLocation, setParkingLocation] = useState(
    data?.parking_location ? data?.parking_location : "",
  );
  const [parkingOther, setParkingOther] = useState(
    data?.parking_other ? data?.parking_other : [],
  );
  const [openParking, setOpenParking] = useState(
    data?.open_parking ? data?.open_parking : "",
  );

  const [selectedEstate, setSelectedEstate] = useState(
    data?.property_id
      ? {
          estate_id: data.estate_id,
          estate_lat: data.estate_lat,
          estate_lng: data.estate_lng,
          estate_name: data.estate_name,
          estate_address: data.estate_address,
          estate_entrance_year: data.estate_entrance_year,
        }
      : null,
  );

  const onNextStep = () => {
    if (activeTab === 1) {
      if (propertyType === null) {
        toast.warn("請先選擇物業類型", {
          position: "top-center",
        });

        return;
      } else if (!propertyUsage) {
        toast.warn("請先選擇出租空間", {
          position: "top-center",
        });
        return;
      } else if (!rentPrice || (rentPrice && rentPrice == "")) {
        toast.warn("請先輸入租金", {
          position: "top-center",
        });
        return;
      } else if (contractType && contractType.length == 0) {
        toast.warn("請先選擇租期", {
          position: "top-center",
        });
        return;
      }
    } else if (activeTab === 2) {
      // Check 私樓
      if (propertyType === 0) {
        if (!saleableArea) {
          toast.warn("請先輸入實用面積", {
            position: "top-center",
          });
          return;
        }

        if (!roomNumber) {
          toast.warn("請先選擇房間數目", {
            position: "top-center",
          });
          return;
        }
        // Check 車位
      } else if (propertyType === 2) {
        if (!parkingLocation) {
          toast.warn("請選擇車位位置", {
            position: "top-center",
          });

          return;
        }
      }
    } else if (activeTab === 4) {
      if (photoList.length === 0) {
        toast.warn("請上傳相片", {
          position: "top-center",
        });
        return;
      }
    } else if (activeTab === 6) {
      if (!propertyEstateID) {
        toast.warn("請先輸入大廈名稱", {
          position: "top-center",
        });

        return;
      }

      if (!propertyFloor && propertyType == 0) {
        toast.warn("請先選擇樓層", {
          position: "top-center",
        });

        return;
      }
    } else if (activeTab === 7) {
      if (!propertyTitle) {
        toast.warn("請先輸入標題", {
          position: "top-center",
        });
        return;
      }

      if (!propertyDesc) {
        toast.warn("請先輸入內容介紹", {
          position: "top-center",
        });
        return;
      }

      if (!minimalStart) {
        toast.warn("請先選擇最快起租日期", {
          position: "top-center",
        });
        return;
      }
    } else if (activeTab === 8) {
      postRent();
      return;
    }

    if (propertyType === 2 && activeTab === 2) {
      setActiveTab(activeTab + 2);
    } else {
      if (propertyID && activeTab === 5) {
        setActiveTab(activeTab + 2);
      } else setActiveTab(activeTab + 1);
    }
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  const onBeforeStep = () => {
    setActiveTab(activeTab - 1);
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  const removePhoto = (selected) => {
    const temp = Object.assign([], photoList);
    temp.splice(selected, 1);
    setPhotoList(temp);
  };

  const uploadPhoto = async (file) => {
    setPhotoLoading(true);
    const api = new API(session.token);
    const phoneUri = Object.assign([], photoList);
    const image = await resizeFile(file);
    const formData = new FormData();
    formData.append("photo", image);
    const response = await api.submitPropertyPhoto(formData);
    setPhotoLoading(false);
    if (response && response.ok) {
      phoneUri.push(response.data.full_url);
      setPhotoList(phoneUri);
    }
  };

  const postRent = async () => {
    const api = new API(session.token);
    const response = await api.postRent({
      propertyID,
      propertyCommission,
      propertyType,
      propertyUsage,
      saleableArea,
      roomNumber,
      bathNumber,
      facility,
      photos: photoList,
      rentPrice,
      prePaidMonth,
      prePaidRentDiscount,
      contractReducePrice,
      contractType,
      propertyTitle,
      propertyDesc,
      propertyShare: false,
      propertyFloor,
      propertyEstateID,
      isBestTalent,
      minimalStart,
      visitTime,
      allowPet,
      parkingLocation,
      parkingOther,
      openParking,
    });
    if (response && response.ok) {
      router.push(`/property/${response.data.property_id}`);
    } else {
      toast.error(
        response.data != null && response.data.message
          ? response.data.message
          : "系統出現錯誤，請稍後再試",
        {
          position: "top-center",
        },
      );
    }
  };

  useEffect(() => {
    if (!loadingSession)
      if (!session || (session && !session.member)) {
        router.push("/");
      }
  }, [session, loadingSession]);

  return (
    <Layout session={session}>
      <div
        className="nc-PageAddListing1 px-4 max-w-3xl mx-auto pb-24 pt-14 sm:py-14 lg:pb-32"
        data-nc-id="PageAddListing1"
      >
        <div className="space-y-11">
          <div>
            <span className="text-4xl font-semibold">{activeTab}</span>{" "}
            <span className="text-lg text-neutral-500 dark:text-neutral-400">
              / 8
            </span>
          </div>
          <div className="listingSection__wrap ">
            {activeTab === 1 && (
              <AddPropertyType
                propertyType={propertyType}
                rentPrice={rentPrice}
                propertyUsage={propertyUsage}
                contractType={contractType}
                setContractType={setContractType}
                setPropertyUsage={setPropertyUsage}
                setRentPrice={setRentPrice}
                setPropertyType={setPropertyType}
                minimalStart={minimalStart}
                setMinimalStart={setMinimalStart}
              />
            )}
            {activeTab === 2 &&
              propertyType === appConfig.PROPERTY_TYPE["住宅"] && (
                <AddPropertyArea
                  saleableArea={saleableArea}
                  setSaleableArea={setSaleableArea}
                  roomNumber={roomNumber}
                  setRoomNumber={setRoomNumber}
                  bathNumber={bathNumber}
                  setBathNumber={setBathNumber}
                  allowPet={allowPet}
                  setAllowPet={setAllowPet}
                />
              )}
            {activeTab === 2 &&
              propertyType === appConfig.PROPERTY_TYPE["車位"] && (
                <AddPropertyParking
                  parkingLocation={parkingLocation}
                  setParkingLocation={setParkingLocation}
                  parkingOther={parkingOther}
                  setParkingOther={setParkingOther}
                  openParking={openParking}
                  setOpenParking={setOpenParking}
                />
              )}
            {activeTab === 3 && (
              <AddPropertyFacility
                facility={facility}
                setFacility={setFacility}
              />
            )}

            {activeTab === 4 && (
              <AddPropertyPhoto
                loading={photoLoading}
                photoList={photoList}
                setPhotoList={setPhotoList}
                uploadPhoto={uploadPhoto}
                removePhoto={removePhoto}
              />
            )}

            {activeTab === 5 && (
              <AddPropertyPlan
                isPrePaid={isPrePaid}
                prePaidMonth={prePaidMonth}
                prePaidRentDiscount={prePaidRentDiscount}
                isBestTalent={isBestTalent}
                contractReducePrice={contractReducePrice}
                setIsPrePaid={setIsPrePaid}
                setPrePaidMonth={setPrePaidMonth}
                setPrePaidRentDiscount={setPrePaidRentDiscount}
                setIsBestTalent={setIsBestTalent}
                setContractReducePrice={setContractReducePrice}
              />
            )}

            {activeTab === 6 && (
              <AddPropertyLocation
                propertyType={propertyType}
                propertyFloor={propertyFloor}
                propertyAge={propertyAge}
                propertyBuildingName={propertyBuildingName}
                propertySchoolNet={propertySchoolNet}
                propertyEstateID={propertyEstateID}
                setPropertyEstateID={setPropertyEstateID}
                setPropertySchoolNet={setPropertySchoolNet}
                setPropertyBuildingName={setPropertyBuildingName}
                setPropertyAge={setPropertyAge}
                setPropertyFloor={setPropertyFloor}
                setSelectedEstate={setSelectedEstate}
              />
            )}

            {activeTab === 7 && (
              <AddPropertyTitle
                propertyType={propertyType}
                propertyTitle={propertyTitle}
                setPropertyTitle={setPropertyTitle}
                propertyDesc={propertyDesc}
                setPropertyDesc={setPropertyDesc}
                visitTime={visitTime}
                setVisitTime={setVisitTime}
              />
            )}

            {activeTab === 8 && (
              <PageAddPropertyPreview
                propertyType={propertyType}
                photoList={photoList}
                propertyTitle={propertyTitle}
                rentPrice={rentPrice}
                roomNumber={roomNumber}
                selectedEstate={selectedEstate}
                saleableArea={saleableArea}
                parkingLocation={parkingLocation}
              />
            )}
          </div>
          <div className="flex justify-end space-x-5">
            {activeTab > 1 && (
              <ButtonSecondary onClick={onBeforeStep}> 上一步</ButtonSecondary>
            )}
            <ButtonPrimary onClick={onNextStep}>
              {activeTab === 8 ? "發佈" : "繼續"}
            </ButtonPrimary>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default AddPropertyPage;

export async function getServerSideProps(context) {
  const session = await getSession(context);
  const { property_id } = context.query;

  if (!session) {
    context.res.writeHead(302, { Location: "/" });
    context.res.end();
    return { props: {} };
  }

  const api = new API(session && session.token);
  const response = await api.fetchPropertyDetail(property_id);
  const data = response && response.ok ? response.data.property : null;

  return {
    props: { data },
  };
}
