import Head from "next/head";
import { getSession } from "next-auth/client";
import Layout from "../src/components/layout";

function About({ session }) {
  return (
    <Layout session={session}>
      <Head>
        <meta charSet="utf-8" />
        <title>關於我們 - 免佣租屋網</title>

        <meta
          property="og:title"
          content="關於我們 - 免佣租屋網"
          key="ogtitle"
        />
        <meta
          property="og:site_name"
          content="關於我們 - 免佣租屋網"
          key="ogsitename"
        />

        <meta
          name="description"
          content="免佣租屋平台的構思，是一班有志向的年青人。有見香港的租屋平台尚有很大的進步空間，能夠做得更有效率、更平既價錢，也更多選擇，更專業。"
          key="desc"
        />
        <meta
          name="og:description"
          property="og:description"
          content="免佣租屋平台的構思，是一班有志向的年青人。有見香港的租屋平台尚有很大的進步空間，能夠做得更有效率、更平既價錢，也更多選擇，更專業。"
          key="ogdesc"
        />

        <meta property="og:image" content="/images/icon.png" />
        <meta property="og:locale" content="zh_HK" />

        <link rel="icon" href="/favicon.ico" />
        <link rel="shortcut icon" href="/favicon.ico" />
      </Head>

      <div className="container py-8 lg:py-18 space-y-16 lg:space-y-28">
        <div className="nc-SectionHero relative" data-nc-id="SectionHero">
          <div className="flex flex-col lg:flex-row space-y-14 lg:space-y-0 lg:space-x-10 items-center relative text-center lg:text-left">
            <div className="w-screen max-w-full xl:max-w-lg space-y-5 lg:space-y-7">
              <h2 className="text-3xl !leading-tight font-semibold text-neutral-900 md:text-4xl xl:text-5xl dark:text-neutral-100">
                👋 關於我們
              </h2>
              <span className="block text-base xl:text-lg text-neutral-6000 dark:text-neutral-400">
                免佣租屋平台的構思，是一班有志向的年青人。有見香港的租屋平台尚有很大的進步空間，能夠做得更有效率、更平既價錢，也更多選擇，更專業。
                <br />
                <br />
                令一般大眾亦可以自己搵到岩水既租盤，亦唔需要傳統透過地產代理付出昂貴佣金，幫你慳返半個月代理佣金！
              </span>

              {/* <h2 className="my-24 sm:my-20  text-xl leading-[115%] md:text-xl md:leading-[115%] font-semibold text-neutral-900">
                聯絡我們
              </h2> */}

              <div>
                <h3 className="uppercase font-semibold text-sm dark:text-neutral-200 tracking-wider">
                  聯絡我們
                </h3>
                <span className="block mt-2 text-neutral-500 dark:text-neutral-400">
                  okayfound@gmail.com
                </span>
              </div>
            </div>

            <div className="flex-grow">
              <img className="w-full" src="./images/aboutus.svg" alt="" />
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default About;

export async function getServerSideProps(context) {
  const session = await getSession(context);

  return {
    props: { session },
  };
}
