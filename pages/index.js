import { useEffect, useState } from "react";
import Head from "next/head";
import { useRouter } from "next/router";
import { getSession } from "next-auth/client";
import {
  HomeHeader,
  HomeHowWork,
  HomeListing,
  HomeDownloadApp,
} from "../src/components/home";
import appConfig from "../src/utils/appConfig";
import API from "../src/utils/api";
import Layout from "../src/components/layout";
import { eventTracker } from "../src/utils/gaEventTracker";

function Home({ session }) {
  const router = useRouter();
  const [tabActiveState, setTabActiveState] = useState("住宅");
  const [list, setList] = useState([]);
  const [price, setPrice] = useState([1000, 60000]);
  const [propertyType, setPropertyType] = useState(
    appConfig.PROPERTY_TYPE["住宅"],
  );
  const [subPropertyType, setSubPropertyType] = useState([]);
  const [propertyDistrict, setPropertyDistrict] = useState([]);

  const searchProperty = () => {
    let url = `/list?property_type=${propertyType}&price1=${price[0]}&price2=${price[1]}`;
    if (propertyDistrict.length > 0) url += `&district=${propertyDistrict}`;
    if (subPropertyType.length > 0) {
      url += `&sub_property_type=${subPropertyType}`;
    }
    router.push(url);
  };

  const fetchListByType = async () => {
    const api = new API();
    const response = await api.fetchListByType({
      propertyType: tabActiveState,
    });
    if (response && response.ok) {
      setList(response.data.list);
    }
  };

  useEffect(() => {
    fetchListByType();
  }, [tabActiveState]);

  useEffect(() => {
    const $body = document.querySelector("body");
    if (!$body) return;
    $body.classList.add("theme-cyan-blueGrey");
    return () => {
      $body.classList.remove("theme-cyan-blueGrey");
    };
  }, []);

  return (
    <Layout session={session}>
      <Head>
        <meta charSet="utf-8" />
        <title>租屋台 - 免佣租屋網</title>

        <meta property="og:title" content="租屋台 - 免佣租屋網" key="ogtitle" />
        <meta property="og:site_name" content="租屋台 - 免佣租屋網" />
        <meta
          name="description"
          content="免佣免費租屋網站，讓租客與業主即時透過平台聊天及查詢單位詳情，免除地產廣告等騷擾及自行安排時間睇樓搵樓"
        />
        <meta
          name="og:description"
          property="og:description"
          content="免佣免費租屋網站，讓租客與業主即時透過平台聊天及查詢單位詳情，免除地產廣告等騷擾及自行安排時間睇樓搵樓"
        />
        <meta property="og:image" content="/images/icon.png" />
        <meta property="og:locale" content="zh_HK" />

        <link rel="icon" href="/favicon.ico" />
        <link rel="shortcut icon" href="/favicon.ico" />
      </Head>
      <div className="nc-PageHome2 relative overflow-hidden">
        <div className="container relative space-y-24 mb-24 lg:space-y-28 lg:mb-28">
          <HomeHeader
            price={price}
            setPrice={setPrice}
            propertyType={propertyType}
            setPropertyType={setPropertyType}
            subPropertyType={subPropertyType}
            setSubPropertyType={setSubPropertyType}
            propertyDistrict={propertyDistrict}
            setPropertyDistrict={setPropertyDistrict}
            searchProperty={searchProperty}
          />
          <HomeHowWork />
          <HomeListing
            list={list}
            propertyType={propertyType}
            tabActiveState={tabActiveState}
            setTabActiveState={setTabActiveState}
          />
          <HomeDownloadApp />
        </div>
      </div>
    </Layout>
  );
}

export default Home;

export async function getServerSideProps(context) {
  const session = await getSession(context);

  return {
    props: { session },
  };
}
