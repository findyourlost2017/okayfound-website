import Head from "next/head";
import { useEffect, useState } from "react";
import { useSession } from "next-auth/client";
import Layout from "../src/components/layout";
import appConfig from "../src/utils/appConfig";
import {
  ListHeader,
  ListOtherFilter,
  ListContent,
  ListPagination,
} from "../src/components/list";
import API from "../src/utils/api";

const PageList = ({ data }) => {
  const [session, loading] = useSession();
  const [page, setPage] = useState(1);
  const [list, setList] = useState([]);
  const [total, setTotal] = useState(1);
  const [price, setPrice] = useState([
    data?.price1 ? data?.price1 : 1000,
    data?.price2 ? data?.price2 : 60000,
  ]);
  const [propertyType, setPropertyType] = useState(
    data?.property_type ? data?.property_type : null,
  );
  const [subPropertyType, setSubPropertyType] = useState(
    data?.sub_property_type ? data?.sub_property_type : [],
  );
  const [room, setRoom] = useState();
  const [area, setArea] = useState();
  const [propertyDistrict, setPropertyDistrict] = useState(
    data?.district ? data?.district : [],
  );
  const [estateOptions, setEstateOptions] = useState([]);

  const [estateName, setEstateName] = useState();
  const [estateID, setEstateID] = useState();
  const [loadingPage, setLoadingPage] = useState(false);

  const onChangeRoom = (selected) => {
    setRoom(selected);
    setPage(1);
  };

  const onChangeArea = (selected) => {
    setArea(selected);
    setPage(1);
  };

  const onSelectEstate = (item) => {
    setEstateName(item.estate_name);
    setEstateID(item.estate_id);
  };

  const onChangeEstate = async (input) => {
    const api = new API();
    const response = await api.fetchEstate({
      input,
      propertyType: propertyType,
    });
    if (response && response.status === 200) {
      setEstateOptions(response.data.estates);
    }
  };

  const onChangePage = (selectedPage) => {
    fetchPropertyList(selectedPage);
  };

  const searchProperty = () => {
    setPage(1);
    fetchPropertyList(1);
  };

  const fetchPropertyList = async (page) => {
    let formattedDistrict = [];
    if (propertyDistrict && propertyDistrict.length > 0) {
      if (propertyDistrict[0] < 0) {
        appConfig.districtOption[
          appConfig.districtTypeMapping[propertyDistrict[0]]
        ].map((item, index) => {
          if (index > 0) formattedDistrict = formattedDistrict.concat(item.id);
        });
      } else {
        formattedDistrict = propertyDistrict;
      }
    }

    setLoadingPage(true);
    const api = new API();
    const response = await api.fetchPropertyList(
      {
        price,
        propertyType,
        propertyDistrict: formattedDistrict,
        subPropertyType,
        room: room && [room],
        area: area && [area],
        estate: estateID,
        source: "website",
      },
      page,
    );
    setLoadingPage(false);

    if (response && response.ok) {
      if (page === 1) setTotal(response.data.total);
      setList(response.data.list);
    }
  };

  useEffect(() => {
    fetchPropertyList(1);
  }, []);

  return (
    <Layout session={session}>
      <Head>
        <title>最新免佣租盤 - 租屋台</title>
        <meta
          property="og:title"
          content="最新租樓列表 - 租屋台"
          key="ogtitle"
        />
        <link rel="icon" href="/favicon.ico" />
        <link rel="shortcut icon" href="/favicon.ico" />
      </Head>

      <div
        className={`nc-ListingStayPage relative overflow-hidden `}
        data-nc-id="ListingStayPage"
      >
        <div
          className="nc-SectionGridFilterCard pt-10 lg:pt-16 "
          data-nc-id="SectionGridFilterCard"
        >
          <div className="container relative overflow-hidden mb-5">
            <ListHeader
              price={price}
              setPrice={setPrice}
              propertyType={propertyType}
              setPropertyType={setPropertyType}
              subPropertyType={subPropertyType}
              setSubPropertyType={setSubPropertyType}
              propertyDistrict={propertyDistrict}
              setPropertyDistrict={setPropertyDistrict}
              searchProperty={searchProperty}
            />
            <ListOtherFilter
              room={room}
              area={area}
              estateID={estateID}
              estateName={estateName}
              estateOptions={estateOptions}
              setEstateID={setEstateID}
              setEstateName={setEstateName}
              onChangeArea={onChangeArea}
              onChangeRoom={onChangeRoom}
              onChangeEstate={onChangeEstate}
              onSelectEstate={onSelectEstate}
            />
            <ListContent data={list} loading={loadingPage} />
            {!loading && (
              <ListPagination
                page={page}
                onChangePage={onChangePage}
                total={total}
              />
            )}
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default PageList;

export async function getServerSideProps(context) {
  const { property_type, price1, price2, district, sub_property_type } =
    context.query;
  const formattedDistrict = district?.split(",").map((i) => parseInt(i));
  const formattedSubProperty = sub_property_type
    ?.split(",")
    .map((i) => parseInt(i));

  let checkCorrectDistrict = true;
  let checkCorrectSubProperty = true;

  formattedDistrict?.map((item) => {
    if (!Number.isInteger(item)) {
      checkCorrectDistrict = false;
    }
  });

  formattedSubProperty?.map((item) => {
    if (!Number.isInteger(item) || !appConfig.propertyTypeMapping[item]) {
      checkCorrectSubProperty = false;
    }
  });

  return {
    props: {
      data: {
        district:
          checkCorrectDistrict && formattedDistrict ? formattedDistrict : null,
        property_type:
          property_type !== undefined &&
          (parseInt(property_type) === appConfig.PROPERTY_TYPE["住宅"] ||
            parseInt(property_type) === appConfig.PROPERTY_TYPE["車位"])
            ? property_type
            : null,
        sub_property_type:
          checkCorrectSubProperty && formattedSubProperty
            ? formattedSubProperty
            : null,
        price1: price1 ? price1 : null,
        price2: price2 ? price2 : null,
      },
    },
  };
}
