import { withImageProxy } from "@blazity/next-image-proxy";

export default withImageProxy({
  whitelistedPatterns: [
    /^https?:\/\/(.*).car4goal.com/,
    /^https?:\/\/(.*).s3.wasabisys.com/,
    /^https?:\/\/(.*).okayfound.com/,
  ],
});
