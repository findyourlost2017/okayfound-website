import NextAuth from "next-auth";
import Providers from "next-auth/providers";
import API from "../../../src/utils/api";

const options = {
  providers: [
    Providers.Credentials({
      id: "update-user",
      name: "Credentials",
      credentials: {},
      authorize: async (credentials) => {
        return Promise.resolve({
          user: JSON.parse(credentials.user).member,
          token: JSON.parse(credentials.user).token,
        });
      },
    }),
    Providers.Facebook({
      clientId: process.env.FACEBOOK_ID,
      clientSecret: process.env.FACEBOOK_SECRET,
    }),
    Providers.Google({
      clientId: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      authorizationUrl:
        "https://accounts.google.com/o/oauth2/v2/auth?prompt=consent&access_type=offline&response_type=code",
    }),
  ],
  session: {
    jwt: true,
  },
  callbacks: {
    redirect: async (url, _) => {
      if (url === "/api/auth/signin") {
        return Promise.resolve("/");
      }
      return Promise.resolve("/");
    },
    session: async (session, user, sessionToken) => {
      return Promise.resolve({
        member: user.member,
        token: user.token,
      });
    },
    jwt: async (token, user, account, profile) => {
      if (profile && account.provider == "facebook") {
        const api = new API();
        const response = await api.loginAccount({
          device_id: "WEBSITE",
          platform_type: 3,
          member_name: profile.name,
          email: profile.email,
          uid: profile.id,
        });
        if (response && response.ok) {
          token.member = response.data.member;
          token.token = response.data.token;
        }
      } else if (profile && account.provider == "google") {
        const api = new API();

        const response = await api.loginAccount({
          device_id: "WEBSITE",
          platform_type: 2,
          uid: profile.id,
          email: profile.email,
          member_name: profile.name,
          idToken: profile.idToken,
        });
        if (response && response.ok) {
          token.member = response.data.member;
          token.token = response.data.token;
        }
      } else if (profile && account.type == "credentials") {
        token.member = user.user;
        token.token = user.token;
      }

      return Promise.resolve(token); // ...here
    },
  },
  events: {},
  // Enable debug messages in the console if you are having problems
  debug: false,
};

export default (req, res) => NextAuth(req, res, options);
