import Head from "next/head";
import React, { useState, useEffect } from "react";
import { getSession } from "next-auth/client";
import { useRouter } from "next/router";
import Layout from "../../src/components/layout";
import appConfig from "../../src/utils/appConfig";
import {PropertyPhotos,PropertyBasicInfo,PropertyIntro,PropertyFacility,PropertyParkingOther,PropertyNearFacility,PropertyMap,PropertySidebar,PropertyRestaurant,PropertyPetro,} from "../../src/components/property";
import API from "../../src/utils/api";

const Property = ({ data, session }) => {
  const [restaurants, setRestaurants] = useState([]);
  const [petro, setPetro] = useState([]);
  const router = useRouter();

  const onShowChat = () => {
    if (data.messenger) {
      window.open(data.messenger);
    } else if (session && session.member) {
      router.push({
        pathname: "/messages",
        query: { property_id: data.property_id },
      });
    } else router.push("/login");
  };

  const fetchRestaurant = async (lat, lng) => {
    const api = new API();
    const response = await api.fetchRestaurant({ lat, lng });
    if (response && response.ok) {
      setRestaurants(response.data.list);
    }
  };

  const fetchPetro = async (lat, lng) => {
    const api = new API();
    const response = await api.fetchPetro({ lat, lng });
    if (response && response.ok) {
      setPetro(response.data.list);
    }
  };

  useEffect(() => {
    if (data.property_type === 0 && data.estate_lat && data.estate_lng)
      fetchRestaurant(data.estate_lat, data.estate_lng);
    else if (data.property_type === 2 && data.estate_lat && data.estate_lng) {
      fetchPetro(data.estate_lat, data.estate_lng);
    }
  }, []);

  return (
    <Layout session={session}>
      <Head>
        <meta charSet="utf-8" />
        <title>{data?.title} - 租屋台</title>
        <meta
          property="og:title"
          content={data?.title + " - 租屋台"}
          key="ogtitle"
        />
        <meta name="description" content={data?.description} key="desc" />
        <meta
          name="og:description"
          property="og:description"
          content={data?.description}
        />
        <meta property="fb:app_id" content="5711089418954202" />
        <meta property="og:image" content={data?.thumbnail} />
        <meta property="og:site_name" content="租屋台 - 免佣租屋網" />
        <meta property="og:type" content="website" />
        <meta
          property="og:url"
          content={`https://okayfound.com/property/${data.property_id}`}
        />
        <meta property="og:locale" content="zh-HK" />

        <link rel="icon" href="../images/icon.png" />
      </Head>
      <div
        className="ListingDetailPage nc-ListingStayDetailPage"
        data-nc-id="ListingStayDetailPage"
      >
        <PropertyPhotos photos={data.photos} />
        <main className="container relative z-10 mt-11 flex flex-col lg:flex-row ">
          <div className="w-full lg:w-3/5 xl:w-2/3 space-y-8 lg:space-y-10 lg:pr-10">
            <PropertyBasicInfo data={data} />

            <PropertyIntro data={data} />
            {data.property_type !== appConfig.PROPERTY_TYPE["車位"] && (
              <PropertyFacility facilities={data.facilities} />
            )}
            {data.property_type === appConfig.PROPERTY_TYPE["車位"] &&
              data.parking_other &&
              data.parking_other.length > 0 && (
                <PropertyParkingOther parkingOther={data.parking_other} />
              )}
            {!data.estate_name.includes("單幢") && (
              <PropertyNearFacility
                shop={data.shop}
                mall={data.mall}
                transport={data.transport}
              />
            )}
            {!data.estate_name.includes("單幢") &&
              data.estate_lat &&
              data.estate_lng && (
                <PropertyMap lat={data.estate_lat} lng={data.estate_lng} />
              )}
          </div>
          <PropertySidebar
            data={data}
            onShowChat={onShowChat}
            member={session?.member}
          />
        </main>
        {data.property_type === 0 && restaurants && restaurants.length > 0 && (
          <PropertyRestaurant restaurants={restaurants} />
        )}

        {data.property_type === 2 && petro && petro.length > 0 && (
          <PropertyPetro petro={petro} />
        )}

        <div className="pb-10" />
      </div>
    </Layout>
  );
};

export default Property;

export async function getServerSideProps(context) {
  const session = await getSession(context);
  const api = new API(session && session.token);
  const response = await api.fetchPropertyDetail(context.query.slug);

  const data = response && response.ok ? response.data.property : null;
  return {
    props: { data, session },
  };
}
