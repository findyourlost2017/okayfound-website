import Head from "next/head";
import { ToastContainer } from "react-toastify";
import { Provider } from "next-auth/client";
import Script from "next/script";
import "moment/locale/zh-tw";

import "../styles/globals.css";
import "../styles/index.scss";
import "rc-slider/assets/index.css";
import "../fonts/line-awesome-1.3.0/css/line-awesome.css";
import "react-toastify/dist/ReactToastify.css";
import "../fonts/fontawesome/css/all.min.css";

function MyApp({ Component, pageProps }) {
  return (
    <div className="bg-white text-base dark:bg-neutral-900 text-neutral-900 dark:text-neutral-200">
      <Head>
        <meta charSet="utf-8" />
        <title>租屋台 - 免佣租屋網</title>
        <meta property="og:title" content="租屋台 - 免佣租屋網" key="ogtitle" />
        <meta property="og:site_name" content="租屋台 - 免佣租屋網" />
        <meta
          name="keyword"
          content="免佣租屋,租樓,租屋,租房,搵樓,樓盤,樓盤搜尋,地產,業主,代理,自讓,住宅,車位,分租,短租,村屋,工商,商廈,店舖,獨立屋,套房,土地,農地,公屋"
        />
        <meta
          name="description"
          content="香港免佣租屋，讓租客與業主即時透過平台聊天及查詢單位詳情，免除地產廣告等騷擾及自行安排時間睇樓搵樓"
        />
        <meta
          property="og:description"
          content="香港免佣租屋，讓租客與業主即時透過平台聊天及查詢單位詳情，免除地產廣告等騷擾及自行安排時間睇樓搵樓"
        />
        {/* <meta property="og:image" content="/images/icon.png" /> */}
        <meta property="og:locale" content="zh-HK" />
        <meta property="fb:app_id" content="5711089418954202" />

        <link rel="icon" href="/favicon.ico" />
        <link rel="shortcut icon" href="/favicon.ico" />
        <script
          async
          src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-6055646395921991"
          crossorigin="anonymous"
        ></script>
      </Head>
      {/* //G-C8CMVGZEG8 */}
      <Script
        src="https://www.googletagmanager.com/gtag/js?id=G-C8CMVGZEG8"
        strategy="afterInteractive"
      />
      <Script id="google-analytics" strategy="afterInteractive">
        {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){window.dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-C8CMVGZEG8', {
            page_path: window.location.pathname,
          });
        `}
      </Script>
      <script
        async
        src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-6055646395921991"
        crossorigin="anonymous"
      ></script>
      <ToastContainer />
      <Provider session={pageProps.session}>
        <Component {...pageProps} />
      </Provider>
    </div>
  );
}

export default MyApp;
