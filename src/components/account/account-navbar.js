import { signOut } from "next-auth/client";
import { NavLink } from "../common";

function AccountNavbar() {
  return (
    <div className="border-b border-neutral-200 dark:border-neutral-700 pt-12 bg-white dark:bg-neutral-800">
      <div className="container">
        <div className="flex space-x-8 md:space-x-14 overflow-x-auto hiddenScrollbar">
          <NavLink
            // activeClassName="!border-primary-500"
            href="/account"
            className="block py-5 md:py-8 border-b-2 border-transparent flex-shrink-0"
          >
            帳號資料
          </NavLink>
          <NavLink
            // activeClassName="!border-primary-500"
            href="/my-listing"
            className="block py-5 md:py-8 border-b-2 border-transparent flex-shrink-0"
          >
            我的租盤
          </NavLink>
          <NavLink
            // activeClassName="!border-primary-500"
            onClick={() => signOut()}
            href="#"
            className="cursor-pointer block py-5 md:py-8 border-b-2 border-transparent flex-shrink-0"
          >
            登出
          </NavLink>
        </div>
      </div>
    </div>
  );
}

export default AccountNavbar;
