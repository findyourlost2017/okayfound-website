import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import _ from "lodash";
import { toast } from "react-toastify";
import API from "../../utils/api";
import appConfig from "../../utils/appConfig";
import { PropertyCard } from "../property";
import LoadingCard from "../common/loading-card";

function AccountListing({ token }) {
  const router = useRouter();
  const [list, setList] = useState([]);
  const [loading, setLoading] = useState(true);

  const editProperty = (selected) => {
    router.push(`/add-property?property_id=${selected.property_id}`);
  };

  const repostProperty = async (propertyID) => {
    // set loading true
    setList((state) =>
      state.map((item) => {
        if (item.property_id === propertyID) item.loading = true;
        return { ...item };
      }),
    );

    const api = new API(token);
    const response = await api.repostRent(propertyID);
    if (response && response.ok) {
      toast.success("重新刊登租盤成功");
      setList((state) =>
        state.map((item) => {
          if (item.property_id === propertyID) {
            item.status = appConfig.PROPERTY_STATUS["APPROVAL"];
            item.loading = false;
          }
          return { ...item };
        }),
      );
    } else {
      // set loading true
      setList((state) =>
        state.map((item) => {
          if (item.property_id === propertyID) item.loading = false;
          return { ...item };
        }),
      );
      toast.error(
        response.data != null && response.data.message
          ? response.data.message
          : "系統出現錯誤，請稍後再試",
        {
          position: "top-center",
        },
      );
    }
  };

  const stopProperty = async (propertyID) => {
    // set loading true
    setList((state) =>
      state.map((item) => {
        if (item.property_id === propertyID) item.loading = true;
        return { ...item };
      }),
    );

    const api = new API(token);
    const response = await api.stopRent(propertyID);
    if (response && response.ok) {
      toast.success("暫停租盤成功");
      setList((state) =>
        state.map((item) => {
          if (item.property_id === propertyID) {
            item.status = appConfig.PROPERTY_STATUS["STOPPED"];
            item.loading = false;
          }
          return { ...item };
        }),
      );
    } else {
      // set loading true
      setList((state) =>
        state.map((item) => {
          if (item.property_id === propertyID) item.loading = false;
          return { ...item };
        }),
      );
      toast.error(
        response.data != null && response.data.message
          ? response.data.message
          : "系統出現錯誤，請稍後再試",
        {
          position: "top-center",
        },
      );
    }
  };

  const deleteProperty = async (propertyID) => {
    const api = new API(token);
    const response = await api.deleteRent(propertyID);
    if (response && response.ok) {
      toast.success("刪除租盤成功");
      setList((state) => _.filter(state, (i) => i.property_id !== propertyID));
    } else {
      toast.error(
        response.data != null && response.data.message
          ? response.data.message
          : "系統出現錯誤，請稍後再試",
        {
          position: "top-center",
        },
      );
    }
  };

  const fetchPropertyHistory = async () => {
    setLoading(true);
    const api = new API(token);
    const response = await api.fetchPropertyHistory();
    setLoading(false);
    if (response && response.status === 200) {
      setList(response.data.list);
    } else {
      toast.error(
        response.data != null && response.data.message
          ? response.data.message
          : "系統出現錯誤，請稍後再試",
        {
          position: "top-center",
        },
      );
    }
  };

  useEffect(() => {
    fetchPropertyHistory();
  }, []);

  return (
    <div className="space-y-6 sm:space-y-8">
      <h2 className="text-3xl font-semibold">我的租盤</h2>
      <div className="w-14 border-b border-neutral-200" />

      <div className="grid grid-cols-1 gap-6 md:gap-8 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
        {!loading &&
          list &&
          list.map((item) => (
            <PropertyCard
              key={`${item.property_id}_list`}
              data={item}
              editProperty={editProperty}
              deleteProperty={deleteProperty}
              stopProperty={stopProperty}
              repostProperty={repostProperty}
              editable={true}
            />
          ))}

        {loading &&
          _.range(8).map((item) => (
            <LoadingCard key={`${item}_loading_list`} />
          ))}
      </div>
    </div>
  );
}

export default AccountListing;
