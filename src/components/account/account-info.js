import { useState } from "react";
import { signIn } from "next-auth/client";
import { Input, Label } from "../input";
import ButtonPrimary from "../button/button-primary";
import API from "../../utils/api";

function AccountInfo({ member, token }) {
  const [memberName, setMemberName] = useState(member.member_name);
  const [loading, setLoading] = useState(false);

  const updateProfile = async () => {
    setLoading(true);
    const api = new API(token);
    const response = await api.updateProfile({ memberName });
    setLoading(false);
    if (response && response.ok) {
      await signIn("update-user", {
        user: JSON.stringify({
          member: response.data.member,
          token: token,
        }),
      });
    }
  };

  return (
    <div className="space-y-6 sm:space-y-8">
      <h2 className="text-3xl font-semibold">帳號資料</h2>
      <div className="w-14 border-b border-neutral-200 dark:border-neutral-700"></div>
      <div className="flex flex-col md:flex-row">
        <div className="flex-grow mt-10 md:mt-0 md:pl-16 max-w-3xl space-y-6">
          <div>
            <Label>Email</Label>
            <Input className="mt-1.5" defaultValue={member.email} disabled />
          </div>
          {/* ---- */}

          <div>
            <Label>帳號名稱</Label>
            <Input
              className="mt-1.5"
              value={memberName}
              onChange={(e) => setMemberName(e.target.value)}
            />
          </div>

          {/* ---- */}

          <div className="pt-2">
            <ButtonPrimary
              className="bg-orange-500 hover:bg-orange-700"
              onClick={updateProfile}
              loading={loading}
            >
              更新資訊
            </ButtonPrimary>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AccountInfo;
