import { Fragment, useRef } from "react";
import useDragAndDrop from "../../utils/useDragAndDrop";

function PageAddPropertyPhoto({
  loading,
  photoList,
  uploadPhoto,
  removePhoto,
}) {
  const inputFile = useRef(null);
  const { onDragOver, onDragLeave } = useDragAndDrop();

  const onDrop = (e) => {
    e.preventDefault();
    e.stopPropagation();
    if (e.dataTransfer.files && e.dataTransfer.files[0]) {
      uploadPhoto(e.dataTransfer.files[0]);
    }
  };

  const renderGallery = () => {
    return (
      <>
        {photoList &&
          photoList.map((item, index) => (
            <div className="flex flex-wrap w-1/3" key={`${item}_photo`}>
              <div className="relative w-full p-1 md:p-2">
                <img
                  alt="gallery"
                  className="block object-cover object-center w-full h-full max-h-32 rounded-lg"
                  src={item}
                />
                <button
                  onClick={() => removePhoto(index)}
                  className="absolute top-0 right-0 inline-flex items-center justify-center w-8 h-8 text-pink-100 transition-colors duration-150 bg-pink-700 rounded-full focus:shadow-outline hover:bg-pink-800"
                >
                  <i className="text-white-500 fa-solid fa-xmark" />
                </button>
              </div>
            </div>
          ))}
      </>
    );
  };

  const renderUploadBox = () => {
    return (
      <div
        className="mt-5 cursor-pointer"
        onDragOver={onDragOver}
        onDragLeave={onDragLeave}
        onDrop={onDrop}
        onClick={() => inputFile.current.click()}
      >
        <div className="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-neutral-300 dark:border-neutral-6000 border-dashed rounded-md">
          {!loading && (
            <div className="space-y-1 text-center">
              <svg
                className="mx-auto h-12 w-12 text-neutral-400"
                stroke="currentColor"
                fill="none"
                viewBox="0 0 48 48"
                aria-hidden="true"
              >
                <path
                  d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                ></path>
              </svg>

              <div className="flex text-sm text-neutral-6000 dark:text-neutral-300">
                <label
                  htmlFor="file-upload"
                  className="relative cursor-pointer  rounded-md font-medium text-primary-6000 hover:text-primary-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-primary-500"
                >
                  <span className="text-center">上傳相片</span>
                  <input
                    id="file-upload"
                    name="file-upload"
                    type="file"
                    accept="image/*"
                    className="sr-only"
                    ref={inputFile}
                    onChange={(e) => uploadPhoto(e.target.files[0])}
                  />
                </label>
                <p className="pl-1">或拖動相片至此</p>
              </div>
              <p className="text-xs text-neutral-500 dark:text-neutral-400">
                PNG, JPG, GIF up to 10MB
              </p>
            </div>
          )}

          {loading && (
            <div className="mt-3 space-y-1 text-center">
              <div className="animate-bounce inline-flex items-center justify-center">
                <svg
                  aria-hidden="true"
                  className="mr-2 w-6 h-6 text-gray-200 animate-spin dark:text-gray-600 fill-orange-500"
                  viewBox="0 0 100 101"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                    fill="currentColor"
                  />
                  <path
                    d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                    fill="currentFill"
                  />
                </svg>
              </div>
              <p className="text-sm text-neutral-500 dark:text-neutral-400">
                正在上傳相片
              </p>
            </div>
          )}
        </div>
      </div>
    );
  };

  return (
    <Fragment>
      <h2 className="text-2xl font-semibold">新增相片</h2>
      <span className="block mt-2 text-neutral-500 dark:text-neutral-400">
        上傳的第一張相將用作為發佈封面用。你可以先上傳一張相片，在發佈後再補上更多相片。
      </span>
      <div className="w-14 border-b border-neutral-200 dark:border-neutral-700"></div>
      {/* Photo uploader */}
      <div className="space-y-8">
        {/* Gallery */}
        <div className="flex flex-wrap -m-1 md:-m-2">{renderGallery()}</div>
        {/* End Gallery */}
        {/* Upload Box */}
        <div>{renderUploadBox()}</div>
        {/* End Upload Box */}
      </div>
    </Fragment>
  );
}

export default PageAddPropertyPhoto;
