import { Fragment } from "react";
import FormItem from "../input/form-item";
import Select from "../input/select";
import Checkbox from "../input/checkbox-sm";

function PageAddPropertyParking({
  parkingLocation,
  setParkingLocation,
  parkingOther,
  setParkingOther,
  openParking,
  setOpenParking,
}) {
  const selectParkingOther = (item) => {
    const temp = Object.assign([], parkingOther);
    if (temp.indexOf(item) > -1) {
      let index = temp.indexOf(item);
      temp.splice(index, 1);
    } else {
      temp.push(item);
    }
    setParkingOther(temp);
  };

  return (
    <Fragment>
      <h2 className="text-2xl font-semibold">車位資料</h2>
      <div className="w-14 border-b border-neutral-200 dark:border-neutral-700"></div>
      <FormItem label="位置">
        <Select
          value={parkingLocation}
          onChange={(e) => setParkingLocation(e.target.value)}
        >
          <option value="" disabled selected>
            請選擇車位位置
          </option>
          <option value="室內">室內</option>
          <option value="露天">露天</option>
        </Select>
      </FormItem>

      {parkingLocation === "露天" && (
        <FormItem label="露天情況">
          <Select
            value={openParking}
            onChange={(e) => setOpenParking(e.target.value)}
          >
            <option value="" disabled>
              請選擇露天情況
            </option>
            <option value="有蓋">有蓋</option>
            <option value="️無蓋">️無蓋</option>
          </Select>
        </FormItem>
      )}

      <FormItem label="其他">
        <div className="mt-6 grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-5">
          <Checkbox
            label="獨立車位"
            name="獨立車位"
            checked={parkingOther?.indexOf("獨立車位") > -1}
            onChange={() => selectParkingOther("獨立車位")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="有閘"
            name="有閘"
            checked={parkingOther?.indexOf("有閘") > -1}
            onChange={() => selectParkingOther("有閘")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="可泊大車/七人車/輕型貨車"
            name="可泊大車/七人車/輕型貨車"
            checked={parkingOther?.indexOf("可泊大車/七人車/輕型貨車") > -1}
            onChange={() => selectParkingOther("可泊大車/七人車/輕型貨車")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="可洗車"
            name="可洗車"
            checked={parkingOther?.indexOf("可洗車") > -1}
            onChange={() => selectParkingOther("可洗車")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="可供電"
            name="可供電"
            checked={parkingOther?.indexOf("可供電") > -1}
            onChange={() => selectParkingOther("可供電")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
        </div>
      </FormItem>
    </Fragment>
  );
}

export default PageAddPropertyParking;
