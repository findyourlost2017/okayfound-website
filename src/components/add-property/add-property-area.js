import { Fragment } from "react";
import Select from "../input/select";
import FormItem from "../input/form-item";
import Input from "../input/input";
import appConfig from "../../utils/appConfig";

function PageAddPropertyArea({
  saleableArea,
  setSaleableArea,
  roomNumber,
  setRoomNumber,
  bathNumber,
  setBathNumber,
  allowPet,
  setAllowPet,
}) {
  return (
    <Fragment>
      <h2 className="text-2xl font-semibold">單位面積</h2>
      <div className="w-14 border-b border-neutral-200 dark:border-neutral-700"></div>

      <FormItem label="實用面積">
        <div className="relative">
          <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
            <span className="text-gray-500">
              <i className="fa-solid fa-ruler-combined"></i>
            </span>
          </div>
          <Input
            className="!pl-10 !pr-10"
            placeholder="請輸入實呎"
            value={saleableArea}
            onChange={(e) => setSaleableArea(e.target.value)}
          />
          <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
            <span className="text-gray-500">呎</span>
          </div>
        </div>
      </FormItem>

      <FormItem label="房間數目">
        <Select
          value={roomNumber}
          onChange={(e) => setRoomNumber(e.target.value)}
        >
          <option value="" disabled>
            請選擇房間數目
          </option>
          {appConfig.roomItems.map((item) => (
            <option value={item.value} key={`${item.value}_option_room`}>
              {item.label}
            </option>
          ))}
        </Select>
      </FormItem>

      <FormItem label="浴室數目">
        <Select
          value={bathNumber}
          onChange={(e) => setBathNumber(e.target.value)}
        >
          <option value="" disabled>
            請選擇浴室數目
          </option>
          {appConfig.bathRoomItems.map((item) => (
            <option value={item.value} key={`${item.value}_option_bath`}>
              {item.label}
            </option>
          ))}
        </Select>
      </FormItem>

      <FormItem label="可否養寵物？">
        <Select value={allowPet} onChange={(e) => setAllowPet(e.target.value)}>
          <option value={0}>不介意</option>
          <option value={1}>介意</option>
        </Select>
      </FormItem>
    </Fragment>
  );
}

export default PageAddPropertyArea;
