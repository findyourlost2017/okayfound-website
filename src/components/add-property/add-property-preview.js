import { Fragment } from "react";
import _ from "lodash";
import PropertyCard from "../property/property-card";
import appConfig from "../../utils/appConfig";

function PageAddPropertyPreview({
  photoList,
  propertyTitle,
  rentPrice,
  propertyType,
  selectedEstate,
  roomNumber,
  saleableArea,
  parkingLocation,
}) {
  return (
    <Fragment>
      <h2 className="text-2xl font-semibold">預覽你的發佈 🎉</h2>
      <div className="w-14 border-b border-neutral-200 dark:border-neutral-700"></div>
      <div>
        <h3 className="text-lg font-semibold mb-4">以下是你的租盤</h3>
        <div className="grid grid-cols-1 gap-6 md:gap-8 sm:grid-cols-2 lg:grid-cols-2 xl:grid-cols-2">
          <PropertyCard
            data={{
              photos: photoList,
              title: propertyTitle,
              estate_address: selectedEstate.estate_address,
              price: rentPrice,
              property_type: propertyType,
              estate_type: selectedEstate.estate_type,
              estate_name: selectedEstate.estate_name,
              number_of_room: _.find(appConfig.roomItems, {
                value: parseInt(roomNumber),
              })?.label,
              saleable_feet: saleableArea,
              parking_location: parkingLocation,
            }}
            disabled={true}
          />
        </div>
      </div>
    </Fragment>
  );
}

export default PageAddPropertyPreview;
