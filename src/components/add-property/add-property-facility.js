import { Fragment } from "react";
import FormItem from "../input/form-item";
import Checkbox from "../input/checkbox-sm";

function PageAddPropertyFacility({ facility, setFacility }) {
  const selectFacility = (item) => {
    const tempFacility = Object.assign([], facility);
    if (tempFacility.indexOf(item) > -1) {
      let index = tempFacility.indexOf(item);
      tempFacility.splice(index, 1);
    } else {
      tempFacility.push(item);
    }
    setFacility(tempFacility);
  };

  return (
    <Fragment>
      <h2 className="text-2xl font-semibold">單位將包括什麼設施？</h2>
      <div className="w-14 border-b border-neutral-200 dark:border-neutral-700"></div>
      <FormItem label="電器">
        <div className="mt-6 grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-5">
          <Checkbox
            label="電視"
            name="電視"
            checked={facility.indexOf("電視") > -1}
            onChange={() => selectFacility("電視")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="冷氣機"
            name="冷氣機"
            checked={facility.indexOf("冷氣機") > -1}
            onChange={() => selectFacility("冷氣機")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="洗衣機"
            name="洗衣機"
            checked={facility.indexOf("洗衣機") > -1}
            onChange={() => selectFacility("洗衣機")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="浴室寶"
            name="浴室寶"
            checked={facility.indexOf("浴室寶") > -1}
            onChange={() => selectFacility("浴室寶")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="熱水爐"
            name="熱水爐"
            checked={facility.indexOf("熱水爐") > -1}
            onChange={() => selectFacility("熱水爐")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="煮食爐"
            name="煮食爐"
            checked={facility.indexOf("煮食爐") > -1}
            onChange={() => selectFacility("煮食爐")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="雪櫃"
            name="雪櫃"
            checked={facility.indexOf("雪櫃") > -1}
            onChange={() => selectFacility("雪櫃")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="微波爐"
            name="微波爐"
            checked={facility.indexOf("微波爐") > -1}
            onChange={() => selectFacility("微波爐")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="抽油煙機"
            name="抽油煙機"
            checked={facility.indexOf("抽油煙機") > -1}
            onChange={() => selectFacility("抽油煙機")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="焗爐"
            name="焗爐"
            checked={facility.indexOf("焗爐") > -1}
            onChange={() => selectFacility("焗爐")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
        </div>
      </FormItem>

      <FormItem label="傢俬">
        <div className="mt-6 grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-5">
          <Checkbox
            label="地台"
            name="地台"
            checked={facility.indexOf("地台") > -1}
            onChange={() => selectFacility("地台")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="梳化"
            name="梳化"
            checked={facility.indexOf("梳化") > -1}
            onChange={() => selectFacility("梳化")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="床"
            name="床"
            checked={facility.indexOf("床") > -1}
            onChange={() => selectFacility("床")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="衣櫃"
            name="衣櫃"
            checked={facility.indexOf("衣櫃") > -1}
            onChange={() => selectFacility("衣櫃")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="鞋櫃"
            name="鞋櫃"
            checked={facility.indexOf("鞋櫃") > -1}
            onChange={() => selectFacility("鞋櫃")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="組合櫃"
            name="組合櫃"
            checked={facility.indexOf("組合櫃") > -1}
            onChange={() => selectFacility("組合櫃")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="飯枱"
            name="飯枱"
            checked={facility.indexOf("飯枱") > -1}
            onChange={() => selectFacility("飯枱")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="書枱"
            name="書枱"
            checked={facility.indexOf("書枱") > -1}
            onChange={() => selectFacility("書枱")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
        </div>
      </FormItem>
      <FormItem label="其他">
        <div className="mt-6 grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-5">
          <Checkbox
            label="Wifi"
            name="Wifi"
            checked={facility.indexOf("Wifi") > -1}
            onChange={() => selectFacility("Wifi")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
        </div>
      </FormItem>
    </Fragment>
  );
}

export default PageAddPropertyFacility;
