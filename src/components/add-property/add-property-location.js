import { Fragment, useState } from "react";
import moment from "moment";
import FormItem from "../input/form-item";
import Input from "../input/input";
import Select from "../input/select";
import AutoCompleteInput from "../input/auto-complete-input";
import API from "../../utils/api";
import appConfig from "../../utils/appConfig";

function PageAddPropertyLocation({
  propertyType,
  setPropertyEstateID,
  propertySchoolNet,
  setPropertySchoolNet,
  propertyBuildingName,
  setPropertyBuildingName,
  propertyAge,
  setPropertyAge,
  propertyFloor,
  setPropertyFloor,
  setSelectedEstate,
}) {
  const [options, setOptions] = useState([]);

  const onSearchAddress = async (input) => {
    setPropertyAge("");
    setPropertySchoolNet("");
    setSelectedEstate();
    const api = new API();
    const response = await api.fetchEstate({
      input,
      propertyType: propertyType,
    });
    if (response && response.status === 200) {
      setOptions(response.data.estates);
    }
  };

  const onSelectEstate = (selected) => {
    setPropertyEstateID(selected.estate_id);
    if (selected.estate_entrance_year)
      setPropertyAge(moment().format("YYYY") - selected.estate_entrance_year);
    setPropertySchoolNet(selected.school);
    setPropertyBuildingName(selected.estate_full_name);
    setSelectedEstate(selected);
  };

  return (
    <Fragment>
      <h2 className="text-2xl font-semibold">
        你的{propertyType === 0 ? "單位" : "車位"}位置是？
      </h2>
      <div className="w-14 border-b border-neutral-200 dark:border-neutral-700" />
      <FormItem label="大廈名稱">
        <AutoCompleteInput
          options={options}
          defaultValue={propertyBuildingName}
          onChange={onSearchAddress}
          onSelectEstate={onSelectEstate}
        />
      </FormItem>
      {propertyType === 0 && (
        <FormItem label="樓齡(可選填)">
          <Input
            placeholder="請輸入樓齡"
            value={propertyAge}
            onChange={(e) => setPropertyAge(e.target.value)}
          />
        </FormItem>
      )}
      {propertyType === 0 && (
        <FormItem label="校網(可選填)">
          <Select
            value={propertySchoolNet}
            onChange={(e) => setPropertySchoolNet(e.target.value)}
          >
            <option value="" disabled>
              請選擇校網
            </option>

            {appConfig.schoolNet.map((item) => (
              <option
                value={item.value}
                key={`${item.value}_school_net_option`}
              >
                {item.label}
              </option>
            ))}
          </Select>
        </FormItem>
      )}
      {propertyType === 0 && (
        <FormItem label="樓層">
          <Select
            value={propertyFloor}
            onChange={(e) => setPropertyFloor(e.target.value)}
          >
            <option value="" disabled>
              請選擇樓層
            </option>
            <option value="低層">低層</option>
            <option value="中層">中層</option>
            <option value="高層">高層</option>
          </Select>
        </FormItem>
      )}
    </Fragment>
  );
}

export default PageAddPropertyLocation;
