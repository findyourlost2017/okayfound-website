import { Fragment, useRef } from "react";
import FormItem from "../input/form-item";
import Select from "../input/select";
import Input from "../input/input";

function PageAddPropertyPlan({
  isPrePaid,
  prePaidMonth,
  prePaidRentDiscount,
  isBestTalent,
  contractReducePrice,
  setIsPrePaid,
  setPrePaidMonth,
  setPrePaidRentDiscount,
  setIsBestTalent,
  setContractReducePrice,
}) {
  return (
    <Fragment>
      <h2 className="text-2xl font-semibold">優惠計劃</h2>
      <div className="w-14 border-b border-neutral-200 dark:border-neutral-700"></div>

      {/* 優質租客計劃 */}
      <span className="block text-lg font-semibold">優質租客計劃</span>
      <div className="flow-root">
        <div className="-my-3 divide-y divide-neutral-100 dark:divide-neutral-800">
          <div className="flex items-center justify-between py-3">
            <span className="text-neutral-6000 dark:text-neutral-400 font-medium">
              你可選擇讓租客提供過往已打釐印的租約，以換取租金減免。以 2
              份舊租約為上限。
            </span>
            <label
              htmlFor="is-best-talent"
              className="inline-flex relative items-center cursor-pointer"
            >
              <input
                type="checkbox"
                value={isBestTalent === "Y"}
                onChange={(e) => {
                  setIsBestTalent(e.target.checked ? "Y" : "N");
                  setContractReducePrice("");
                }}
                id="is-best-talent"
                className="sr-only peer"
              />
              <div className="w-11 h-6 bg-gray-200 rounded-full peer  peer-focus:ring-4 peer-focus:ring-orange-300 dark:peer-focus:ring-yellow-800 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-orange-500"></div>
            </label>
          </div>
        </div>
      </div>

      {isBestTalent === "Y" && (
        <FormItem label="一次性回贈金額">
          <div className="relative">
            <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
              <span className="text-gray-500">$</span>
            </div>
            <Input
              className="!pl-8 !pr-10"
              placeholder="0.00"
              value={contractReducePrice}
              onChange={(e) => setContractReducePrice(e.target.value)}
            />
            <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
              <span className="text-gray-500">HKD</span>
            </div>
          </div>
        </FormItem>
      )}

      <div className="w-14 border-b border-neutral-200 dark:border-neutral-700"></div>

      {/* 一次繳交數月租金計劃 */}
      <span className="block text-lg font-semibold">一次繳交數月租金計劃</span>
      <div className="flow-root">
        <div className="-my-3 divide-y divide-neutral-100 dark:divide-neutral-800">
          <div className="flex items-center justify-between py-3">
            <span className="text-neutral-6000 dark:text-neutral-400 font-medium">
              你可選擇讓租客一次繳交數個月租金，以換取租金折扣。
            </span>
            <label
              htmlFor="is-pre-paid"
              className="inline-flex relative items-center cursor-pointer"
            >
              <input
                type="checkbox"
                checked={isPrePaid}
                onChange={(e) => {
                  setIsPrePaid(e.target.checked);
                  setPrePaidMonth("");
                  setPrePaidRentDiscount("");
                }}
                id="is-pre-paid"
                className="sr-only peer"
              />
              <div className="w-11 h-6 bg-gray-200 rounded-full peer  peer-focus:ring-4 peer-focus:ring-orange-300 dark:peer-focus:ring-yellow-800 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-orange-500"></div>
            </label>
          </div>
        </div>
      </div>

      {isPrePaid && (
        <FormItem label="預繳月份">
          <Select
            value={prePaidMonth}
            onChange={(e) => setPrePaidMonth(e.target.value)}
          >
            <option value="" disabled>
              請選擇預繳月份
            </option>
            <option value={3}>預繳3個月</option>
            <option value={6}>預繳6個月</option>
            <option value={9}>預繳9個月</option>
            <option value={12}>預繳12個月</option>
            <option value={15}>預繳15個月</option>
            <option value={18}>預繳18個月</option>
            <option value={24}>預繳24個月</option>
          </Select>
        </FormItem>
      )}

      {isPrePaid && (
        <FormItem label="預繳後租金">
          <Input
            placeholder="請輸入折扣後租金"
            value={prePaidRentDiscount}
            onChange={(e) => setPrePaidRentDiscount(e.target.value)}
          />
        </FormItem>
      )}
    </Fragment>
  );
}

export default PageAddPropertyPlan;
