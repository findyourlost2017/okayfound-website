import { Fragment } from "react";
import FormItem from "../input/form-item";
import Select from "../input/select";
import Checkbox from "../input/checkbox-sm";
import Input from "../input/input";
import appConfig from "../../utils/appConfig";

function PageAddPropertyType({
  propertyType,
  setPropertyType,
  rentPrice,
  setRentPrice,
  propertyUsage,
  setPropertyUsage,
  contractType,
  setContractType,
  minimalStart,
  setMinimalStart,
}) {
  const onChangeContract = (item) => {
    const tempContract = Object.assign([], contractType);
    if (tempContract.indexOf(item) > -1) {
      let index = tempContract.indexOf(item);
      tempContract.splice(index, 1);
    } else {
      tempContract.push(item);
    }
    setContractType(tempContract);
  };

  return (
    <Fragment>
      <h2 className="text-2xl font-semibold">物業種類</h2>
      <div className="w-14 border-b border-neutral-200 dark:border-neutral-700"></div>
      <FormItem label="你的物業是？">
        <Select
          value={propertyType}
          onChange={(e) => setPropertyType(parseInt(e.target.value))}
        >
          <option value={0}>住宅</option>
          <option value={2}>車位</option>
        </Select>
      </FormItem>
      {/* 每月租金 */}
      <FormItem label="每月租金">
        <div className="relative">
          <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
            <span className="text-gray-500">$</span>
          </div>
          <Input
            className="!pl-8 !pr-10"
            placeholder="0.00"
            value={rentPrice}
            onChange={(e) => setRentPrice(e.target.value)}
          />
          <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
            <span className="text-gray-500">HKD</span>
          </div>
        </div>
      </FormItem>

      <FormItem label="放盤形式為...(可多選)">
        <div className="mt-6 grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-5">
          <Checkbox
            label="短租"
            name="短租"
            checked={contractType?.indexOf("short") > -1}
            onChange={() => onChangeContract("short")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
          <Checkbox
            label="長租"
            name="長租"
            checked={contractType?.indexOf("long") > -1}
            onChange={() => onChangeContract("long")}
            labelClassName="text-sm"
            inputClassName="h-4 w-4 mt-0.5"
          />
        </div>
      </FormItem>

      <FormItem label="最快起租日">
        <Input
          type="date"
          defaultValue="MM/YY"
          value={minimalStart}
          onChange={(e) => setMinimalStart(e.target.value)}
        />
      </FormItem>

      {propertyType === appConfig.PROPERTY_TYPE["住宅"] && (
        <FormItem label="你將提供什麼空間作租用？">
          <Select
            value={propertyUsage}
            onChange={(e) => setPropertyUsage(e.target.value)}
          >
            <option value="" disabled selected>
              請選擇空間
            </option>
            <option value="whole">整個住宅</option>
            <option value="room">獨立房間</option>
            <option value="sofa">廳長</option>
          </Select>
        </FormItem>
      )}
    </Fragment>
  );
}

export default PageAddPropertyType;
