import { Fragment } from "react";
import FormItem from "../input/form-item";
import Textarea from "../input/textarea";
import Input from "../input/input";

function PageAddPropertyTitle({
  propertyType,
  propertyTitle,
  setPropertyTitle,
  propertyDesc,
  setPropertyDesc,
  visitTime,
  setVisitTime,
}) {
  return (
    <Fragment>
      <h2 className="text-2xl font-semibold">為你的發佈加上標題</h2>
      <div className="w-14 border-b border-neutral-200 dark:border-neutral-700"></div>
      <FormItem label="標題">
        <Input
          placeholder="請輸入引人注目的標題"
          value={propertyTitle}
          onChange={(e) => setPropertyTitle(e.target.value)}
        />
      </FormItem>
      <FormItem label="簡介">
        <Textarea
          placeholder="請填寫樓盤簡介"
          name="add-property-desc"
          value={propertyDesc}
          onChange={(e) => setPropertyDesc(e.target.value)}
        />
      </FormItem>
      {propertyType !== "車位" && (
        <FormItem label="可以預約睇樓時間">
          <Textarea
            placeholder="請填寫睇樓時間"
            name="visit-time"
            value={visitTime}
            onChange={(e) => setVisitTime(e.target.value)}
          />
        </FormItem>
      )}
    </Fragment>
  );
}

export default PageAddPropertyTitle;
