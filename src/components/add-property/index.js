import AddPropertyType from "./add-property-type";
import AddPropertyArea from "./add-property-area";
import AddPropertyParking from "./add-property-parking";
import AddPropertyFacility from "./add-property-facility";
import AddPropertyPhoto from "./add-property-photo";
import AddPropertyPlan from "./add-property-plan";
import AddPropertyLocation from "./add-property-location";
import AddPropertyTitle from "./add-property-title";
import PageAddPropertyPreview from "./add-property-preview";

export {
  AddPropertyType,
  AddPropertyArea,
  AddPropertyParking,
  AddPropertyFacility,
  AddPropertyPhoto,
  AddPropertyPlan,
  AddPropertyLocation,
  AddPropertyTitle,
  PageAddPropertyPreview,
};
