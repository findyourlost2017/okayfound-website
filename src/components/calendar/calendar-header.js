import moment from "moment";

function CalendarHeader({ currentDate, onChangeDate }) {
  function getCurrentWeek() {
    let current = moment(currentDate);

    let weekStart = current.clone().startOf("isoWeek");
    let days = [];

    for (let i = 0; i <= 6; i++) {
      days.push(moment(weekStart).add(i, "days").format("YYYY-MM-DD"));
    }
    return days;
  }

  const renderItem = (item) => {
    const selected = currentDate === item;
    return (
      <div
        className={`flex group hover:bg-orange-500 hover:shadow-lg hover-light-shadow rounded-lg mx-1 transition-all	duration-300 cursor-pointer justify-center w-16 ${
          selected && "bg-orange-500 relative"
        }`}
        key={`${item}_calendar_date`}
        onClick={() => onChangeDate(item)}
      >
        {selected && (
          <span className="flex h-3 w-3 absolute -top-1 -right-1">
            <span className="animate-ping absolute group-hover:opacity-75 opacity-0 inline-flex h-full w-full rounded-full bg-orange-500 "></span>
            <span className="relative inline-flex rounded-full h-3 w-3 bg-white"></span>
          </span>
        )}
        <div className="flex items-center px-4 py-4">
          <div className="text-center">
            <p
              className={`${
                selected ? "text-white" : "text-gray-900"
              } group-hover:text-white text-sm transition-all	duration-300`}
            >
              {moment(item).format("ddd")}
            </p>
            <p
              className={`${
                selected ? "text-white" : "text-gray-900"
              } group-hover:text-white mt-3 group-hover:font-bold transition-all	duration-300`}
            >
              {moment(item).format("DD")}
            </p>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className="flex bg-white justify-start md:justify-center rounded-lg overflow-x-scroll mx-auto py-4 px-2  md:mx-12 items-center">
      <div
        className="cursor-pointer pr-3"
        onClick={() =>
          onChangeDate(
            moment(currentDate).subtract(7, "d").format("YYYY-MM-DD"),
          )
        }
      >
        <i className="fa-solid fa-chevron-left" />
      </div>
      {getCurrentWeek().map((item) => renderItem(item))}
      <div
        className="cursor-pointer pl-3"
        onClick={() =>
          onChangeDate(moment(currentDate).add(7, "d").format("YYYY-MM-DD"))
        }
      >
        <i className="fa-solid fa-chevron-right" />
      </div>
    </div>
  );
}

export default CalendarHeader;
