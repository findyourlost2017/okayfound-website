const Checkbox = ({
  subLabel = "",
  label = "",
  name,
  className = "",
  inputClassName = "",
  labelClassName = "",
  defaultChecked,
  checked,
  onChange,
}) => {
  return (
    <div className={`flex text-sm sm:text-base ${className}`}>
      <input
        id={name}
        name={name}
        type="checkbox"
        className={`focus:ring-action-primary text-orange-500 border-orange rounded border-orange-500 bg-white  checked:bg-orange-500 focus:bg-white-500 ${inputClassName}`}
        defaultChecked={defaultChecked}
        checked={checked}
        onChange={(e) => onChange && onChange(e.target.checked)}
      />
      {label && (
        <label
          htmlFor={name}
          className="ml-3.5 flex flex-col flex-1 justify-center"
        >
          <span className={`text-neutral-900 ${labelClassName}`}>{label}</span>
          {subLabel && (
            <p className="mt-1 text-neutral-500 dark:text-neutral-400 text-sm font-light">
              {subLabel}
            </p>
          )}
        </label>
      )}
    </div>
  );
};

export default Checkbox;
