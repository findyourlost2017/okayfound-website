import React, { Fragment } from "react";
import { Popover, Transition } from "@headlessui/react";
import _ from "lodash";
import Checkbox from "./checkbox";
import appConfig from "../../utils/appConfig";

const DistrictSelect = ({
  propertyType,
  setPropertyType,
  subPropertyType,
  setSubPropertyType,
}) => {
  let typeOfPropertyText = "";
  if (propertyType !== null || propertyType !== undefined) {
    typeOfPropertyText = appConfig.PROPERTY_TYPE_MAP[propertyType];
    if (subPropertyType && subPropertyType.length > 0) {
      typeOfPropertyText += "(";
      subPropertyType.map((item, index) => {
        if (index !== 0) typeOfPropertyText += ",";
        typeOfPropertyText += appConfig.SUB_PROPERTY_MAPPING[item];
      });
      typeOfPropertyText += ")";
    }
  }

  return (
    <Popover className="flex relative flex-1">
      {({ open }) => (
        <>
          <Popover.Button
            className={`flex text-left w-full flex-shrink-0 items-center nc-hero-field-padding space-x-3 focus:outline-none cursor-pointer ${
              open ? "nc-hero-field-focused" : ""
            }`}
            onClick={() => {
              if (!open) document.querySelector("html")?.click();
            }}
          >
            <div className="text-neutral-300 dark:text-neutral-400">
              <svg
                className="nc-icon-field nc-icon-field-2"
                width="24"
                height="24"
                fill="none"
                viewBox="0 0 24 24"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="1.5"
                  d="M6.75024 19.2502H17.2502C18.3548 19.2502 19.2502 18.3548 19.2502 17.2502V9.75025L12.0002 4.75024L4.75024 9.75025V17.2502C4.75024 18.3548 5.64568 19.2502 6.75024 19.2502Z"
                ></path>
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="1.5"
                  d="M9.74963 15.7493C9.74963 14.6447 10.6451 13.7493 11.7496 13.7493H12.2496C13.3542 13.7493 14.2496 14.6447 14.2496 15.7493V19.2493H9.74963V15.7493Z"
                ></path>
              </svg>
            </div>
            <div className="flex-1">
              <span className="block xl:text-lg font-semibold overflow-hidden">
                <span className="line-clamp-1">
                  {typeOfPropertyText || `租盤類別`}
                </span>
              </span>
              <span className="block mt-1 text-sm text-neutral-400 leading-none font-light ">
                請選擇租盤類別
              </span>
            </div>
          </Popover.Button>
          <Transition
            as={Fragment}
            enter="transition ease-out duration-200"
            enterFrom="opacity-0 translate-y-1"
            enterTo="opacity-100 translate-y-0"
            leave="transition ease-in duration-150"
            leaveFrom="opacity-100 translate-y-0"
            leaveTo="opacity-0 translate-y-1"
          >
            <Popover.Panel className="absolute left-0 z-10 w-full sm:min-w-[340px] max-w-sm bg-white dark:bg-neutral-800 top-full mt-3 py-5 sm:py-6 px-4 sm:px-8 rounded-3xl shadow-xl">
              <div className="">
                {appConfig.propertyType.map((item, index) => (
                  <div
                    key={item.value}
                    className="relative flex flex-col space-y-5"
                  >
                    <div className={`${index > 0 ? "mt-5" : ""}`}>
                      <Checkbox
                        name={item.label}
                        label={item.label}
                        subLabel={item.subLabel}
                        checked={propertyType === item.value}
                        onChange={(selected) => {
                          if (selected) {
                            setPropertyType(item.value);
                            setSubPropertyType([]);
                          } else {
                            setPropertyType();
                            setSubPropertyType([]);
                          }
                        }}
                      />
                    </div>

                    {propertyType === item.value &&
                      item.groups &&
                      item.groups.map((subItem) => (
                        <div key={`${subItem.value}_subItem`} className="ml-10">
                          <Checkbox
                            name={subItem.label}
                            label={subItem.label}
                            checked={
                              subPropertyType.indexOf(subItem.value) > -1
                            }
                            onChange={(selected) => {
                              const temp = [...subPropertyType];
                              if (selected) {
                                temp.push(subItem.value);
                                setSubPropertyType(temp);
                              } else {
                                setSubPropertyType((state) => {
                                  return state.filter(
                                    (e) => e !== subItem.value,
                                  );
                                });
                              }
                            }}
                          />
                        </div>
                      ))}
                  </div>
                ))}
              </div>
            </Popover.Panel>
          </Transition>
        </>
      )}
    </Popover>
  );
};

export default DistrictSelect;
