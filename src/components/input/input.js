import React from "react";

const Input = React.forwardRef(
  (
    {
      className = "",
      sizeClass = "h-11 px-4 py-3",
      fontClass = "text-sm font-normal",
      rounded = "rounded-2xl",
      children,
      type = "text",
      ...args
    },
    ref,
  ) => {
    return (
      <input
        ref={ref}
        type={type}
        className={`block w-full border-neutral-200 focus:border-primary-300 focus:ring focus:ring-primary-200 focus:ring-opacity-50 bg-white ${rounded} ${fontClass} ${sizeClass} ${className}`}
        {...args}
      />
    );
  },
);

Input.displayName = "Input";

export default Input;
