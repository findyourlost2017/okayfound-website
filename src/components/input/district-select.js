import React, { Fragment, useEffect } from "react";
import { Popover, Transition } from "@headlessui/react";
import _ from "lodash";
import Checkbox from "./checkbox";
import appConfig from "../../utils/appConfig";

const DistrictSelect = ({
  onChange,
  propertyDistrict,
  setPropertyDistrict,
}) => {
  let typeOfDistrictText = "";
  if (propertyDistrict && propertyDistrict.length > 0) {
    typeOfDistrictText =
      appConfig.districtID["香港島"].indexOf(propertyDistrict[0]) > -1
        ? "香港島"
        : appConfig.districtID["九龍"].indexOf(propertyDistrict[0]) > -1
          ? "九龍"
          : appConfig.districtID["新界"].indexOf(propertyDistrict[0]) > -1
            ? "新界"
            : "離島";
    // typeOfDistrictText = typeOfDistrict
    //   .map((item) => {
    //     return item;
    //   })
    //   .join(", ");
  }

  const countSelectedDistrict = (title) => {
    let count = 0;
    appConfig.districtOption[title].forEach((item) => {
      if (_.intersection(propertyDistrict, item.id).length > 0) count += 1;
    });
    return count;
  };

  return (
    <Popover className="flex relative flex-1">
      {({ open }) => (
        <>
          <Popover.Button
            className={`flex text-left w-full flex-shrink-0 items-center nc-hero-field-padding space-x-3 focus:outline-none cursor-pointer ${
              open ? "nc-hero-field-focused" : ""
            }`}
            onClick={() => {
              if (!open) document.querySelector("html")?.click();
            }}
          >
            <div className="text-neutral-300 dark:text-neutral-400">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-4 w-4 sm:h-6 sm:w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={1.5}
                  d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z"
                />
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={1.5}
                  d="M15 11a3 3 0 11-6 0 3 3 0 016 0z"
                />
              </svg>
            </div>
            <div className="flex-1">
              <span className="block xl:text-lg font-semibold overflow-hidden">
                <span className="line-clamp-1">
                  {typeOfDistrictText || `區域`}
                </span>
              </span>
              <span className="block mt-1 text-sm text-neutral-400 leading-none font-light ">
                請選擇不同的區域
              </span>
            </div>
          </Popover.Button>
          <Transition
            as={Fragment}
            enter="transition ease-out duration-200"
            enterFrom="opacity-0 translate-y-1"
            enterTo="opacity-100 translate-y-0"
            leave="transition ease-in duration-150"
            leaveFrom="opacity-100 translate-y-0"
            leaveTo="opacity-0 translate-y-1"
          >
            <Popover.Panel className="absolute left-0 z-10 w-full sm:min-w-[340px] max-w-sm bg-white dark:bg-neutral-800 top-full mt-3 py-5 sm:py-6 px-4 sm:px-8 rounded-3xl shadow-xl">
              <div className="">
                {appConfig.districtType.map((item, index) => (
                  <div
                    key={item.value}
                    className="relative flex flex-col space-y-5"
                  >
                    <div className={`${index > 0 ? "mt-5" : ""}`}>
                      <Checkbox
                        name={item.label}
                        label={item.label}
                        subLabel={item.subLabel}
                        checked={countSelectedDistrict(item.value) > 0}
                        onChange={(selected) => {
                          if (selected) {
                            let newAddDistrict = [item.mapKey];
                            // appConfig.districtOption[item.value].map(
                            //   (subDisItem) => {
                            //     newAddDistrict = newAddDistrict.concat(
                            //       subDisItem.id
                            //     );
                            //   }
                            // );
                            setPropertyDistrict(newAddDistrict);
                          } else setPropertyDistrict();
                        }}
                      />
                    </div>

                    {/* Sub District */}
                    {propertyDistrict &&
                      countSelectedDistrict(item.value) > 0 &&
                      appConfig.districtOption[item.value].map((subItem) => (
                        <div key={`${subItem.id}_subItem`} className="ml-10">
                          <Checkbox
                            name={subItem.name}
                            label={subItem.name}
                            checked={
                              _.intersection(propertyDistrict, subItem.id)
                                .length > 0
                            }
                            onChange={() => {
                              if (subItem.id[0] < 0) {
                                if (
                                  propertyDistrict.length === 1 &&
                                  propertyDistrict[0] === subItem.id[0]
                                ) {
                                  setPropertyDistrict([]);
                                }
                              } else {
                                if (
                                  propertyDistrict.length > 0 &&
                                  propertyDistrict[0] !== subItem.id[0]
                                ) {
                                  setPropertyDistrict((state) => {
                                    state = state.concat(subItem.id);
                                    return _.filter(state, (i) => i > 0);
                                  });
                                }
                              }
                            }}
                          />
                        </div>
                      ))}
                  </div>
                ))}
              </div>
            </Popover.Panel>
          </Transition>
        </>
      )}
    </Popover>
  );
};

export default DistrictSelect;
