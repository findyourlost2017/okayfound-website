import { useRef, useEffect, useState } from "react";

const LocationInput = ({
  options,
  defaultValue,
  autoFocus = false,
  onChange,
  onSelectEstate,
  placeHolder = "請選擇大廈",
  desc = "Where are you going?",
  //   className = "nc-flex-1.5",
  className = "",
  sizeClass = "h-11 px-4 py-3",
  fontClass = "text-sm font-normal",
  rounded = "rounded-2xl",
}) => {
  const containerRef = useRef(null);
  const inputRef = useRef(null);

  const [value, setValue] = useState(defaultValue);
  const [showPopover, setShowPopover] = useState(autoFocus);

  useEffect(() => {
    setValue(defaultValue);
  }, [defaultValue]);

  useEffect(() => {
    setShowPopover(autoFocus);
  }, [autoFocus]);

  useEffect(() => {
    if (eventClickOutsideDiv) {
      document.removeEventListener("click", eventClickOutsideDiv);
    }
    showPopover && document.addEventListener("click", eventClickOutsideDiv);
    return () => {
      document.removeEventListener("click", eventClickOutsideDiv);
    };
  }, [showPopover]);

  useEffect(() => {
    if (showPopover && inputRef.current) {
      inputRef.current.focus();
    }
  }, [showPopover]);

  const eventClickOutsideDiv = (event) => {
    if (!containerRef.current) return;
    // CLICK IN_SIDE
    if (!showPopover || containerRef.current.contains(event.target)) {
      return;
    }
    // CLICK OUT_SIDE
    setShowPopover(false);
  };

  const handleSelectLocation = (item) => {
    setValue(item.full_name);
    // onInputDone && onInputDone(item);
    onSelectEstate && onSelectEstate(item);
    setShowPopover(false);
  };

  const renderSearchValue = () => {
    return (
      <>
        {options &&
          options.map((item) => (
            <span
              onClick={() => handleSelectLocation(item)}
              key={item.estate_id}
              className="flex px-4 sm:px-8 items-center space-x-3 sm:space-x-4 py-4 sm:py-5 hover:bg-neutral-100 dark:hover:bg-neutral-700 cursor-pointer"
            >
              <span className="block text-neutral-400">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-4 w-4 sm:h-6 sm:w-6"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={1.5}
                    d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z"
                  />
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={1.5}
                    d="M15 11a3 3 0 11-6 0 3 3 0 016 0z"
                  />
                </svg>
              </span>
              <span className="block font-medium text-neutral-700 dark:text-neutral-200">
                {item.full_name}
              </span>
            </span>
          ))}
      </>
    );
  };

  const renderEmptyValue = () => {
    return (
      <div className="relative rounded-lg  p-4 text-center">
        <h2 className="text-lg font-medium">找不到大廈資料...</h2>
        <p className="mt-4 text-sm text-gray-500">請選擇以下樓盤類型</p>
        <div className="flex justify-center">
          <button
            onClick={() => {
              setValue("單幢樓");
              onChange && onChange("單幢樓");
            }}
            className="mt-5 mr-4 inline-flex items-center justify-center rounded-lg bg-neutral-400 px-4 py-1.5 font-medium text-white hover:bg-neutral-500"
          >
            單幢樓
          </button>

          <button
            onClick={() => {
              setValue("村屋");
              onChange && onChange("村屋");
            }}
            className="mt-5 inline-flex items-center justify-center rounded-lg bg-neutral-400 px-4 py-1.5 font-medium text-white hover:bg-neutral-500"
          >
            村屋
          </button>
        </div>
      </div>
    );
  };

  return (
    <div className={`relative flex ${className}`} ref={containerRef}>
      <div
        onClick={() => setShowPopover(true)}
        className={`flex flex-1 relative flex-shrink-0 items-center space-x-3 cursor-pointer focus:outline-none text-left  ${
          showPopover ? "nc-hero-field-focused" : ""
        }`}
      >
        <div className="flex-grow">
          <input
            type="text"
            className={`block w-full border-neutral-200 focus:border-primary-300 focus:ring focus:ring-primary-200 focus:ring-opacity-50 bg-white  ${rounded} ${fontClass} ${sizeClass} ${className}`}
            placeholder={placeHolder}
            value={value}
            autoFocus={showPopover}
            onChange={(e) => {
              setValue(e.currentTarget.value);
              onChange && onChange(e.currentTarget.value);
            }}
            ref={inputRef}
          />

          {/* {value && showPopover && (
            <ClearDataButton
              onClick={() => {
                setValue("");
                onChange && onChange("");
              }}
            />
          )} */}
        </div>
      </div>
      {showPopover && value && (
        <div className="absolute left-0 z-40 w-full min-w-[300px] sm:min-w-[500px] bg-white dark:bg-neutral-800 top-full mt-3 py-3 sm:py-6 rounded-3xl shadow-xl max-h-96 overflow-y-auto">
          {renderSearchValue()}
        </div>
      )}
      {value && (!options || (options && options.length === 0)) && (
        <div className="absolute left-0 z-40 w-full min-w-[300px] sm:min-w-[500px] bg-white dark:bg-neutral-800 top-full mt-3 py-3 sm:py-6 rounded-3xl shadow-xl max-h-96 overflow-y-auto">
          {renderEmptyValue()}
        </div>
      )}
    </div>
  );
};

export default LocationInput;
