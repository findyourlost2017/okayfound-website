import HomeListingHeader from "./home-listing-header";
import PropertyCardH from "../property/property-card-h";

function HomeListing({
  list,
  propertyType,
  tabActiveState,
  setTabActiveState,
}) {
  const renderCard = (item, index) => {
    return (
      <PropertyCardH
        key={`${item.property_id}_${index}_home`}
        className="h-full"
        item={item}
      />
    );
  };

  return (
    <div className="relative py-16">
      {/* Background */}
      <div
        className="nc-BackgroundSection absolute inset-y-0 w-screen xl:max-w-[1340px] 2xl:max-w-screen-2xl left-1/2 transform -translate-x-1/2 xl:rounded-[40px] z-0 bg-neutral-100 dark:bg-black dark:bg-opacity-20"
        data-nc-id="BackgroundSection"
      />

      <div className="nc-SectionGridFeatureProperty relative">
        <HomeListingHeader
          propertyType={propertyType}
          tabActiveState={tabActiveState}
          setTabActiveState={setTabActiveState}
        />
        <div className="grid gap-6 md:gap-8 grid-cols-1 sm:grid-cols-1 xl:grid-cols-2 ">
          {list && list.map(renderCard)}
        </div>
      </div>
    </div>
  );
}

export default HomeListing;
