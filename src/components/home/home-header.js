import { useEffect } from "react";
// import Image from "next/image";

import PropertySearchForm from "../property/property-search-form";

function HomeHeader({
  price,
  setPrice,
  propertyType,
  setPropertyType,
  subPropertyType,
  setSubPropertyType,
  propertyDistrict,
  setPropertyDistrict,
  searchProperty,
}) {
  useEffect(() => {
    const $body = document.querySelector("body");
    if (!$body) return;
    $body.classList.add("theme-cyan-blueGrey");
    return () => {
      $body.classList.remove("theme-cyan-blueGrey");
    };
  }, []);

  return (
    <div className={`nc-SectionHero2 relative`} data-nc-id="SectionHero2">
      <div className="absolute inset-y-0 w-5/6 xl:w-3/4 right-0 flex-grow">
        <img
          className="absolute inset-0 object-cover w-full h-full"
          src={"/images/hero-right-3.png"}
          layout="fill"
          priority={false}
          alt="hero"
        />
      </div>
      <div className="relative py-14 lg:py-20">
        <div className="relative inline-flex">
          <div className="w-screen right-20 md:right-52 inset-y-0 absolute bg-orange-400"></div>
          <div className="relative max-w-3xl inline-flex flex-shrink-0 flex-col items-start py-16 sm:py-20 lg:py-24 space-y-8 sm:space-y-10 text-white">
            <h2 className="font-semibold text-4xl md:text-5xl xl:text-7xl !leading-[110%]">
              業主租客免佣
              <br />
              可自行安排睇樓搵樓
            </h2>
          </div>
        </div>
        <div className="hidden lg:block lg:mt-20 w-full">
          <PropertySearchForm
            price={price}
            setPrice={setPrice}
            propertyType={propertyType}
            setPropertyType={setPropertyType}
            subPropertyType={subPropertyType}
            setSubPropertyType={setSubPropertyType}
            propertyDistrict={propertyDistrict}
            setPropertyDistrict={setPropertyDistrict}
            searchProperty={searchProperty}
          />
        </div>
      </div>
    </div>
  );
}

export default HomeHeader;
