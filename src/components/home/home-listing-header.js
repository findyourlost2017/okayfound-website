import Link from "next/link";
import { Nav, NavItem, Heading } from "../common";
import ButtonSecondary from "../button/button-secondary";
import appConfig from "../../utils/appConfig";

function HomeListingHeader({
  propertyType,
  tabActiveState,
  setTabActiveState,
}) {
  const handleClickTab = (item) => {
    setTabActiveState(item);
  };

  const tabs = ["住宅", "車位"];
  return (
    <div className="flex flex-col mb-8 relative">
      <Heading desc="提供全香港各區最新及最熱門屋苑、單幢式大廈、工商廈及車位等租盤。">
        最新租盤
      </Heading>
      <div className="flex items-center justify-between">
        <Nav
          className="sm:space-x-2"
          containerClassName="relative flex w-full overflow-x-auto text-sm md:text-base hiddenScrollbar"
        >
          {tabs.map((item, index) => (
            <NavItem
              key={index}
              isActive={tabActiveState === item}
              onClick={() => handleClickTab(item)}
            >
              {item}
            </NavItem>
          ))}
        </Nav>
        <Link
          href={
            propertyType === appConfig.PROPERTY_TYPE["住宅"]
              ? "/list?property_type=0"
              : "/list?property_type=2"
          }
        >
          <span className="block flex-shrink-0">
            <ButtonSecondary className="!leading-none">
              <span>查看所有</span>
              <i className="ml-3 las la-arrow-right text-xl"></i>
            </ButtonSecondary>
          </span>
        </Link>
      </div>
    </div>
  );
}

export default HomeListingHeader;
