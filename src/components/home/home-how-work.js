import Link from "next/link";
import { useRouter } from "next/router";
import { Heading, NcImage } from "../common";

function HomeHowWork() {
  const router = useRouter();

  const showSearchLand = () => {
    // router.push("https://www1.iris.gov.hk/eservices/common/selectuser.jsp");
  };

  return (
    <div className="nc-SectionHowItWork">
      <Heading
        isCenter
        desc="係租客同業主喺唔經地產代理嘅協助下，雙方直接聯絡及簽租約"
      >
        自己搵樓及放租
      </Heading>
      <div className="mt-20 relative grid md:grid-cols-3 gap-20">
        <img
          className="hidden md:block absolute inset-x-0 top-10"
          src="/images/VectorHIW.svg"
          alt=""
        />

        {/* First */}
        <div className="relative flex flex-col items-center max-w-xs mx-auto">
          <NcImage
            containerClassName="mb-8 max-w-[200px] mx-auto"
            src="/images/book-search.jpg"
          />

          <Link href="https://www1.iris.gov.hk/eservices/common/selectuser.jsp">
            <a
              className="text-center mt-auto"
              target="_blank"
              onClick={showSearchLand}
            >
              <h3 className="text-xl font-semibold">查冊</h3>
              <span className="block mt-5 text-neutral-500 dark:text-neutral-400">
                經土地註冊處可查閱相關物業資料、買賣合約等，顯示單位是否有僭建及核對業主身份，費用為$10港元/次
              </span>
            </a>
          </Link>
        </div>

        <div className="relative flex flex-col items-center max-w-xs mx-auto">
          <NcImage
            containerClassName="mb-8 max-w-[200px] mx-auto"
            src="/images/died.jpg"
          />
          <Link href="https://www.property.hk/unlucky/0/H/0/">
            <a className="text-center mt-auto" target="_blank">
              <h3 className="text-xl font-semibold">查兇宅</h3>
              <span className="block mt-5 text-neutral-500 dark:text-neutral-400">
                查看單位是否凶宅可透過各大凶宅網、及參考銀行對單位的估價，兇宅租金最多可有三至四成折讓
              </span>
            </a>
          </Link>
        </div>

        <div className="relative flex flex-col items-center max-w-xs mx-auto">
          <NcImage
            containerClassName="mb-8 max-w-[200px] mx-auto"
            src="/images/stamping.jpg"
          />
          <Link href="https://www.gov.hk/tc/residents/taxes/etax/services/stamping_of_document.htm">
            <a className="text-center mt-auto" target="_blank">
              <h3 className="text-xl font-semibold">打釐印</h3>
              <span className="block mt-5 text-neutral-500 dark:text-neutral-400">
                在雙方自行商討細節、免租期及簽租約後，而租約向稅務局繳完「釐印費」及打過釐印後才有具法律效力
              </span>
            </a>
          </Link>
        </div>
      </div>
    </div>
  );
}

export default HomeHowWork;
