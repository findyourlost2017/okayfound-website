import Link from "next/link";

function HomeDownloadApp() {
  return (
    <div className="relative pb-0 pt-24 lg:py-32 xl:py-40 2xl:py-48">
      <div
        className={`nc-BackgroundSection absolute inset-y-0 w-screen xl:max-w-[1340px] 2xl:max-w-screen-2xl left-1/2 transform -translate-x-1/2 xl:rounded-[40px] z-0 bg-neutral-100 bg-opacity-80`}
        data-nc-id="BackgroundSection"
      >
        <img
          className="absolute inset-0 w-full h-full object-cover rounded-3xl object-right"
          src="/images/dowloadAppBG.png"
          alt="download App Png"
          priority={false}
          layout="fill"
        />
      </div>
      <div className="relative inline-block ">
        <h2 className="text-5xl md:text-6xl xl:text-7xl font-bold text-neutral-800">
          下載手機程式
        </h2>
        <span className="block mt-7 max-w-md text-neutral-6000">
          租屋台香港租屋免佣網，無任何佣金／收費
        </span>
        <div className="flex space-x-3 mt-10 sm:mt-14">
          <Link href="https://apps.apple.com/app/%E7%A7%9F%E5%B1%8B%E5%8F%B0/id1521785785">
            <a className="mr-4" target="_blank" rel="noopener noreferrer">
              <img src="/images/btn-ios.png" alt="" width={145} height={47} />
            </a>
          </Link>
          <Link href="https://play.google.com/store/apps/details?id=com.okayfound.app">
            <a target="_blank" rel="noopener noreferrer">
              <img
                src="/images/btn-android.png"
                alt="android download okayfound"
                width={145}
                height={47}
              />
            </a>
          </Link>
        </div>

        <img
          className="hidden lg:block absolute lg:left-full lg:top-0 xl:top-1/2 z-10  lg:max-w-sm 2xl:max-w-none"
          src="./images/appSvg1.png"
          alt=""
        />

        {/* <div className="block lg:hidden mt-10 max-w-2xl rounded-3xl overflow-hidden">
          <img src="./images/appRightImg.png" alt="" />
        </div> */}
      </div>
    </div>
  );
}

export default HomeDownloadApp;
