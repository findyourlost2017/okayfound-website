import { useEffect } from "react";
import { useFilePicker } from "use-file-picker";
import appConfig from "../../utils/appConfig";
import NcModal from "../common/nc-modal";
import AddTimetableModal from "./add-timetable-modal";

function ChatFooter({
  onChangeInput,
  height,
  inputRef,
  input,
  onSendClick,
  onSendMessage,
  onAddTimetable,
}) {
  const [openFileSelector, { plainFiles, loading, errors }] = useFilePicker({
    accept: [".png", ".jpg", ".jpeg"],
    multiple: false,
    limitFilesConfig: { max: 1 },
    maxFileSize: 15,
  });

  useEffect(() => {
    if (plainFiles && plainFiles.length > 0)
      onSendMessage(plainFiles[0], appConfig.MESSAGE_TYPE.image);
  }, [plainFiles]);

  return (
    <div className="relative flex flex-none border-t border-solid">
      <div className="w-full py-1 px-1.5">
        <textarea
          type="text"
          row="1"
          name="chat-input"
          placeholder="輸入訊息"
          value={input}
          ref={inputRef}
          onChange={onChangeInput}
          style={{ height: height }}
          onKeyDown={(evt) => {
            const keyCode = evt.key;
            if (event.key === "Enter") {
              evt.preventDefault();
              onSendClick();
            }
          }}
          className="resize-none max-h-60 border-transparent focus:border-transparent focus:ring-0 w-9/12 focus:outline-none focus:placeholder-gray-400 text-gray-600 placeholder-gray-600 rounded-none py-3 border-none"
        />
      </div>
      <div className="absolute right-0 items-center inset-y-0 hidden sm:flex">
        <NcModal
          renderTrigger={(openModal) => (
            <button
              type="button"
              onClick={openModal}
              className="group relative inline-flex items-center justify-center rounded-full h-10 w-10 mr-1 transition duration-500 ease-in-out text-gray-500 hover:bg-gray-300 focus:outline-none"
            >
              <i className="fa-solid fa-calendar-circle-plus" />
              <div className="opacity-0 w-24 bg-black text-white text-center text-xs rounded-lg py-2 absolute z-10 group-hover:opacity-100 bottom-full  pointer-events-none">
                預約行程
              </div>
            </button>
          )}
          renderContent={(closeModal) => (
            <AddTimetableModal
              onAddTimetable={onAddTimetable}
              closeModal={closeModal}
            />
          )}
          modalTitle="新增預約行程"
          contentExtraClass="w-6/12 min-h-0"
        />

        <button
          type="button"
          onClick={openFileSelector}
          className="group relative inline-flex items-center justify-center rounded-full h-10 w-10 mr-1 transition duration-500 ease-in-out text-gray-500 hover:bg-gray-300 focus:outline-none"
        >
          <i className="fa-solid fa-image"></i>
          <div className="opacity-0 w-24 bg-black text-white text-center text-xs rounded-lg py-2 absolute z-10 group-hover:opacity-100 bottom-full  pointer-events-none">
            傳送相片
          </div>
        </button>
        <button
          type="button"
          onClick={onSendClick}
          className="inline-flex items-center rounded justify-center px-4 py-3 mr-2 transition duration-500 ease-in-out text-white bg-orange-500 hover:bg-orange-400 focus:outline-none"
        >
          {/* <span className="font-bold">傳送</span> */}
          <i className="fa-solid fa-paper-plane-top transform"></i>
        </button>
      </div>
    </div>
  );
}

export default ChatFooter;
