import moment from "moment";
import Image from "next/image";
import appConfig from "../../utils/appConfig";
import ButtonSecondary from "../../components/button/button-secondary";

function MessagesList({
  list,
  member,
  loading,
  currentChat,
  onShowMessageContent,
  onShowMoreFriend,
}) {
  const generateMessageTime = (item) => {
    if (
      moment(item?.message?.created_at).format("YYYY-MM-DD") ===
      moment().format("YYYY-MM-DD")
    ) {
      return moment(item?.message?.created_at).format("HH:mm");
    } else {
      return moment(item?.message?.created_at).format("DD/MM");
    }
  };

  return (
    <div
      className={`flex-grow bg-white border-r border-gray-300 overflow-y-auto ${
        currentChat && "hidden md:block"
      } `}
    >
      <div className="p-4 border-0	border-none">
        <h2 className="text-2xl font-semibold">聊天記錄</h2>
        <div className="mt-3 w-full border-b border-neutral-200" />
        <div className=" divide-neutral-100 mt-0">
          {list &&
            list.map((item) => (
              <div
                key={`${item.friend_id}_messages`}
                className="flex items-center border-b py-4 cursor-pointer "
                // onClick={() => setCurrentChat(item)}
                onClick={() => onShowMessageContent(item)}
              >
                <Image
                  src={item?.property?.thumbnail}
                  alt={item?.property?.title}
                  height={80}
                  width={80}
                  className="rounded"
                  objectFit="cover"
                />
                <div className="mx-4">
                  <p className="text-sm font-semibold">
                    {item?.property?.title}
                  </p>
                  <p className="text-sm text-neutral-500  mt-0.5">
                    {item?.property?.member_id === member.member_id
                      ? `與${
                          item?.member?.member_name
                            ? item?.member?.member_name
                            : "詢問者"
                        }的對話`
                      : "與業主的對話"}
                  </p>
                  <span className="block text-neutral-6000 text-sm">
                    {parseInt(item.message.message_type) ===
                    appConfig.MESSAGE_TYPE["text"]
                      ? item?.message?.text
                      : "圖片"}
                  </span>
                </div>

                <div className="ml-auto">
                  <p
                    className={`${
                      item.unread_count > 0 ? "mb-3" : "mb-10"
                    } text-xs text-gray-500 right-0 top-0 mr-4`}
                  >
                    {generateMessageTime(item)}
                  </p>
                  {item.unread_count > 0 && (
                    <span className="inline-block w-2 h-2 ml-4 bg-orange-500 rounded-full"></span>
                  )}
                </div>
              </div>
            ))}
        </div>
        <div className="pt-4 text-center">
          <ButtonSecondary onClick={onShowMoreFriend} loading={loading}>
            查看更多對話
          </ButtonSecondary>
        </div>
      </div>
    </div>
  );
}

export default MessagesList;
