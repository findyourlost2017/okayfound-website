import { useState } from "react";
import DatePicker from "react-datepicker";
import FormItem from "../input/form-item";
import ButtonPrimary from "../button/button-primary";

import "react-datepicker/dist/react-datepicker.css";

function AddTimetableModal({ closeModal, onAddTimetable }) {
  console.log(closeModal);
  const [startDate, setStartDate] = useState(new Date());

  const addTimetable = () => {
    onAddTimetable(startDate);
    closeModal();
  };

  return (
    <div className="h-96">
      <FormItem label="睇樓時間">
        <DatePicker
          showTimeSelect
          minDate={new Date()}
          dateFormat="dd/MM/yyyy  HH:mm"
          timeFormat="HH:mm"
          timeIntervals={15}
          selected={startDate}
          onChange={(date) => setStartDate(date)}
        />
      </FormItem>

      <div className="absolute bottom-10 space-x-5 w-11/12 items-center">
        <ButtonPrimary className="w-full items-center" onClick={addTimetable}>
          確定
        </ButtonPrimary>
      </div>
    </div>
  );
}

export default AddTimetableModal;
