import { useState } from "react";
import _ from "lodash";
import ChatItem from "./chat-item";
import ChatHeader from "./chat-header";
import ChatFooter from "./chat-footer";
import appConfig from "../../utils/appConfig";
import ModalPhotos from "../modal/photos";

function MessagesContent({
  currentChat,
  member,
  input,
  setInput,
  inputRef,
  messages,
  onSendMessage,
  scrollContentRef,
  timetable,
  onAddTimetable,
}) {
  const [height, setHeight] = useState(50);
  const [isOpen, setIsOpen] = useState(false);
  const [openFocusIndex, setOpenFocusIndex] = useState(0);

  const handleOpenModal = (txt) => {
    const findIndex = _.findIndex(
      _.filter(
        messages,
        (i) => i.message_type === appConfig.MESSAGE_TYPE.image,
      ),
      { text: txt },
    );
    if (findIndex > -1) {
      setIsOpen(true);
      setOpenFocusIndex(findIndex);
    }
  };

  const handleCloseModal = () => setIsOpen(false);

  const onChangeInput = (e) => {
    const inputLength = e.target.value.split(/\r|\r\n|\n/).length;

    setInput(e.target.value);
    if (inputLength > 1) setHeight(50 + inputLength * 12);
  };

  const onSendClick = () => {
    if (input && /\S/.test(input) && input.length > 0) {
      onSendMessage(input, appConfig.MESSAGE_TYPE.text);
      setHeight(50);
      setInput("");
    }
  };

  return (
    <div className="flex-1 flex-col justify-between flex">
      {/* Header */}
      <ChatHeader
        currentChat={currentChat}
        member={member}
        timetable={timetable}
      />
      {/* Content */}
      <div
        id="messages"
        ref={scrollContentRef}
        className="flex-1 flex flex-col space-y-1 py-3 overflow-y-auto scrollbar-thumb-blue scrollbar-thumb-rounded scrollbar-track-blue-lighter scrollbar-w-2 scrolling-touch"
      >
        {messages &&
          messages.map((item, index) => {
            const previousMessage = messages[index - 1]
              ? messages[index - 1]
              : null;

            return (
              <ChatItem
                member={member}
                item={item}
                previousMessage={previousMessage}
                handleOpenModal={handleOpenModal}
                key={`${item._id}_chat_message`}
              />
            );
          })}
      </div>

      {/* Footer */}
      <ChatFooter
        height={height}
        inputRef={inputRef}
        input={input}
        setInput={setInput}
        onSendMessage={onSendMessage}
        onChangeInput={onChangeInput}
        onSendClick={onSendClick}
        onAddTimetable={onAddTimetable}
      />

      <ModalPhotos
        imgs={_(messages)
          .filter((i) => i.message_type === appConfig.MESSAGE_TYPE.image)
          .map("text")
          .value()}
        isOpen={isOpen}
        onClose={handleCloseModal}
        initFocus={openFocusIndex}
        uniqueClassName="nc-ListingStayDetailPage-modalPhotos"
      />
    </div>
  );
}

export default MessagesContent;
