import moment from "moment";
import Image from "next/image";
import appConfig from "../../utils/appConfig";
import Link from "next/link";

function ChatHeader({ currentChat, member, timetable }) {
  function generateDay() {
    if (moment.unix(timetable.booking_at).isSame(moment(), "d")) {
      return `今日 ${moment.unix(timetable.booking_at).format("HH:mm")}`;
    } else if (!moment.unix(timetable.booking_at).isSame(moment(), "week")) {
      return `${moment.unix(timetable.booking_at).format("MM月D號 HH:mm")} (${
        appConfig.weekOfDay[moment.unix(timetable.booking_at).day()]
      })`;
    } else if (
      moment
        .unix(timetable.booking_at)
        .startOf("d")
        .diff(moment().startOf("d"), "days") === 1
    ) {
      return `明天 ${moment.unix(timetable.booking_at).format("HH:mm")}`;
    } else if (
      moment
        .unix(timetable.booking_at)
        .startOf("d")
        .diff(moment().startOf("d"), "days") === 2
    ) {
      return `後日 ${moment.unix(timetable.booking_at).format("HH:mm")}`;
    } else {
      return `${
        appConfig.weekOfDay[moment(timetable.booking_at).day()]
      } ${moment.unix(timetable.booking_at).format("HH:mm")}`;
    }
  }

  return (
    <Link href={`/property/${currentChat.property_id}`}>
      <div className="cursor-pointer w-full flex sm:items-center justify-between py-3 border-y border-gray-200">
        <div className="relative flex items-center pl-6 w-full ">
          {/* <NcImage
            className="rounded w-16 h-16 object-cover"
            src={currentChat?.property?.thumbnail}
          /> */}

          <Image
            src={currentChat?.property?.thumbnail}
            height={52}
            width={68}
            className="rounded"
            objectFit="cover"
          />

          <div className="flex flex-col leading-tight ml-4">
            <div className="mt-1 flex items-center">
              <span className="font-semibold text-sm mr-3">
                {currentChat?.property?.title}
              </span>
            </div>
            <span className="text-gray-700 text-sm mr-3">
              HK${appConfig.formatMoney(currentChat?.property.price)}
            </span>
            <span className="text-sm text-gray-600">
              {currentChat?.property?.member_id === member.member_id
                ? `與${
                    currentChat?.member?.member_name
                      ? currentChat?.member?.member_name
                      : "詢問者"
                  }的對話`
                : "與業主的對話"}
            </span>
          </div>
          {/* Booked */}

          <div className="ml-auto">
            {timetable && (
              <span className="bg-orange-500 mr-6 text-white text-xs font-semibold inline-flex items-center px-2.5 py-1.5 rounded mr-2">
                <svg
                  aria-hidden="true"
                  className="mr-1 w-3 h-3"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fill-rule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-12a1 1 0 10-2 0v4a1 1 0 00.293.707l2.828 2.829a1 1 0 101.415-1.415L11 9.586V6z"
                    clip-rule="evenodd"
                  ></path>
                </svg>
                已預約睇樓 {generateDay()}
              </span>
            )}
          </div>
        </div>
      </div>
    </Link>
  );
}

export default ChatHeader;
