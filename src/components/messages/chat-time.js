import moment from "moment";

function ChatTime({ currentMessage, previousMessage }) {
  const isSameTime = () => {
    if (!previousMessage || !previousMessage.created_at) {
      return false;
    }
    const currentCreatedAt = moment(currentMessage.created_at);
    const diffCreatedAt = moment(previousMessage.created_at);

    if (!currentCreatedAt.isValid() || !diffCreatedAt.isValid()) {
      return false;
    }
    return currentCreatedAt.diff(diffCreatedAt, "minutes") > 20 ? false : true;
  };

  if (currentMessage && !isSameTime(currentMessage, previousMessage)) {
    return (
      <div>
        <p className="text-sm text-center">
          {moment(currentMessage.created_at).format("MMMDD日 HH:mm")}
        </p>
      </div>
    );
  } else return null;
}

export default ChatTime;
