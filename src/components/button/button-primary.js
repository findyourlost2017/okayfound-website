import Button from "./button";
import React from "react";

const ButtonPrimary = ({ className = "", ...args }) => {
  return (
    <Button
      className={`ttnc-ButtonPrimary disabled:bg-opacity-70 bg-orange-500 hover:bg-orange-600 text-neutral-50 ${className}`}
      {...args}
    />
  );
};

export default ButtonPrimary;
