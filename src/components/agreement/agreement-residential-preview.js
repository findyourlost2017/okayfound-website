import { useEffect, useState } from "react";
import {
  PDFViewer,
  Document,
  Page,
  Text,
  View,
  StyleSheet,
  Font,
} from "@react-pdf/renderer";

function AgreementPreview({ currentStep, formData }) {
  const [domLoaded, setDomLoaded] = useState(false);

  Font.register({
    family: "PingFang",
    fonts: [
      {
        src: "https://api.okayfound.com/PingFang-SC-Regular.ttf",
        fontWeight: 400,
      },
      {
        src: "https://api.okayfound.com/PingFang-SC-Bold.ttf",
        fontWeight: 700,
      },
    ],
  });

  Font.registerHyphenationCallback((word) => {
    if (word.length === 1) {
      return [word];
    }

    return Array.from(word)
      .map((char) => [char, ""])
      .reduce((arr, current) => {
        arr.push(...current);
        return arr;
      }, []);
  });

  useEffect(() => {
    setDomLoaded(true);
  }, []);

  return (
    <div className={`${currentStep === 3 ? "block" : "hidden"} agreement`}>
      {domLoaded && (
        <PDFViewer width={1100} height={842}>
          <Document>
            <Page size="A4" style={styles.page}>
              <Text style={[styles.title]}>住宅租約</Text>
              <Text style={styles.title}>Residential Tenancy Agreement</Text>
              <View style={styles.contractHeader}>
                <Text style={styles.contractDateTxt}>
                  本合約訂於{" "}
                  <Text style={{ textDecoration: "underline" }}>
                    {formData?.contractDate
                      ? `    ${formData.contractDate}    `
                      : "                          "}
                  </Text>{" "}
                  由業主及租客訂立,雙方資料詳列於附表一。
                </Text>
                <Text style={styles.contractDateEngTxt}>
                  This Agreement is made on{" "}
                  <Text style={{ textDecoration: "underline" }}>
                    {formData?.contractDate
                      ? `    ${formData.contractDate}    `
                      : "                          "}
                  </Text>{" "}
                  Between the Landlord and the Tenant as more particularly
                  described in Schedule I.
                </Text>
              </View>
              <View style={styles.height20} />
              <Text style={styles.contractDateTxt}>
                業主及租客雙方以詳列於附表一的租期及租金分別租出及租入詳列於附表一的物業，並同意遵守及履行下列條款：-
              </Text>
              <Text style={styles.contractDateEngTxt}>
                The Landlord shall let and the Tenant shall take the Premises
                for the Term and at the Rent as more particularly described in
                Schedule I and both parties agree to observe and perform the
                terms and conditions as follow:-
              </Text>
              <View style={[styles.termRow, { marginTop: 20 }]}>
                <Text style={styles.termNumber}>1</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    租客須在租期內每個月份的{" "}
                    <Text
                      style={{ color: "blue", textDecoration: "underline" }}
                    >
                      {formData?.tenancySubmitPriceDay}
                    </Text>{" "}
                    號或以前上期繳付指定的租金予業主。倘租客於應繳租金之日的七天內仍未清付該租金，則業主有權採取適當行動追討租客所欠的租金，而由此而引起的一切費用及開支將構成租客所欠業主的債項，業主將有權向租客一併追討所欠款項全數。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    The Tenant shall pay to the Landlord the Rent in advance on
                    or before{" "}
                    <Text
                      style={{ color: "blue", textDecoration: "underline" }}
                    >
                      {formData?.tenancySubmitPriceDay}
                    </Text>{" "}
                    (st/nd/rd/th) of each and every calendar month during the
                    Term. If the Tenant shall fail to pay the Rent within 7 days
                    from the due date, the Landlord shall have the right to
                    institute appropriate action to recover the Rent and all
                    costs, expenses and other outgoings so incurred by the
                    Landlord in relation to such action shall be a debt owed by
                    the Tenant to the Landlord and shall be recoverable in full
                    by the Landlord.
                  </Text>
                </View>
              </View>
              {/* Term 2 */}
              <View style={styles.termRow}>
                <Text style={styles.termNumber}>2</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    租客在沒有業主書面同意前，不得對該物業作任何改動及/或加建。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    The Tenant shall not make any alteration and / or additions
                    to the Premises without prior written consent of the
                    Landlord.
                  </Text>
                </View>
              </View>
              {/* Term 3 */}
              <View style={styles.termRow}>
                <Text style={styles.termNumber}>3</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    租客不得轉讓、轉租或分租該物業或其他任何部份或將該物業或其他任何部份的佔用權讓予任何其他人等。此租約權益為租客個人擁有。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    The Tenant shall not assign, transfer, sublet or part with
                    the possession of the Premises or any part thereof to any
                    other person. This tenancy shall be personal to the Tenant
                    named therein.
                  </Text>
                </View>
              </View>
              {/* Term 4 */}
              <View style={styles.termRow}>
                <Text style={styles.termNumber}>4</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    租客須遵守香港一干法律條例和規則及該物業所屬的大廈有關的公契內的條款。租客亦不可違反屬該物業地段內的官批地契
                    上的任何制約性條款。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    The Tenant shall comply with all ordinances, regulations and
                    rules of Hong Kong and shall observe and perform the
                    covenants, terms and conditions of the Deed of Mutual
                    Covenant and Sub-Deed of Mutual Covenant (if any) relating
                    to the Premises. The Tenant shall not contravene any
                    negative or restrictive covenants contained in the
                    Government Lease(s) under which the Premises are held from
                    the Government.
                  </Text>
                </View>
              </View>
              {/* Term 5 */}
              <View style={styles.termRow}>
                <Text style={styles.termNumber}>5</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    租客須在租約期內清繳有關物業的水費、電費、煤氣費、電話費及其他類似的雜費。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    The Tenant shall during the Term pay and discharge all
                    charges in respect of water, electricity, gas and telephone
                    and other similar charges payable in respect of the
                    Premises.
                  </Text>
                </View>
              </View>
              {/* Term 6 */}
              <View style={styles.termRow}>
                <Text style={styles.termNumber}>6</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    租客須在租約期內保持物業內部的維修狀態良好(自然損耗及因固有的缺陷所產生的損壞除外)並須於租約期滿或終止時將物業在同樣維修狀態下交吉回業主。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    The Tenant shall during the Term keep the interior of the
                    Premises in good and tenantable repair and condition (fair
                    wear and tear and damage caused by inherent defects
                    excepted) and shall deliver up vacant possession of the
                    Premises in the same repair and condition on the expiration
                    or sooner determination of this Agreement.
                  </Text>
                </View>
              </View>
              {/* Term 7 */}
              <View style={styles.termRow}>
                <Text style={styles.termNumber}>7</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    租客須交予業主保証金(金額如附表一所列)作為保証物業租客遵守及履行此租約上租客所需遵守的條款的按金。若租客在租
                    期內並無干犯此合約內任何條款，則業主須於收回交吉的物業或一切租客欠款後(以較遲者作準)三十天內無息退還該保証金
                    予租客。但若租客拖欠根據此合約需要支付的租金及/或其他款項超過七天(無論有否以法律行動追討)或若租客違反此合約內
                    任何條款，業主可合法收回該物業而此租約將立被終止；業主可從保証金內扣除因租客違約而令業主所受的損失，而此項權
                    利將不會影響業主因租客違約而可採取的其他合法行動的權利。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    The Tenant shall pay to the Landlord the Security Deposit
                    set out in Schedule I for the due observance and performance
                    of the terms and conditions herein contained and on his part
                    to be observed and performed. Provided that there is no
                    antecedent breach of any of the terms and conditions herein
                    contained, the Landlord shall refund the Security Deposit to
                    the Tenant without interest within 30 days from the date of
                    delivery of vacant possession of the Premises to the
                    Landlord or settlement of any outstanding payment owed by
                    the Tenant to the Landlord, whichever is later. If the Rent
                    and/or any charges payable by the Tenant hereunder or any
                    part thereof shall be unpaid for seven (7) days after the
                    same shall become payable (whether legally demanded or not)
                    or if the Tenant shall commit a breach of any of the terms
                    and conditions herein contained, it shall be lawful for the
                    Landlord at any time thereafter to re-enter the Premises
                    whereupon this Agreement shall absolutely determine and Page
                    2 the Landlord may deduct any loss or damage suffered by the
                    Landlord as a result of the Tenant’s breach from the
                    Security Deposit without prejudice to any other right of
                    action or any remedy of the Landlord in respect of such
                    breach of the Tenant.
                  </Text>
                </View>
              </View>
              {/* Term 8 */}
              <View style={styles.termRow}>
                <Text style={styles.termNumber}>8</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    若租客按時清繳租金和雜費及沒有干犯此合約內任何條款，則業主不得在租約期內干擾租客享用該物業。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    Provided the Tenant shall have paid the Rent and other
                    outgoings on the days and in the manner herein provided and
                    observed and perform the terms and conditions herein
                    contained and on the Tenant’s part to be observed and
                    performed, the Tenant shall peacefully hold and enjoy the
                    Premises during the Term without any interruption by the
                    Landlord.
                  </Text>
                </View>
              </View>
              {/* Term 9 */}
              <View style={styles.termRow}>
                <Text style={styles.termNumber}>9</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    業主提供家電或家俬(如附表二所列)予租客使用，租客須於租約期滿或終止時將家電或家俬在良好狀態下交回業主(自然損耗及因固有的缺陷所產生的損壞除外)。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    The Landlord shall provide the applicance(s) and
                    furniture(s) set out in Schedule II for the Tenant to use,
                    the Tenant shall maintain the applicance(s) and furniture(s)
                    in good condition on the expiration or sooner determination
                    of this Agreement(fair wear and tear and damage caused by
                    inherent defects excepted).
                  </Text>
                </View>
              </View>
              {/* Term 10 */}
              <View style={styles.termRow}>
                <Text style={styles.termNumber}>10</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    業主須保養及適當維修物業內各主要結構部份(包括主要的排污渠、喉管和電線)。唯業主須在收到租客的書面要求後才會有責任在合理時限內將有關損壞維修妥當。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    The Landlord shall keep and maintain the structural parts of
                    the Premises including the main drains, pipes and cables in
                    proper state of repair provided that the Landlord’s
                    liability shall not be incurred unless and until written
                    notice of any defect or want of repair has been given by the
                    Tenant to the Landlord and the Landlord shall have failed to
                    take reasonable steps to repair and remedy the same after
                    the lapse of a reasonable time from the date of service of
                    such notice.
                  </Text>
                </View>
              </View>
              {/* Term 11 */}
              <View style={styles.termRow}>
                <Text style={styles.termNumber}>11</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    業主負責繳付有關該物業的物業稅。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    The Landlord shall pay the Property Tax payable in respect
                    of the Premises.
                  </Text>
                </View>
              </View>
              {/* Term 12 */}
              <View style={styles.termRow}>
                <Text style={styles.termNumber}>12</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    業主及租客各負責屬此合約一式兩份的印花稅一半費用。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    The Stamp Duty payable on this Agreement in duplicate shall
                    be borne by the Landlord and the Tenant in equal shares.
                  </Text>
                </View>
              </View>
              {/* Term 13 */}
              <View style={styles.termRow}>
                <Text style={styles.termNumber}>13</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    業主及租客雙方同意遵守附表二的附加條件(如有)。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    The Landlord and the Tenant agree to be bound by the
                    additional terms and conditions in Schedule II (if any).
                  </Text>
                </View>
              </View>
              {/* Term 14 */}
              <View style={styles.termRow}>
                <Text style={styles.termNumber}>14</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    此合約內的英文本與中文本存有差異時，將以英文本為準。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    If there is any conflict between the English version and the
                    Chinese version in this Agreement, the English version shall
                    prevail.
                  </Text>
                </View>
              </View>

              <View style={{ flexDirection: "row", marginTop: 30 }}>
                <View>
                  <Text style={styles.normalTxt}>
                    業主收到租客,{"     "} 港幣
                  </Text>
                  <Text style={styles.normalEngTxt}>Landlord received HKD</Text>
                </View>
                <View
                  style={{
                    marginLeft: 10,
                    marginRight: 10,
                    width: 54,
                    borderBottomWidth: 1,
                  }}
                >
                  <Text
                    style={[
                      styles.normalTxt,
                      {
                        color: "blue",
                        paddingLeft: 10,
                        position: "absolute",
                        top: 3,
                      },
                    ]}
                  >
                    {formData?.rentPrice
                      ? `   $${formData?.rentPrice}`
                      : `          `}
                  </Text>
                </View>
                <View>
                  <Text style={styles.normalTxt}>保証金</Text>
                  <Text style={styles.normalEngTxt}>
                    Security Deposit from Tenant
                  </Text>
                </View>
              </View>
              <Text style={styles.normalTxt}>支票號碼(如有)</Text>
              <Text style={styles.normalEngTxt}>Check #(If Any)</Text>

              <View style={{ flexDirection: "row", marginTop: 10 }}>
                <View>
                  <Text style={styles.normalTxt}>
                    業主收到租客,{"     "} 港幣
                  </Text>
                  <Text style={styles.normalEngTxt}>Landlord received HKD</Text>
                </View>
                <View
                  style={{
                    marginLeft: 10,
                    marginRight: 10,
                    width: 54,
                    borderBottomWidth: 1,
                  }}
                >
                  <Text
                    style={[
                      styles.normalTxt,
                      {
                        color: "blue",
                        paddingLeft: 10,
                        position: "absolute",
                        top: 3,
                      },
                    ]}
                  >
                    {formData?.securityDeposit
                      ? `   $${formData?.securityDeposit}`
                      : `          `}
                  </Text>
                </View>
                <View>
                  <Text style={styles.normalTxt}>首月租金</Text>
                  <Text style={styles.normalEngTxt}>
                    Initial Rent from Tenant
                  </Text>
                </View>
              </View>
              <Text style={styles.normalTxt}>支票號碼(如有)</Text>
              <Text style={styles.normalEngTxt}>Check #(If Any)</Text>

              {/* Sign */}
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 90,
                }}
              >
                {/* Left */}
                <View style={{ width: 200 }}>
                  <View
                    style={{
                      width: 200,
                      borderBottomWidth: 1,
                      borderBottomColor: "black",
                    }}
                  />
                  <Text style={[styles.normalTxt, { marginTop: 8 }]}>
                    業主確認接受這合約內所有條款的約束
                  </Text>
                  <Text style={[styles.normalEngTxt]}>
                    Confirmed and accepted all the terms and conditions
                    Confirmed and accepted all the terms and conditions herein
                    by the Landlord:
                  </Text>
                </View>

                {/* Right */}
                <View style={{ width: 200, marginRight: 14 }}>
                  <View
                    style={{
                      width: 200,
                      borderBottomWidth: 1,
                      borderBottomColor: "black",
                    }}
                  />
                  <Text style={[styles.normalTxt, { marginTop: 8 }]}>
                    租客確認接受這合約內所有條款的約束 :
                  </Text>
                  <Text style={[styles.normalEngTxt]}>
                    Confirmed and accepted all the terms and conditions herein
                    by the Landlord: herein by the Tenant:
                  </Text>
                </View>
              </View>
              {/* End Sign */}
              {/* Sign 2 */}
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 90,
                }}
              >
                {/* Left */}
                <View style={{ width: 200 }}>
                  <View
                    style={{
                      width: 200,
                      borderBottomWidth: 1,
                      borderBottomColor: "black",
                    }}
                  />
                  <View style={{ flexDirection: "row" }}>
                    <View>
                      <Text style={[styles.normalTxt, { marginTop: 8 }]}>
                        身份証/商業登記証號碼
                      </Text>
                      <Text style={[styles.normalEngTxt]}>HK ID / BR No.</Text>
                    </View>
                    <View
                      style={{
                        alignItems: "center",
                        justifyContent: "center",
                        paddingLeft: 18,
                        paddingTop: 4,
                      }}
                    >
                      <Text style={[styles.normalTxt, { color: "blue" }]}>
                        {formData?.landlordID && formData?.landlordID}
                      </Text>
                    </View>
                  </View>
                </View>

                {/* Right */}
                <View style={{ width: 200, marginRight: 14 }}>
                  <View
                    style={{
                      width: 200,
                      borderBottomWidth: 1,
                      borderBottomColor: "black",
                    }}
                  />
                  <View style={{ flexDirection: "row" }}>
                    <View>
                      <Text style={[styles.normalTxt, { marginTop: 8 }]}>
                        身份証/商業登記証號碼
                      </Text>
                      <Text style={[styles.normalEngTxt]}>HK ID / BR No.</Text>
                    </View>
                    <View
                      style={{
                        alignItems: "center",
                        justifyContent: "center",
                        paddingLeft: 18,
                        paddingTop: 4,
                      }}
                    >
                      <Text style={[styles.normalTxt, { color: "blue" }]}>
                        {formData?.tenantID && formData?.tenantID}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
              <Text
                style={[
                  styles.normalTxt,
                  { marginTop: 2, textAlign: "center" },
                ]}
              >
                附表一
              </Text>
              <Text style={[styles.normalEngTxt, { textAlign: "center" }]}>
                Schedule I
              </Text>

              <View style={{ flexDirection: "row", marginTop: 8 }}>
                <View style={{ width: 160 }}>
                  <Text style={styles.normalTxt}>物業地址:</Text>
                  <Text style={styles.normalEngTxt}>Premises Address :</Text>
                </View>
                <View
                  style={{
                    borderBottomWidth: 1,
                    width: 400,
                  }}
                >
                  <Text
                    style={[
                      styles.normalTxt,
                      { paddingLeft: 6, paddingTop: 4, color: "blue" },
                    ]}
                  >
                    {formData?.propertyAddressOne}
                  </Text>
                </View>
              </View>

              {/* Row */}
              <View style={{ flexDirection: "row", marginTop: 4 }}>
                <View style={{ width: 160 }}>
                  <Text style={styles.normalTxt}>業主姓名:</Text>
                  <Text style={styles.normalEngTxt}>Landlord Name</Text>
                </View>
                <View
                  style={{
                    borderBottomWidth: 1,
                    width: 120,
                  }}
                >
                  <Text
                    style={[
                      styles.normalTxt,
                      { paddingLeft: 6, paddingTop: 4, color: "blue" },
                    ]}
                  >
                    {formData?.landlordName}
                  </Text>
                </View>
                <View style={{ marginLeft: 20, width: 120 }}>
                  <Text style={styles.normalTxt}>身份証/商業登記証號碼</Text>
                  <Text style={styles.normalEngTxt}>HK ID / BR No</Text>
                </View>

                <View
                  style={{
                    width: 140,
                    borderBottomWidth: 1,
                  }}
                >
                  <Text
                    style={[
                      styles.normalTxt,
                      { paddingLeft: 6, paddingTop: 4, color: "blue" },
                    ]}
                  >
                    {formData?.landlordID}
                  </Text>
                </View>
              </View>
              {/* Row 聯絡地址 */}
              <View style={{ flexDirection: "row", marginTop: 4 }}>
                <View style={{ width: 160 }}>
                  <Text style={styles.normalTxt}>聯絡地址:</Text>
                  <Text style={styles.normalEngTxt}>Address</Text>
                </View>
                <View
                  style={{
                    borderBottomWidth: 1,
                    width: 250,
                  }}
                />
                <View style={{ marginLeft: 10, width: 40 }}>
                  <Text style={styles.normalTxt}>電話</Text>
                  <Text style={styles.normalEngTxt}>Phone</Text>
                </View>

                <View
                  style={{
                    width: 100,
                    borderBottomWidth: 1,
                  }}
                />
              </View>
              {/* Row 租客姓名 */}
              <View style={{ flexDirection: "row", marginTop: 4 }}>
                <View style={{ width: 160 }}>
                  <Text style={styles.normalTxt}>租客姓名:</Text>
                  <Text style={styles.normalEngTxt}>Tenant Name</Text>
                </View>
                <View
                  style={{
                    borderBottomWidth: 1,
                    width: 120,
                  }}
                >
                  <Text
                    style={[
                      styles.normalTxt,
                      { paddingLeft: 6, paddingTop: 4, color: "blue" },
                    ]}
                  >
                    {formData?.tenantName}
                  </Text>
                </View>
                <View style={{ marginLeft: 20, width: 120 }}>
                  <Text style={styles.normalTxt}>身份証/商業登記証號碼</Text>
                  <Text style={styles.normalEngTxt}>HK ID / BR No</Text>
                </View>

                <View
                  style={{
                    width: 140,
                    borderBottomWidth: 1,
                  }}
                >
                  <Text
                    style={[
                      styles.normalTxt,
                      { paddingLeft: 6, paddingTop: 4, color: "blue" },
                    ]}
                  >
                    {formData?.tenantID}
                  </Text>
                </View>
              </View>
              {/* Row 聯絡地址 */}
              <View style={{ flexDirection: "row", marginTop: 4 }}>
                <View style={{ width: 160 }}>
                  <Text style={styles.normalTxt}>聯絡地址:</Text>
                  <Text style={styles.normalEngTxt}>Address</Text>
                </View>
                <View
                  style={{
                    borderBottomWidth: 1,
                    width: 250,
                  }}
                />
                <View style={{ marginLeft: 10, width: 40 }}>
                  <Text style={styles.normalTxt}>電話</Text>
                  <Text style={styles.normalEngTxt}>Phone</Text>
                </View>

                <View
                  style={{
                    width: 100,
                    borderBottomWidth: 1,
                  }}
                />
              </View>
              {/* Row 租期由 */}
              <View style={{ flexDirection: "row", marginTop: 4 }}>
                <View>
                  <Text style={styles.normalTxt}>租期由</Text>
                  <Text style={styles.normalEngTxt}>Term From</Text>
                </View>

                <View style={{ marginLeft: 45 }}>
                  <Text style={styles.normalTxt}>年-月-日</Text>
                  <Text style={styles.normalEngTxt}>yyyy-mm-dd</Text>
                </View>

                <View
                  style={{
                    borderBottomWidth: 1,
                    width: 100,
                    marginLeft: 15,
                  }}
                >
                  <Text
                    style={[
                      styles.normalTxt,
                      { paddingLeft: 6, paddingTop: 6, color: "blue" },
                    ]}
                  >
                    {formData?.tenancyStart}
                  </Text>
                </View>

                <View style={{ marginLeft: 15 }}>
                  <Text style={styles.normalTxt}>至</Text>
                  <Text style={styles.normalEngTxt}>To </Text>
                </View>

                <View style={{ marginLeft: 15 }}>
                  <Text style={styles.normalTxt}>年-月-日</Text>
                  <Text style={styles.normalEngTxt}>yyyy-mm-dd</Text>
                </View>

                <View
                  style={{
                    borderBottomWidth: 1,
                    width: 100,
                    marginLeft: 15,
                  }}
                >
                  <Text
                    style={[
                      styles.normalTxt,
                      { paddingLeft: 6, paddingTop: 6, color: "blue" },
                    ]}
                  >
                    {formData?.tenancyEnd}
                  </Text>
                </View>
                <View style={{ marginLeft: 5 }}>
                  <Text style={styles.normalTxt}>(包括首尾兩天)</Text>
                  <Text style={styles.normalEngTxt}>(both days inclusive)</Text>
                </View>
              </View>

              {/* Row 每月租金, 港幣 */}
              <View style={{ flexDirection: "row", marginTop: 4 }}>
                <View style={{ width: 160 }}>
                  <Text style={styles.normalTxt}>每月租金, 港幣</Text>
                  <Text style={styles.normalEngTxt}>Rent Per Month, HKD</Text>
                </View>
                <View
                  style={{
                    borderBottomWidth: 1,
                    width: 150,
                  }}
                >
                  {formData?.rentPrice && (
                    <Text
                      style={[
                        styles.normalTxt,
                        { paddingLeft: 6, paddingTop: 6, color: "blue" },
                      ]}
                    >
                      ${formData?.rentPrice}
                    </Text>
                  )}
                </View>

                <View style={{ marginLeft: 10, width: 100 }}>
                  <Text style={styles.normalTxt}>保證金, 港幣</Text>
                  <Text style={styles.normalEngTxt}>Security Deposit, HKD</Text>
                </View>

                <View
                  style={{
                    borderBottomWidth: 1,
                    width: 140,
                  }}
                >
                  {formData?.securityDeposit && (
                    <Text
                      style={[
                        styles.normalTxt,
                        { paddingLeft: 6, paddingTop: 6, color: "blue" },
                      ]}
                    >
                      ${formData?.securityDeposit}
                    </Text>
                  )}
                </View>
              </View>

              <Text
                style={[
                  styles.normalTxt,
                  { marginTop: 12, textAlign: "center" },
                ]}
              >
                附表二
              </Text>
              <Text style={[styles.normalEngTxt, { textAlign: "center" }]}>
                Schedule II
              </Text>
              {/* Row */}
              <View style={[styles.termRow, { marginTop: 10 }]}>
                <Text style={styles.termNumber}>1</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    租客除將該物業作住宅用途外，不可將物業或其任何部份作其他用途。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    The Tenant shall not use or permit to be used the Premises
                    or any part thereof for any purpose other than for
                    RESIDENTIAL purpose only.
                  </Text>
                </View>
              </View>

              <View style={[styles.termRow, { marginTop: 8 }]}>
                <Text style={styles.termNumber}>2</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    業主或租客需負責下列費用 Landlord or Tenant shall be
                    responsible for the following payments:
                  </Text>
                </View>
              </View>
              {/* Row */}
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  border: 1,
                }}
              >
                <View style={{ width: "33%", flexDirection: "row" }}>
                  <View style={{ paddingLeft: 14 }}>
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {formData?.managementFee === "0" ? "業主 /" : "租客 /"}
                    </Text>
                    <Text style={[styles.normalEngTxt, { color: "blue" }]}>
                      {formData?.managementFee === "0" ? "Landlord" : "Tenant"}
                    </Text>
                  </View>
                  <View
                    style={{ marginLeft: "auto", width: 85, marginRight: 14 }}
                  >
                    <Text style={styles.normalTxt}>負責管理費</Text>
                    <Text style={styles.normalEngTxt}>For Management Fee</Text>
                  </View>
                </View>
                <View
                  style={{
                    width: "33%",
                    borderLeftWidth: 1,
                    flexDirection: "row",
                  }}
                >
                  <View style={{ paddingLeft: 14 }}>
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {formData?.governmentRate === "0" ? "業主 /" : "租客 /"}
                    </Text>
                    <Text style={[styles.normalEngTxt, { color: "blue" }]}>
                      {formData?.governmentRate === "0" ? "Landlord" : "Tenant"}
                    </Text>
                  </View>
                  <View
                    style={{ marginLeft: "auto", width: 85, marginRight: 14 }}
                  >
                    <Text style={styles.normalTxt}>負責差餉</Text>
                    <Text style={styles.normalEngTxt}>For Government Rate</Text>
                  </View>
                </View>
                <View
                  style={{
                    width: "33%",
                    borderLeftWidth: 1,
                    flexDirection: "row",
                  }}
                >
                  <View style={{ paddingLeft: 14 }}>
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {formData?.governmentRentRate === "0"
                        ? "業主 /"
                        : "租客 /"}
                    </Text>
                    <Text style={[styles.normalEngTxt, { color: "blue" }]}>
                      {formData?.governmentRentRate === "0"
                        ? "Landlord"
                        : "Tenant"}
                    </Text>
                  </View>
                  <View
                    style={{ marginLeft: "auto", width: 85, marginRight: 14 }}
                  >
                    <Text style={styles.normalTxt}>負責地租</Text>
                    <Text style={styles.normalEngTxt}>For Government Rent</Text>
                  </View>
                </View>
              </View>
              {/* Row */}
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  border: 1,
                  borderTopWidth: 0,
                }}
              >
                <View style={{ width: "33%", flexDirection: "row" }}>
                  <View style={{ paddingLeft: 14 }}>
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {formData?.electricityBill === "0" ? "業主 /" : "租客 /"}
                    </Text>
                    <Text style={[styles.normalEngTxt, { color: "blue" }]}>
                      {formData?.electricityBill === "0"
                        ? "Landlord"
                        : "Tenant"}
                    </Text>
                  </View>
                  <View
                    style={{ marginLeft: "auto", width: 85, marginRight: 14 }}
                  >
                    <Text style={styles.normalTxt}>負責電費</Text>
                    <Text style={styles.normalEngTxt}>
                      For Electricity Bill
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    width: "33%",
                    borderLeftWidth: 1,
                    flexDirection: "row",
                  }}
                >
                  <View style={{ paddingLeft: 14 }}>
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {formData?.waterBill === "0" ? "業主 /" : "租客 /"}
                    </Text>
                    <Text style={[styles.normalEngTxt, { color: "blue" }]}>
                      {formData?.waterBill === "0" ? "Landlord" : "Tenant"}
                    </Text>
                  </View>
                  <View
                    style={{ marginLeft: "auto", width: 85, marginRight: 14 }}
                  >
                    <Text style={styles.normalTxt}>負責水費</Text>
                    <Text style={styles.normalEngTxt}>For Water Bill</Text>
                  </View>
                </View>
                <View
                  style={{
                    width: "33%",
                    borderLeftWidth: 1,
                    flexDirection: "row",
                  }}
                >
                  <View style={{ paddingLeft: 14 }}>
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {formData?.gasBill === "0" ? "業主 /" : "租客 /"}
                    </Text>
                    <Text style={[styles.normalEngTxt, { color: "blue" }]}>
                      {formData?.gasBill === "0" ? "Landlord" : "Tenant"}
                    </Text>
                  </View>
                  <View
                    style={{ marginLeft: "auto", width: 85, marginRight: 14 }}
                  >
                    <Text style={styles.normalTxt}>負責煤氣費</Text>
                    <Text style={styles.normalEngTxt}>For Gas Bill</Text>
                  </View>
                </View>
              </View>
              {/* Row */}
              <View style={{ flexDirection: "row" }}>
                <View style={{ width: 80 }}>
                  <Text style={styles.normalTxt}>其他費用細節</Text>
                  <Text style={styles.normalEngTxt}>Other detail</Text>
                </View>
                <View
                  style={{
                    borderBottomWidth: 1,
                    width: 460,
                  }}
                />
              </View>
              {/* Row */}
              <View style={[styles.termRow, { marginTop: 8 }]}>
                <Text style={styles.termNumber}>3</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    業主提供以下電器予租客使用 Landlord provides the following
                    appliances for Tenant to use:
                  </Text>
                </View>
              </View>
              {/* Row */}
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  border: 1,
                }}
              >
                <View style={{ width: "23%", flexDirection: "row" }}>
                  <View style={{ paddingLeft: 8, paddingTop: 4 }}>
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {formData?.rangeHood}
                    </Text>
                  </View>

                  <View
                    style={{ marginLeft: "auto", width: 80, marginRight: 5 }}
                  >
                    <Text style={styles.normalTxt}>部抽油煙機 </Text>
                    <Text style={styles.normalEngTxt}>Range Hood(s)</Text>
                  </View>
                </View>
                <View
                  style={{
                    width: "23%",
                    borderLeftWidth: 1,
                    flexDirection: "row",
                  }}
                >
                  <View style={{ paddingLeft: 8, paddingTop: 4 }}>
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {formData?.cookingStove}
                    </Text>
                  </View>
                  <View
                    style={{ marginLeft: "auto", width: 80, marginRight: 5 }}
                  >
                    <Text style={styles.normalTxt}>部煮食爐 </Text>
                    <Text style={styles.normalEngTxt}>Cooking Stove(s)</Text>
                  </View>
                </View>
                <View
                  style={{
                    width: "23%",
                    borderLeftWidth: 1,
                    flexDirection: "row",
                  }}
                >
                  <View style={{ paddingLeft: 8, paddingTop: 4 }}>
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {formData?.microwave}
                    </Text>
                  </View>
                  <View
                    style={{ marginLeft: "auto", width: 80, marginRight: 5 }}
                  >
                    <Text style={styles.normalTxt}>部微波爐</Text>
                    <Text style={styles.normalEngTxt}>Microwave(s)</Text>
                  </View>
                </View>
                <View
                  style={{
                    width: "32%",
                    borderLeftWidth: 1,
                    flexDirection: "row",
                  }}
                >
                  <View style={{ paddingLeft: 8, paddingTop: 4 }}>
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {formData?.windowConditioner}
                    </Text>
                  </View>
                  <View
                    style={{ marginLeft: "auto", width: 125, marginRight: 5 }}
                  >
                    <Text style={styles.normalTxt}>部窗口式冷氣機</Text>
                    <Text style={styles.normalEngTxt}>
                      Window Type Air Conditioner(s)
                    </Text>
                  </View>
                </View>
              </View>
              {/* Row */}
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  border: 1,
                  borderTopWidth: 0,
                }}
              >
                <View style={{ width: "23%", flexDirection: "row" }}>
                  <View style={{ paddingLeft: 8, paddingTop: 4 }}>
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {formData?.refrigerator}
                    </Text>
                  </View>
                  <View
                    style={{ marginLeft: "auto", width: 80, marginRight: 5 }}
                  >
                    <Text style={styles.normalTxt}>部雪櫃</Text>
                    <Text style={styles.normalEngTxt}>Refrigerator(s)</Text>
                  </View>
                </View>
                <View
                  style={{
                    width: "23%",
                    borderLeftWidth: 1,
                    flexDirection: "row",
                  }}
                >
                  <View style={{ paddingLeft: 8, paddingTop: 4 }}>
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {formData?.washingMachine}
                    </Text>
                  </View>
                  <View
                    style={{ marginLeft: "auto", width: 80, marginRight: 5 }}
                  >
                    <Text style={styles.normalTxt}>部洗衣機</Text>
                    <Text style={styles.normalEngTxt}>Washing Machine(s)</Text>
                  </View>
                </View>
                <View
                  style={{
                    width: "23%",
                    borderLeftWidth: 1,
                    flexDirection: "row",
                  }}
                >
                  <View style={{ paddingLeft: 8, paddingTop: 4 }}>
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {formData?.television}
                    </Text>
                  </View>
                  <View
                    style={{ marginLeft: "auto", width: 80, marginRight: 5 }}
                  >
                    <Text style={styles.normalTxt}>部電視機</Text>
                    <Text style={styles.normalEngTxt}>Television(s)</Text>
                  </View>
                </View>
                <View
                  style={{
                    width: "32%",
                    borderLeftWidth: 1,
                    flexDirection: "row",
                  }}
                >
                  <View style={{ paddingLeft: 8, paddingTop: 4 }}>
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {formData?.splitConditioner}
                    </Text>
                  </View>
                  <View
                    style={{ marginLeft: "auto", width: 125, marginRight: 5 }}
                  >
                    <Text style={styles.normalTxt}>部分體式冷氣機</Text>
                    <Text style={styles.normalEngTxt}>
                      Split Type Air-Conditioner(s)
                    </Text>
                  </View>
                </View>
              </View>
              {/* Row */}
              <View style={{ flexDirection: "row" }}>
                <View style={{ width: 120 }}>
                  <Text style={styles.normalTxt}>其他電器或家俬 :</Text>
                  <Text style={styles.normalEngTxt}>
                    Other appliances or furniture :
                  </Text>
                </View>
                <View
                  style={{
                    borderBottomWidth: 1,
                    width: 420,
                  }}
                >
                  <Text
                    style={[
                      styles.normalTxt,
                      { paddingLeft: 6, paddingTop: 4, color: "blue" },
                    ]}
                  >
                    {formData?.otherFacility}
                  </Text>
                </View>
              </View>
              {/* Row */}
              <View style={[styles.termRow, { marginTop: 8 }]}>
                <Text style={styles.termNumber}>4</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    租客可享有免租期由 ______________ 至 ______________
                    包括首尾兩天，但租客仍需負責繳付免租
                    期內水費、電費、煤氣費、電話費及其他一切雜費支出。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    The Tenant shall be entitled to a rent free period from
                    ______________ to ______________ (both days inclusive),
                    provided that the tenant shall be responsible for the
                    charges of water, electricity, gas, telephone and other
                    outstandings payable in respects of Premises during such
                    rent free period.
                  </Text>
                </View>
              </View>
              {/* Row */}
              <View style={[styles.termRow, { marginTop: 4 }]}>
                <Text style={styles.termNumber}>5</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    儘管與前文不符，業主或租客可給予另一方不少於
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {" "}
                      {formData?.tenancyForceYear}{" "}
                    </Text>
                    個月的書面通知或 ___
                    個月租金作代通知金提早解除此租約；唯該書面通知書不得早於由租期起計之
                    11 個月內發出。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    Notwithstanding anything to the contrary hereinbefore
                    contained, either party shall be entitled to terminate this
                    Agreement earlier than as herein provided by serving not
                    less than{" "}
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {" "}
                      {formData?.tenancyForceYear}{" "}
                    </Text>{" "}
                    months’ written notice or by paying 1 months’ Rent in lieu
                    to the other party provided that the said written notice
                    shall not be served before the expiration of the 11 months
                    of the Term of Tenancy.
                  </Text>
                </View>
              </View>
              {/* Row */}
              <View style={[styles.termRow, { marginTop: 4 }]}>
                <Text style={styles.termNumber}>6</Text>
                <View>
                  <Text style={styles.normalTxt}>業主收租之銀行戶口 :</Text>
                  <Text style={styles.normalEngTxt}>
                    Landlord&apos;s bank account for receiving rent payment :
                  </Text>
                </View>

                <View
                  style={{
                    marginLeft: 14,
                    borderBottomWidth: 1,
                    width: 260,
                  }}
                />
              </View>
              {/* Row */}
              <View style={[styles.termRow, { marginTop: 8 }]}>
                <Text style={styles.termNumber}>7</Text>
                <View>
                  <Text style={styles.normalTxt}>其他 Others:</Text>
                </View>

                <View
                  style={{
                    marginLeft: 14,
                    borderBottomWidth: 1,
                    width: 320,
                  }}
                />
              </View>

              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 60,
                }}
              >
                {/* Left */}
                <View style={{ width: 200 }}>
                  <View
                    style={{
                      width: 200,
                      borderBottomWidth: 1,
                      borderBottomColor: "black",
                    }}
                  />
                  <Text style={[styles.normalTxt, { marginTop: 8 }]}>
                    業主簽處 Signed by the Landlord
                  </Text>
                </View>

                {/* Right */}
                <View style={{ width: 200, marginRight: 14 }}>
                  <View
                    style={{
                      width: 200,
                      borderBottomWidth: 1,
                      borderBottomColor: "black",
                    }}
                  />
                  <Text style={[styles.normalTxt, { marginTop: 8 }]}>
                    租客簽處 Signed by the Tenant
                  </Text>
                </View>
              </View>
            </Page>
          </Document>
        </PDFViewer>
      )}
    </div>
  );
}

export default AgreementPreview;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: "white",
    padding: 34,
    // margin: 14,
  },
  title: {
    fontFamily: "PingFang",
    fontWeight: 700,
    textAlign: "center",
  },
  contractHeader: {
    marginTop: 20,
  },
  contractDateTxt: {
    fontFamily: "PingFang",
    fontSize: 9,
    // paddingHorizontal: 22,
  },
  contractDateEngTxt: {
    fontFamily: "PingFang",
    fontSize: 8,
    // paddingHorizontal: 22,
  },
  height20: {
    marginTop: 20,
  },
  termRow: {
    flexDirection: "row",
    marginTop: 12,
    paddingRight: 24,
    // paddingHorizontal: 24,
  },
  termNumber: {
    fontFamily: "PingFang",
    fontSize: 9,
    width: 18,
  },
  normalTxt: {
    fontFamily: "PingFang",
    fontSize: 9,
    fontWeight: 400,
  },
  normalEngTxt: {
    fontFamily: "PingFang",
    fontSize: 8.5,
    fontWeight: 400,
  },
});
