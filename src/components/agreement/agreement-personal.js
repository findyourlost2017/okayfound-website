import { FormItem, Input, Select } from "../input";

function AgreementPersonal({ currentStep, type, setType }) {
  return (
    <div className={`${currentStep === 0 ? "block" : "hidden"}`}>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-8 md:gap-5">
        <FormItem label="合約種類">
          <Select
            name="selectedType"
            value={type}
            onChange={(e) => setType(parseInt(e.target.value))}
          >
            <option value="" disabled>
              請選擇合約種類
            </option>
            <option value={0}>車位租務合約</option>
            <option value={1}>住宅租務合約</option>
          </Select>
        </FormItem>
      </div>

      <div className="grid grid-cols-1 md:grid-cols-2 gap-8 md:gap-5 mt-6">
        <FormItem label="業主名稱(可選填)">
          <Input type="text" name="landlordName" placeholder="請輸入業主名稱" />
        </FormItem>
        <FormItem label="業主身份證(可選填)">
          <Input type="text" name="landlordID" placeholder="請輸入業主身份證" />
        </FormItem>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-8 md:gap-5 mt-6">
        <FormItem label="租客名稱(可選填)">
          <Input type="text" name="tenantName" placeholder="請輸入租客名稱" />
        </FormItem>
        <FormItem label="租客身份證(可選填)">
          <Input type="text" name="tenantID" placeholder="請輸入租客身份證" />
        </FormItem>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-8 md:gap-5 mt-6">
        <FormItem label={type === 0 ? "車位地址(可選填)" : "物業地址(可選填)"}>
          <Input type="text" name="propertyAddressOne" placeholder="地址行" />
          {/* <Input
            type="text"
            name="propertyAddressTwo"
            placeholder="地址行2"
            className="mt-2"
          /> */}
        </FormItem>
      </div>
    </div>
  );
}

export default AgreementPersonal;
