import ButtonPrimary from "../button/button-primary";
import ButtonThird from "../button/button-third";

function AgreementFooter({ currentStep, setCurrentStep }) {
  return (
    <div className="flex p-2 mt-4">
      {(currentStep === 1 || currentStep === 2 || currentStep === 3) && (
        <ButtonThird
          type="button"
          onClick={() => {
            window.scrollTo({ top: 0, behavior: "smooth" });
            setCurrentStep(currentStep - 1);
          }}
          className="mr-4 bg-gray-200"
        >
          上一步
        </ButtonThird>
      )}
      {currentStep !== 3 && (
        <ButtonPrimary
          type={currentStep === 3 ? "submit" : "button"}
          onClick={async () => {
            window.scrollTo({ top: 0, behavior: "smooth" });

            if (currentStep != 3) {
              setCurrentStep(currentStep + 1);
            } else {
              // const value = await renderToStream(<AgreementPreview />);
            }
          }}
        >
          {currentStep < 3 ? "下一步" : "列印"}
        </ButtonPrimary>
      )}
    </div>
  );
}

export default AgreementFooter;
