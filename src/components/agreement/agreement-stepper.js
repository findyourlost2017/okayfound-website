function AgreementStepper({ currentStep }) {
  return (
    <div className="mx-4 p-4">
      <div className="flex items-center">
        <div className="flex items-center  relative">
          <div
            className={`rounded-full transition duration-500 ease-in-out h-12 w-12 flex border-2 ${
              currentStep >= 0 ? "border-orange-500" : "border-gray-300"
            } justify-center items-center`}
          >
            <i
              className={`fa-solid fa-user ${
                currentStep >= 0 ? "text-orange-500" : "text-gray-500"
              }`}
            />
          </div>
          <div
            className={`absolute top-0 -ml-10 text-center mt-16 w-32 text-xs font-medium uppercase ${
              currentStep >= 0 ? "text-orange-500" : "text-gray-500"
            }`}
          >
            個人資料
          </div>
        </div>
        <div
          className={`flex-auto border-t-2 transition duration-500 ease-in-out ${
            currentStep >= 1 ? "border-orange-500" : "border-gray-300"
          }`}
        />
        <div className="flex items-center relative">
          <div
            className={`rounded-full transition duration-500 ease-in-out h-12 w-12 flex border-2 ${
              currentStep >= 1 ? "border-orange-500" : "border-gray-300"
            } justify-center items-center`}
          >
            <i
              className={`fa-solid fa-house ${
                currentStep >= 1 ? "text-orange-500" : "text-gray-500"
              }`}
            />
          </div>
          <div
            className={`absolute top-0 -ml-10 text-center mt-16 w-32 text-xs font-medium uppercase ${
              currentStep >= 1 ? "text-orange-500" : "text-gray-500"
            }`}
          >
            單位資料
          </div>
        </div>
        <div
          className={`flex-auto border-t-2 transition duration-500 ease-in-out ${
            currentStep >= 2 ? "border-orange-500" : "border-gray-300"
          }`}
        />
        <div className="flex items-center text-teal-600 relative">
          <div
            className={`rounded-full transition duration-500 ease-in-out h-12 w-12 flex border-2 ${
              currentStep >= 2 ? "border-orange-500" : "border-gray-300"
            } justify-center items-center`}
          >
            <i
              className={`fa-solid fa-file-contract ${
                currentStep >= 2 ? "text-orange-500" : "text-gray-500"
              }`}
            />
          </div>
          <div
            className={`absolute top-0 -ml-10 text-center mt-16 w-32 text-xs font-medium uppercase ${
              currentStep >= 2 ? "text-orange-500" : "text-gray-500"
            }`}
          >
            租約詳情
          </div>
        </div>
        <div
          className={`flex-auto border-t-2 transition duration-500 ease-in-out ${
            currentStep === 3 ? "border-orange-500" : "border-gray-300"
          }`}
        />
        <div className="flex items-center text-teal-600 relative">
          <div
            className={`rounded-full transition duration-500 ease-in-out h-12 w-12 flex border-2 ${
              currentStep === 3 ? "border-orange-500" : "border-gray-300"
            } justify-center items-center`}
          >
            <i
              className={`fa-solid fa-file-arrow-down ${
                currentStep === 3 ? "text-orange-500" : "text-gray-500"
              }`}
            />
          </div>
          <div
            className={`absolute top-0 -ml-10 text-center mt-16 w-32 text-xs font-medium uppercase ${
              currentStep === 3 ? "text-orange-500" : "text-gray-500"
            }`}
          >
            下載租約
          </div>
        </div>
      </div>
    </div>
  );
}

export default AgreementStepper;
