import AgreementPersonal from "./agreement-personal";
import AgreementProperty from "./agreement-property";
import AgreementDetail from "./agreement-detail";
import AgreementPreview from "./agreement-preview";
import AgreementResidentialPreview from "./agreement-residential-preview";
import AgreementParkingPreview from "./agreement-parking-preview";

function AgreementContent({ currentStep, type, setType, formData }) {
  return (
    <div className="mx-4 mt-8 p-4 ">
      <AgreementPersonal
        type={type}
        setType={setType}
        currentStep={currentStep}
      />
      <AgreementProperty type={type} currentStep={currentStep} />
      <AgreementDetail type={type} currentStep={currentStep} />
      {type === 1 && (
        <AgreementResidentialPreview
          currentStep={currentStep}
          formData={formData}
        />
      )}

      {type === 0 && (
        <AgreementParkingPreview
          currentStep={currentStep}
          formData={formData}
        />
      )}
      {/* <AgreementPreview currentStep={currentStep} formData={formData} /> */}
    </div>
  );
}

export default AgreementContent;
