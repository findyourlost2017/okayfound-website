import { useEffect, useState } from "react";
import {
  PDFViewer,
  Document,
  Page,
  Text,
  View,
  StyleSheet,
  Font,
} from "@react-pdf/renderer";

function AgreementPreview({ currentStep, formData }) {
  const [domLoaded, setDomLoaded] = useState(false);

  Font.register({
    family: "PingFang",
    fonts: [
      {
        src: "https://api.okayfound.com/PingFang-SC-Regular.ttf",
        fontWeight: 400,
      },
      {
        src: "https://api.okayfound.com/PingFang-SC-Bold.ttf",
        fontWeight: 700,
      },
    ],
  });

  Font.registerHyphenationCallback((word) => {
    if (word.length === 1) {
      return [word];
    }

    return Array.from(word)
      .map((char) => [char, ""])
      .reduce((arr, current) => {
        arr.push(...current);
        return arr;
      }, []);
  });

  useEffect(() => {
    setDomLoaded(true);
  }, []);

  return (
    <div className={`${currentStep === 3 ? "block" : "hidden"} agreement`}>
      {domLoaded && (
        <PDFViewer width={1100} height={842}>
          <Document>
            <Page size="A4" style={styles.page}>
              <Text style={[styles.title]}>車位租約</Text>
              <Text style={styles.title}>Car Park Tenancy Agreement</Text>
              {/* Row */}
              <View style={styles.contractHeader}>
                <Text style={styles.contractDateTxt}>
                  本合約訂於{" "}
                  <Text style={{ textDecoration: "underline" }}>
                    {formData?.contractDate
                      ? `    ${formData.contractDate}    `
                      : "                          "}
                  </Text>{" "}
                  由業主及租客訂立,雙方資料詳列於附表一。
                </Text>
                <Text style={styles.contractDateEngTxt}>
                  This Agreement is made on{" "}
                  <Text style={{ textDecoration: "underline" }}>
                    {formData?.contractDate
                      ? `    ${formData.contractDate}    `
                      : "                          "}
                  </Text>{" "}
                  Between the Landlord and the Tenant as more particularly
                  described in Schedule I.
                </Text>
              </View>

              <View style={styles.height20} />
              <Text style={styles.contractDateTxt}>
                業主及租客雙方以詳列於附表一的租期及租金分別租出及租入詳列於附表一的物業，並同意遵守及履行下列條款：-
              </Text>
              <Text style={styles.contractDateEngTxt}>
                The Landlord shall let and the Tenant shall take the Premises
                for the Term and at the Rent as more particularly described in
                Schedule I and both parties agree to observe and perform the
                terms and conditions as follow:-
              </Text>
              {/* Row */}
              <View style={[styles.termRow, { marginTop: 20 }]}>
                <Text style={styles.termNumber}>1</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    租客須在租期內每個月份的{" "}
                    <Text
                      style={{ color: "blue", textDecoration: "underline" }}
                    >
                      {formData?.tenancySubmitPriceDay}
                    </Text>{" "}
                    號或以前上期繳付指定的租金予業主。倘租客於應繳租金之日的七天內仍未清付該租金，則業主有權採取適當行動追討租客所欠的租金，而由此而引起的一切費用及開支將構成租客所欠業主的債項，業主將有權向租客一併追討所欠款項全數。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    The Tenant shall pay to the Landlord the Rent in advance on
                    or before{" "}
                    <Text
                      style={{ color: "blue", textDecoration: "underline" }}
                    >
                      {formData?.tenancySubmitPriceDay}
                    </Text>{" "}
                    (st/nd/rd/th) of each and every calendar month during the
                    Term. If the Tenant shall fail to pay the Rent within 7 days
                    from the due date, the Landlord shall have the right to
                    institute appropriate action to recover the Rent and all
                    costs, expenses and other outgoings so incurred by the
                    Landlord in relation to such action shall be a debt owed by
                    the Tenant to the Landlord and shall be recoverable in full
                    by the Landlord.
                  </Text>
                </View>
              </View>
              {/* Term 2 */}
              <View style={styles.termRow}>
                <Text style={styles.termNumber}>2</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    租客不得轉讓、轉租或分租該車位予任何其他人等。此租約權益為租客個人擁有。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    The Tenant shall not assign, transfer, sublet or part with
                    the possession of the car park space to any other person.
                    This tenancy shall be personal to the Tenant named therein.
                  </Text>
                </View>
              </View>
              {/* Term 3 */}
              <View style={styles.termRow}>
                <Text style={styles.termNumber}>3</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    租客須遵守該停車場之守則。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    The Tenant shall comply with all regulations and rules of
                    that parking lot.
                  </Text>
                </View>
              </View>
              {/* Term 4 */}
              <View style={styles.termRow}>
                <Text style={styles.termNumber}>4</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    租客須交予業主保証金(金額如附表一所列)，業主須於退租時七天內無息退還該保証金予租客。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    The Tenant shall pay to the Landlord the Security Deposit
                    set out in Schedule I, the Landlord shall refund the
                    Security Deposit to the Tenant without interest within 7
                    days after termination of this agreement.
                  </Text>
                </View>
              </View>
              {/* Term 5 */}
              <View style={styles.termRow}>
                <Text style={styles.termNumber}>5</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    業主須交入閘卡或泊車證予租客，若該証及卡在租期內有所遺失該責任由租客負責，該証及卡須於約滿後由租客交還給業
                    主。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    The Landlord shall provide access card or parking permit to
                    the Tenant, and the Tenant shall responsible for the lost of
                    access card and parking permit during the Term, and should
                    return the access card or parking permit to the Landlord
                    after the termination of this agreement.
                  </Text>
                </View>
              </View>
              {/* Term 6 */}
              <View style={styles.termRow}>
                <Text style={styles.termNumber}>6</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    業主及租客雙方同意遵守附表二的附加條件 (如有)。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    The Landlord and the Tenant agree to be bound by the
                    additional terms and conditions in Schedule II (if any).
                  </Text>
                </View>
              </View>
              {/* Term 7 */}
              <View style={styles.termRow}>
                <Text style={styles.termNumber}>7</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    此合約內的英文本與中文本存有差異時，將以英文本為準。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    If there is any conflict between the English version and the
                    Chinese version in this Agreement, the English version shall
                    prevail
                  </Text>
                </View>
              </View>
              {/* Row */}
              <Text
                style={[
                  styles.normalTxt,
                  { marginTop: 2, textAlign: "center" },
                ]}
                break
              >
                附表一
              </Text>
              <Text style={[styles.normalEngTxt, { textAlign: "center" }]}>
                Schedule I
              </Text>

              {/* Row */}
              <View style={{ flexDirection: "row", marginTop: 8 }}>
                <View style={{ width: 160 }}>
                  <Text style={styles.normalTxt}>車位地址:</Text>
                  <Text style={styles.normalEngTxt}>
                    Address of Car Park space :
                  </Text>
                </View>
                <View
                  style={{
                    borderBottomWidth: 1,
                    width: 400,
                  }}
                >
                  <Text
                    style={[
                      styles.normalTxt,
                      { paddingLeft: 6, paddingTop: 4, color: "blue" },
                    ]}
                  >
                    {formData?.propertyAddressOne}
                  </Text>
                </View>
              </View>

              {/* Row */}
              <View style={{ flexDirection: "row", marginTop: 4 }}>
                <View style={{ width: 160 }}>
                  <Text style={styles.normalTxt}>業主姓名:</Text>
                  <Text style={styles.normalEngTxt}>Landlord Name</Text>
                </View>
                <View
                  style={{
                    borderBottomWidth: 1,
                    width: 120,
                  }}
                >
                  <Text
                    style={[
                      styles.normalTxt,
                      { paddingLeft: 6, paddingTop: 4, color: "blue" },
                    ]}
                  >
                    {formData?.landlordName}
                  </Text>
                </View>
                <View style={{ marginLeft: 20, width: 120 }}>
                  <Text style={styles.normalTxt}>身份証/商業登記証號碼</Text>
                  <Text style={styles.normalEngTxt}>HK ID / BR No</Text>
                </View>

                <View
                  style={{
                    width: 140,
                    borderBottomWidth: 1,
                  }}
                >
                  <Text
                    style={[
                      styles.normalTxt,
                      { paddingLeft: 6, paddingTop: 4, color: "blue" },
                    ]}
                  >
                    {formData?.landlordID}
                  </Text>
                </View>
              </View>

              {/* Row 租客姓名 */}
              <View style={{ flexDirection: "row", marginTop: 4 }}>
                <View style={{ width: 160 }}>
                  <Text style={styles.normalTxt}>租客姓名:</Text>
                  <Text style={styles.normalEngTxt}>Tenant Name</Text>
                </View>
                <View
                  style={{
                    borderBottomWidth: 1,
                    width: 120,
                  }}
                >
                  <Text
                    style={[
                      styles.normalTxt,
                      { paddingLeft: 6, paddingTop: 4, color: "blue" },
                    ]}
                  >
                    {formData?.tenantName}
                  </Text>
                </View>
                <View style={{ marginLeft: 20, width: 120 }}>
                  <Text style={styles.normalTxt}>身份証/商業登記証號碼</Text>
                  <Text style={styles.normalEngTxt}>HK ID / BR No</Text>
                </View>

                <View
                  style={{
                    width: 140,
                    borderBottomWidth: 1,
                  }}
                >
                  <Text
                    style={[
                      styles.normalTxt,
                      { paddingLeft: 6, paddingTop: 4, color: "blue" },
                    ]}
                  >
                    {formData?.tenantID}
                  </Text>
                </View>
              </View>

              {/* Row 租期由 */}
              <View style={{ flexDirection: "row", marginTop: 4 }}>
                <View>
                  <Text style={styles.normalTxt}>租期由</Text>
                  <Text style={styles.normalEngTxt}>Term From</Text>
                </View>

                <View style={{ marginLeft: 45 }}>
                  <Text style={styles.normalTxt}>年-月-日</Text>
                  <Text style={styles.normalEngTxt}>yyyy-mm-dd</Text>
                </View>

                <View
                  style={{
                    borderBottomWidth: 1,
                    width: 100,
                    marginLeft: 15,
                  }}
                >
                  <Text
                    style={[
                      styles.normalTxt,
                      { paddingLeft: 6, paddingTop: 6, color: "blue" },
                    ]}
                  >
                    {formData?.tenancyStart}
                  </Text>
                </View>

                <View style={{ marginLeft: 15 }}>
                  <Text style={styles.normalTxt}>至</Text>
                  <Text style={styles.normalEngTxt}>To </Text>
                </View>

                <View style={{ marginLeft: 15 }}>
                  <Text style={styles.normalTxt}>年-月-日</Text>
                  <Text style={styles.normalEngTxt}>yyyy-mm-dd</Text>
                </View>

                <View
                  style={{
                    borderBottomWidth: 1,
                    width: 100,
                    marginLeft: 15,
                  }}
                >
                  <Text
                    style={[
                      styles.normalTxt,
                      { paddingLeft: 6, paddingTop: 6, color: "blue" },
                    ]}
                  >
                    {formData?.tenancyEnd}
                  </Text>
                </View>
                <View style={{ marginLeft: 5 }}>
                  <Text style={styles.normalTxt}>(包括首尾兩天)</Text>
                  <Text style={styles.normalEngTxt}>(both days inclusive)</Text>
                </View>
              </View>

              {/* Row 每月租金, 港幣 */}
              <View style={{ flexDirection: "row", marginTop: 4 }}>
                <View style={{ width: 160 }}>
                  <Text style={styles.normalTxt}>每月租金, 港幣</Text>
                  <Text style={styles.normalEngTxt}>Rent Per Month, HKD</Text>
                </View>
                <View
                  style={{
                    borderBottomWidth: 1,
                    width: 150,
                  }}
                >
                  {formData?.rentPrice && (
                    <Text
                      style={[
                        styles.normalTxt,
                        { paddingLeft: 6, paddingTop: 6, color: "blue" },
                      ]}
                    >
                      ${formData?.rentPrice}
                    </Text>
                  )}
                </View>

                <View style={{ marginLeft: 10, width: 100 }}>
                  <Text style={styles.normalTxt}>保證金, 港幣</Text>
                  <Text style={styles.normalEngTxt}>Security Deposit, HKD</Text>
                </View>

                <View
                  style={{
                    borderBottomWidth: 1,
                    width: 140,
                  }}
                >
                  {formData?.securityDeposit && (
                    <Text
                      style={[
                        styles.normalTxt,
                        { paddingLeft: 6, paddingTop: 6, color: "blue" },
                      ]}
                    >
                      ${formData?.securityDeposit}
                    </Text>
                  )}
                </View>
              </View>

              {/* Row */}
              <Text
                style={[
                  styles.normalTxt,
                  { marginTop: 12, textAlign: "center" },
                ]}
              >
                附表二
              </Text>
              <Text style={[styles.normalEngTxt, { textAlign: "center" }]}>
                Schedule II
              </Text>

              {/* Row */}
              <View style={[styles.termRow, { marginTop: 8 }]}>
                <Text style={styles.termNumber}>1</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    業主或租客需負責下列費用 Landlord or Tenant shall be
                    responsible for the following payments:
                  </Text>
                </View>
              </View>

              {/* Row */}
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  border: 1,
                }}
              >
                <View style={{ width: "33%", flexDirection: "row" }}>
                  <View style={{ paddingLeft: 14 }}>
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {formData?.managementFee === "0" ? "業主 /" : "租客 /"}
                    </Text>
                    <Text style={[styles.normalEngTxt, { color: "blue" }]}>
                      {formData?.managementFee === "0" ? "Landlord" : "Tenant"}
                    </Text>
                  </View>
                  <View
                    style={{ marginLeft: "auto", width: 85, marginRight: 14 }}
                  >
                    <Text style={styles.normalTxt}>負責管理費</Text>
                    <Text style={styles.normalEngTxt}>For Management Fee</Text>
                  </View>
                </View>
                <View
                  style={{
                    width: "33%",
                    borderLeftWidth: 1,
                    flexDirection: "row",
                  }}
                >
                  <View style={{ paddingLeft: 14 }}>
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {formData?.governmentRate === "0" ? "業主 /" : "租客 /"}
                    </Text>
                    <Text style={[styles.normalEngTxt, { color: "blue" }]}>
                      {formData?.governmentRate === "0" ? "Landlord" : "Tenant"}
                    </Text>
                  </View>
                  <View
                    style={{ marginLeft: "auto", width: 85, marginRight: 14 }}
                  >
                    <Text style={styles.normalTxt}>負責差餉</Text>
                    <Text style={styles.normalEngTxt}>For Government Rate</Text>
                  </View>
                </View>
                <View
                  style={{
                    width: "33%",
                    borderLeftWidth: 1,
                    flexDirection: "row",
                  }}
                >
                  <View style={{ paddingLeft: 14 }}>
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {formData?.governmentRentRate === "0"
                        ? "業主 /"
                        : "租客 /"}
                    </Text>
                    <Text style={[styles.normalEngTxt, { color: "blue" }]}>
                      {formData?.governmentRentRate === "0"
                        ? "Landlord"
                        : "Tenant"}
                    </Text>
                  </View>
                  <View
                    style={{ marginLeft: "auto", width: 85, marginRight: 14 }}
                  >
                    <Text style={styles.normalTxt}>負責地租</Text>
                    <Text style={styles.normalEngTxt}>For Government Rent</Text>
                  </View>
                </View>
              </View>
              {/* Row */}
              <View style={[styles.termRow, { marginTop: 12 }]}>
                <Text style={styles.termNumber}>2</Text>
                <View>
                  <Text style={styles.normalTxt}>
                    儘管與前文不符，業主或租客可給予另一方不少於
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {" "}
                      {formData?.tenancyForceYear}{" "}
                    </Text>
                    個月的書面通知或 ___
                    個月租金作代通知金提早解除此租約；唯該書面通知書不得早於由租期起計之
                    11 個月內發出。
                  </Text>
                  <Text style={styles.normalEngTxt}>
                    Notwithstanding anything to the contrary hereinbefore
                    contained, either party shall be entitled to terminate this
                    Agreement earlier than as herein provided by serving not
                    less than{" "}
                    <Text style={[styles.normalTxt, { color: "blue" }]}>
                      {" "}
                      {formData?.tenancyForceYear}{" "}
                    </Text>{" "}
                    months’ written notice or by paying ___ months’ Rent in lieu
                    to the other party provided that the said written notice
                    shall not be served before the expiration of the 11 months
                    of the Term of Tenancy.
                  </Text>
                </View>
              </View>
              {/* Row */}
              <View style={[styles.termRow, { marginTop: 4 }]}>
                <Text style={styles.termNumber}>3</Text>
                <View>
                  <Text style={styles.normalTxt}>業主收租之銀行戶口 :</Text>
                  <Text style={styles.normalEngTxt}>
                    Landlord&apos;s bank account for receiving rent payment :
                  </Text>
                </View>

                <View
                  style={{
                    marginLeft: 14,
                    borderBottomWidth: 1,
                    width: 260,
                  }}
                />
              </View>
              {/* Row */}
              <View style={[styles.termRow, { marginTop: 8 }]}>
                <Text style={styles.termNumber}>4</Text>
                <View>
                  <Text style={styles.normalTxt}>其他 Others:</Text>
                </View>

                <View
                  style={{
                    marginLeft: 14,
                    borderBottomWidth: 1,
                    width: 320,
                  }}
                />
              </View>

              {/* Row */}
              <View style={{ flexDirection: "row", marginTop: 14 }}>
                <View>
                  <Text style={styles.normalTxt}>
                    業主收到租客,{"     "} 港幣
                  </Text>
                  <Text style={styles.normalEngTxt}>Landlord received HKD</Text>
                </View>
                <View
                  style={{
                    marginLeft: 10,
                    marginRight: 10,
                    width: 54,
                    borderBottomWidth: 1,
                  }}
                >
                  <Text
                    style={[
                      styles.normalTxt,
                      {
                        color: "blue",
                        paddingLeft: 10,
                        position: "absolute",
                        top: 3,
                      },
                    ]}
                  >
                    {formData?.rentPrice
                      ? `   $${formData?.rentPrice}`
                      : `          `}
                  </Text>
                </View>
                <View>
                  <Text style={styles.normalTxt}>保証金</Text>
                  <Text style={styles.normalEngTxt}>
                    Security Deposit from Tenant
                  </Text>
                </View>
              </View>
              <Text style={styles.normalTxt}>支票號碼(如有)</Text>
              <Text style={styles.normalEngTxt}>Check #(If Any)</Text>
              {/* Row */}
              <View style={{ flexDirection: "row", marginTop: 10 }}>
                <View>
                  <Text style={styles.normalTxt}>
                    業主收到租客,{"     "} 港幣
                  </Text>
                  <Text style={styles.normalEngTxt}>Landlord received HKD</Text>
                </View>
                <View
                  style={{
                    marginLeft: 10,
                    marginRight: 10,
                    width: 54,
                    borderBottomWidth: 1,
                  }}
                >
                  <Text
                    style={[
                      styles.normalTxt,
                      {
                        color: "blue",
                        paddingLeft: 10,
                        position: "absolute",
                        top: 3,
                      },
                    ]}
                  >
                    {formData?.securityDeposit
                      ? `   $${formData?.securityDeposit}`
                      : `          `}
                  </Text>
                </View>
                <View>
                  <Text style={styles.normalTxt}>首月租金</Text>
                  <Text style={styles.normalEngTxt}>
                    Initial Rent from Tenant
                  </Text>
                </View>
              </View>
              <Text style={styles.normalTxt}>支票號碼(如有)</Text>
              <Text style={styles.normalEngTxt}>Check #(If Any)</Text>

              {/* Sign */}
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 80,
                }}
              >
                {/* Left */}
                <View style={{ width: 200 }}>
                  <View
                    style={{
                      width: 200,
                      borderBottomWidth: 1,
                      borderBottomColor: "black",
                    }}
                  />
                  <Text style={[styles.normalTxt, { marginTop: 8 }]}>
                    業主確認接受這合約內所有條款的約束
                  </Text>
                  <Text style={[styles.normalEngTxt]}>
                    Confirmed and accepted all the terms and conditions herein
                    by the Landlord:
                  </Text>
                </View>

                {/* Right */}
                <View style={{ width: 200, marginRight: 14 }}>
                  <View
                    style={{
                      width: 200,
                      borderBottomWidth: 1,
                      borderBottomColor: "black",
                    }}
                  />
                  <Text style={[styles.normalTxt, { marginTop: 8 }]}>
                    租客確認接受這合約內所有條款的約束 :
                  </Text>
                  <Text style={[styles.normalEngTxt]}>
                    Confirmed and accepted all the terms and conditions herein
                    by the Tenant:
                  </Text>
                </View>
              </View>
              {/* End Sign */}
              {/* Sign 2 */}
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 80,
                }}
              >
                {/* Left */}
                <View style={{ width: 200 }}>
                  <View
                    style={{
                      width: 200,
                      borderBottomWidth: 1,
                      borderBottomColor: "black",
                    }}
                  />
                  <View style={{ flexDirection: "row" }}>
                    <View>
                      <Text style={[styles.normalTxt, { marginTop: 8 }]}>
                        身份証/商業登記証號碼
                      </Text>
                      <Text style={[styles.normalEngTxt]}>HK ID / BR No.</Text>
                    </View>
                    <View
                      style={{
                        alignItems: "center",
                        justifyContent: "center",
                        paddingLeft: 18,
                        paddingTop: 4,
                      }}
                    >
                      <Text style={[styles.normalTxt, { color: "blue" }]}>
                        {formData?.landlordID && formData?.landlordID}
                      </Text>
                    </View>
                  </View>
                </View>

                {/* Right */}
                <View style={{ width: 200, marginRight: 14 }}>
                  <View
                    style={{
                      width: 200,
                      borderBottomWidth: 1,
                      borderBottomColor: "black",
                    }}
                  />
                  <View style={{ flexDirection: "row" }}>
                    <View>
                      <Text style={[styles.normalTxt, { marginTop: 8 }]}>
                        身份証/商業登記証號碼
                      </Text>
                      <Text style={[styles.normalEngTxt]}>HK ID / BR No.</Text>
                    </View>
                    <View
                      style={{
                        alignItems: "center",
                        justifyContent: "center",
                        paddingLeft: 18,
                        paddingTop: 4,
                      }}
                    >
                      <Text style={[styles.normalTxt, { color: "blue" }]}>
                        {formData?.tenantID && formData?.tenantID}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            </Page>
          </Document>
        </PDFViewer>
      )}
    </div>
  );
}

export default AgreementPreview;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: "white",
    padding: 34,
    // margin: 14,
  },
  title: {
    fontFamily: "PingFang",
    fontWeight: 700,
    textAlign: "center",
  },
  contractHeader: {
    marginTop: 20,
  },
  contractDateTxt: {
    fontFamily: "PingFang",
    fontSize: 9,
    // paddingHorizontal: 22,
  },
  contractDateEngTxt: {
    fontFamily: "PingFang",
    fontSize: 8,
    // paddingHorizontal: 22,
  },
  height20: {
    marginTop: 20,
  },
  termRow: {
    flexDirection: "row",
    marginTop: 12,
    paddingRight: 24,
    // paddingHorizontal: 24,
  },
  termNumber: {
    fontFamily: "PingFang",
    fontSize: 9,
    width: 18,
  },
  normalTxt: {
    fontFamily: "PingFang",
    fontSize: 9,
    fontWeight: 400,
  },
  normalEngTxt: {
    fontFamily: "PingFang",
    fontSize: 8.5,
    fontWeight: 400,
  },
});
