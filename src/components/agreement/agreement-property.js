import { FormItem, Textarea, Select } from "../input";

function AgreementProperty({ currentStep, type }) {
  return (
    <div className={`${currentStep === 1 ? "block" : "hidden"}`}>
      <h5 className="text-xl font-bold dark:text-white">負責費用</h5>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-8 md:gap-5 mt-6">
        <FormItem label="管理費">
          <Select defaultValue={0} name="managementFee">
            <option value="" disabled>
              請選擇管理費
            </option>
            <option value={0}>業主</option>
            <option value={1}>租客</option>
          </Select>
        </FormItem>

        <FormItem label="差餉">
          <Select defaultValue={0} name="governmentRate">
            <option value="" disabled>
              請選擇差餉
            </option>
            <option value={0}>業主</option>
            <option value={1}>租客</option>
          </Select>
        </FormItem>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-8 md:gap-5 mt-6">
        <FormItem label="地租">
          <Select defaultValue={0} name="governmentRentRate">
            <option value="" disabled>
              請選擇地租
            </option>
            <option value={0}>業主</option>
            <option value={1}>租客</option>
          </Select>
        </FormItem>

        {type === 1 && (
          <FormItem label="電費">
            <Select defaultValue={1} name="electricityBill">
              <option value="" disabled>
                請選擇電費
              </option>
              <option value={0}>業主</option>
              <option value={1}>租客</option>
            </Select>
          </FormItem>
        )}
      </div>
      {type === 1 && (
        <div className="grid grid-cols-1 md:grid-cols-2 gap-8 md:gap-5 mt-6">
          <FormItem label="煤氣費">
            <Select defaultValue={1} name="gasBill">
              <option value="" disabled>
                請選擇煤氣費
              </option>
              <option value={0}>業主</option>
              <option value={1}>租客</option>
            </Select>
          </FormItem>

          <FormItem label="水費">
            <Select defaultValue={1} name="waterBill">
              <option value="" disabled>
                請選擇水費
              </option>
              <option value={0}>業主</option>
              <option value={1}>租客</option>
            </Select>
          </FormItem>
        </div>
      )}

      {type === 1 && (
        <>
          <h5 className="text-xl font-bold dark:text-white mt-12">
            傢俬及電器
          </h5>
          <div className="grid grid-cols-1 md:grid-cols-2 gap-8 md:gap-5 mt-6">
            <FormItem label="雪櫃(可選填)">
              <Select defaultValue={0} name="refrigerator">
                <option value={0}>0部</option>
                <option value={1}>1部</option>
                <option value={2}>2部</option>
                <option value={3}>3部</option>
                <option value={4}>4部</option>
                <option value={5}>5部</option>
                <option value={6}>6部</option>
                <option value={7}>7部</option>
                <option value={8}>8部</option>
                <option value={9}>9部</option>
                <option value={10}>10部</option>
              </Select>
            </FormItem>

            <FormItem label="窗口式冷氣機(可選填)">
              <Select defaultValue={0} name="windowConditioner">
                <option value={0}>0部</option>
                <option value={1}>1部</option>
                <option value={2}>2部</option>
                <option value={3}>3部</option>
                <option value={4}>4部</option>
                <option value={5}>5部</option>
                <option value={6}>6部</option>
                <option value={7}>7部</option>
                <option value={8}>8部</option>
                <option value={9}>9部</option>
                <option value={10}>10部</option>
              </Select>
            </FormItem>

            <FormItem label="分體式冷氣機(可選填)">
              <Select defaultValue={0} name="splitConditioner">
                <option value={0}>0部</option>
                <option value={1}>1部</option>
                <option value={2}>2部</option>
                <option value={3}>3部</option>
                <option value={4}>4部</option>
                <option value={5}>5部</option>
                <option value={6}>6部</option>
                <option value={7}>7部</option>
                <option value={8}>8部</option>
                <option value={9}>9部</option>
                <option value={10}>10部</option>
              </Select>
            </FormItem>
          </div>
          <div className="grid grid-cols-1 md:grid-cols-2 gap-8 md:gap-5 mt-6">
            <FormItem label="電視機(可選填)">
              <Select defaultValue={0} name="television">
                <option value={0}>0部</option>
                <option value={1}>1部</option>
                <option value={2}>2部</option>
                <option value={3}>3部</option>
                <option value={4}>4部</option>
                <option value={5}>5部</option>
                <option value={6}>6部</option>
                <option value={7}>7部</option>
                <option value={8}>8部</option>
                <option value={9}>9部</option>
                <option value={10}>10部</option>
              </Select>
            </FormItem>

            <FormItem label="冷氣機(可選填)">
              <Select defaultValue={0} name="airConditioner">
                <option value={0}>0部</option>
                <option value={1}>1部</option>
                <option value={2}>2部</option>
                <option value={3}>3部</option>
                <option value={4}>4部</option>
                <option value={5}>5部</option>
                <option value={6}>6部</option>
                <option value={7}>7部</option>
                <option value={8}>8部</option>
                <option value={9}>9部</option>
                <option value={10}>10部</option>
              </Select>
            </FormItem>
          </div>
          <div className="grid grid-cols-1 md:grid-cols-2 gap-8 md:gap-5 mt-6">
            <FormItem label="微波爐(可選填)">
              <Select defaultValue={0} name="microwave">
                <option value={0}>0部</option>
                <option value={1}>1部</option>
                <option value={2}>2部</option>
                <option value={3}>3部</option>
                <option value={4}>4部</option>
                <option value={5}>5部</option>
                <option value={6}>6部</option>
                <option value={7}>7部</option>
                <option value={8}>8部</option>
                <option value={9}>9部</option>
                <option value={10}>10部</option>
              </Select>
            </FormItem>

            <FormItem label="抽油煙機(可選填)">
              <Select defaultValue={0} name="rangeHood">
                <option value={0}>0部</option>
                <option value={1}>1部</option>
                <option value={2}>2部</option>
                <option value={3}>3部</option>
                <option value={4}>4部</option>
                <option value={5}>5部</option>
                <option value={6}>6部</option>
                <option value={7}>7部</option>
                <option value={8}>8部</option>
                <option value={9}>9部</option>
                <option value={10}>10部</option>
              </Select>
            </FormItem>
          </div>
          <div className="grid grid-cols-1 md:grid-cols-2 gap-8 md:gap-5 mt-6">
            <FormItem label="煮食爐(可選填)">
              <Select defaultValue={0} name="cookingStove">
                <option value={0}>0部</option>
                <option value={1}>1部</option>
                <option value={2}>2部</option>
                <option value={3}>3部</option>
                <option value={4}>4部</option>
                <option value={5}>5部</option>
                <option value={6}>6部</option>
                <option value={7}>7部</option>
                <option value={8}>8部</option>
                <option value={9}>9部</option>
                <option value={10}>10部</option>
              </Select>
            </FormItem>

            <FormItem label="洗衣機(可選填)">
              <Select defaultValue={0} name="washingMachine">
                <option value={0}>0部</option>
                <option value={1}>1部</option>
                <option value={2}>2部</option>
                <option value={3}>3部</option>
                <option value={4}>4部</option>
                <option value={5}>5部</option>
                <option value={6}>6部</option>
                <option value={7}>7部</option>
                <option value={8}>8部</option>
                <option value={9}>9部</option>
                <option value={10}>10部</option>
              </Select>
            </FormItem>
          </div>

          <div className="grid grid-cols-1 gap-8 md:gap-5 mt-6">
            <FormItem label="其他傢俬及電器">
              <Textarea
                type="text"
                name="otherFacility"
                placeholder="請輸入其他傢俬及電器"
              />
            </FormItem>
          </div>
        </>
      )}
    </div>
  );
}

export default AgreementProperty;
