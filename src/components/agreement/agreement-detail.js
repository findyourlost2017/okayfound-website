import { FormItem, Input, Textarea } from "../input";

function AgreementDetail({ currentStep, type, setType }) {
  return (
    <div className={`${currentStep === 2 ? "block" : "hidden"}`}>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-8 md:gap-5 mt-6">
        <FormItem label="訂立此合約之日期(可選填)">
          <Input
            type="date"
            name="contractDate"
            placeholder="訂立此合約之日期"
          />
        </FormItem>
        <FormItem label="租約開始日(可選填)">
          <Input
            type="date"
            name="tenancyStart"
            placeholder="請輸入租約開始日"
          />
        </FormItem>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-8 md:gap-5 mt-6">
        <FormItem label="租約到期日(可選填)">
          <Input type="date" name="tenancyEnd" placeholder="請選擇租約到期日" />
        </FormItem>
        <FormItem label="死約月份(可選填)">
          <div className="relative">
            <Input
              type="text"
              defaultValue={12}
              name="tenancyForceYear"
              className="!pr-10"
              placeholder="請輸入死約月份"
            />
            <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
              <span className="text-gray-500">個月</span>
            </div>
          </div>
        </FormItem>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-8 md:gap-5 mt-6">
        <FormItem label="結束租約提前通知期(可選填)">
          <div className="relative">
            <Input
              type="text"
              name="tenancyForceYear"
              className="!pr-10"
              defaultValue={1}
              placeholder="請選擇結束租約提前通知期"
            />
            <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
              <span className="text-gray-500">個月</span>
            </div>
          </div>
        </FormItem>
        <FormItem label="每月租金(可選填)">
          <div className="relative">
            <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
              <span className="text-gray-500">$</span>
            </div>
            <Input
              type="number"
              name="rentPrice"
              className="!pl-8 !pr-10"
              placeholder="請輸入每月租金"
            />
            <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
              <span className="text-gray-500">HKD</span>
            </div>
          </div>
        </FormItem>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-8 md:gap-5 mt-6">
        <FormItem label="每月交租日(可選填)">
          <div className="relative">
            <Input
              type="number"
              name="tenancySubmitPriceDay"
              className="!pr-10"
              defaultValue={1}
              placeholder="請輸入每月交租日"
            />
            <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
              <span className="text-gray-500">號</span>
            </div>
          </div>
        </FormItem>
        <FormItem label="保證金(上期)(可選填)">
          <div className="relative">
            <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
              <span className="text-gray-500">$</span>
            </div>
            <Input
              type="number"
              name="securityDeposit"
              className="!pl-8 !pr-10"
              placeholder="請輸入保證金"
            />
            <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
              <span className="text-gray-500">HKD</span>
            </div>
          </div>
        </FormItem>
      </div>

      <div className="grid grid-cols-1 gap-8 md:gap-5 mt-6">
        <FormItem label="其他備註">
          <Textarea type="text" name="remark" placeholder="請輸入其他備註" />
        </FormItem>
      </div>
    </div>
  );
}

export default AgreementDetail;
