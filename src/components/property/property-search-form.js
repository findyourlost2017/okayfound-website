import Link from "next/link";
import { PriceRangeInput, PropertyTypeSelect, DistrictSelect } from "../input";

function HomeSearchForm({
  price,
  setPrice,
  propertyType,
  setPropertyType,
  subPropertyType,
  setSubPropertyType,
  propertyDistrict,
  setPropertyDistrict,
  searchProperty,
}) {
  return (
    <div
      className="nc-HeroRealEstateSearchForm w-full py-5 lg:py-0"
      data-nc-id="HeroRealEstateSearchForm"
    >
      <form className="w-full relative xl:mt-8 flex flex-col lg:flex-row lg:items-center rounded-3xl lg:rounded-full shadow-xl dark:shadow-2xl bg-white dark:bg-neutral-800 divide-y divide-neutral-200 dark:divide-neutral-700 lg:divide-y-0">
        <DistrictSelect
          propertyDistrict={propertyDistrict}
          setPropertyDistrict={setPropertyDistrict}
        />
        <PropertyTypeSelect
          propertyType={propertyType}
          setPropertyType={setPropertyType}
          subPropertyType={subPropertyType}
          setSubPropertyType={setSubPropertyType}
        />
        <PriceRangeInput price={price} setPrice={setPrice} />
        <div className="pr-2 xl:pr-4">
          <Link href="#">
            <a
              type="button"
              onClick={searchProperty}
              className="cursor-pointer h-14 md:h-16 w-full md:w-16 rounded-full bg-orange-500 hover:bg-orange-700 flex items-center justify-center text-neutral-50 focus:outline-none"
            >
              <span className="mr-3 md:hidden">搜尋</span>
              <i className="fa-solid fa-magnifying-glass"></i>
            </a>
          </Link>
        </div>
      </form>
    </div>
  );
}

export default HomeSearchForm;
