function PropertyParkingOther({ parkingOther }) {
  const renderContent = () => {
    return (
      <div>
        {/* <span className="block mt-2 text-neutral-500 dark:text-neutral-400">
         其他
        </span> */}
        <div className="w-14 border-b border-neutral-200 dark:border-neutral-700" />
        <div className="mt-5 gap-6 text-sm text-neutral-700 dark:text-neutral-300 ">
          {parkingOther.map((item) => {
            return (
              <div key={item} className="flex mt-2 items-center space-x-3">
                <i className={`text-3xl las la-check-circle`}></i>

                {/* <i className={`${iconMapping[item]}`}></i> */}
                <span className=" ">{item}</span>
              </div>
            );
          })}
        </div>
      </div>
    );
  };

  return (
    <div className="listingSection__wrap">
      <div>
        <h2 className="text-2xl font-semibold">其他資訊</h2>
      </div>

      {renderContent()}
    </div>
  );
}

export default PropertyParkingOther;
