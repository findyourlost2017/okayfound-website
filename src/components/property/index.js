import PropertyPhotos from "./property-photos";
import PropertyBasicInfo from "./property-basic-info";
import PropertyIntro from "./property-intro";
import PropertyFacility from "./property-facility";
import PropertyParkingOther from "./property-parking-other";
import PropertyNearFacility from "./property-near-facility";
import PropertyMap from "./property-map";
import PropertySidebar from "./property-sidebar";
import PropertyRestaurant from "./property-restaurant";
import PropertyPetro from "./property-petro";
import PropertySearchForm from "./property-search-form";
import PropertyCard from "./property-card";

export {
  PropertyPhotos,
  PropertyBasicInfo,
  PropertyIntro,
  PropertyFacility,
  PropertyParkingOther,
  PropertyNearFacility,
  PropertyMap,
  PropertySidebar,
  PropertyRestaurant,
  PropertySearchForm,
  PropertyCard,
  PropertyPetro,
};
