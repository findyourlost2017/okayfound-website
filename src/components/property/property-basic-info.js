import appConfig from "../../utils/appConfig";
import { Badge, NcModal } from "../common";
import ShareModal from "../property/share-modal";

function PropertyBasicInfo({ data }) {
  return (
    <div className="listingSection__wrap !space-y-6">
      <div className="flex justify-between items-center">
        <div>
          <Badge name={data.estate_name} />
          {data.allow_pet === 0 && (
            <Badge name="寵物友善租盤" color="pink" className="ml-4" />
          )}
        </div>

        <div className="flow-root">
          <div className="flex text-neutral-700 dark:text-neutral-300 text-sm -mx-3 -my-1.5">
            <NcModal
              renderTrigger={(openModal) => (
                <span
                  onClick={openModal}
                  className="py-1.5 px-3 flex rounded-lg hover:bg-neutral-100 cursor-pointer"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-5 w-5 "
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={1.5}
                      d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-8l-4-4m0 0L8 8m4-4v12"
                    />
                  </svg>
                  <span className="hidden sm:block ml-2.5">分享</span>
                </span>
              )}
              renderContent={(closeModal) => (
                <ShareModal closeModal={closeModal} data={data} />
              )}
              modalTitle="分享租盤"
              contentExtraClass="w-6/12 min-h-0"
            />
          </div>
        </div>
      </div>

      <h2 className="text-2xl sm:text-3xl lg:text-4xl font-semibold">
        {data.title}
      </h2>

      <div className="flex items-center space-x-4">
        <span>
          <i className="las la-map-marker-alt"></i>
          <span className="ml-1">
            {data.estate_address
              ? data.estate_address
              : `${data.district_name} ${data.estate_name}`}
          </span>
        </span>
      </div>

      <div className="w-full border-b border-neutral-100 dark:border-neutral-700" />

      {/* 住宅 INFO */}
      {data.property_type === appConfig.PROPERTY_TYPE["住宅"] && (
        <div className="flex items-center justify-between xl:justify-start space-x-8 xl:space-x-12 text-sm text-neutral-700 dark:text-neutral-300">
          <div className="flex items-center space-x-4">
            <i className="fa-solid fa-house"></i>
            <span className=" ">{data.number_of_room}</span>
          </div>
          <div className="flex items-center space-x-4 ">
            <i className="fa-solid fa-ruler-triangle"></i>
            <span className="">{data.saleable_feet}呎</span>
          </div>

          <div className="flex items-center space-x-4">
            <i className="fa-solid fa-bath"></i>
            <span className=" ">{data.number_of_bath_room}廁</span>
          </div>

          <div className="flex items-center space-x-4">
            <i className="fa-solid fa-sack-dollar"></i>
            <span className=" ">
              {data.is_commission === "N" ? "業主盤" : "經紀盤"}
            </span>
          </div>
        </div>
      )}
      {/* 車位 INFO */}
      {data.property_type === appConfig.PROPERTY_TYPE["車位"] && (
        <div className="flex items-center justify-between xl:justify-start space-x-8 xl:space-x-12 text-sm text-neutral-700 dark:text-neutral-300">
          <div className="flex items-center space-x-4">
            <i className="fa-solid fa-circle-parking"></i>
            <span className=" ">
              {data.parking_location === "露天" && data.open_parking
                ? `${data.parking_location}(${data.open_parking})`
                : data.parking_location}
            </span>
          </div>
        </div>
      )}
    </div>
  );
}

export default PropertyBasicInfo;
