import React from "react";

function PropertyBasicInfo({ data }) {
  function replaceWithBr() {
    return urlify(data.description).replace(/\n/g, "<br />");
  }

  function urlify(text) {
    var urlRegex = /(https?:\/\/[^\s]+)/g;
    return text.replace(urlRegex, function (url) {
      return (
        '<a class="text-blue-500 underline" href="' +
        url +
        '" target="_blank">' +
        url +
        "</a>"
      );
    });
    // or alternatively
    // return text.replace(urlRegex, '<a href="$1">$1</a>')
  }

  return (
    <div className="listingSection__wrap">
      <h2 className="text-2xl font-semibold">業主介紹</h2>
      <div className="w-14 border-b border-neutral-200 dark:border-neutral-700"></div>
      <div className="text-neutral-6000 dark:text-neutral-300">
        <span dangerouslySetInnerHTML={{ __html: replaceWithBr() }} />
      </div>
    </div>
  );
}

export default PropertyBasicInfo;
