import Link from "next/link";
import convertNumbThousand from "../../utils/convertNumbThousand";
import { Badge, GallerySlider } from "../common";
import appConfig from "../../utils/appConfig";
import NcModal from "../common/nc-modal";
import ButtonPrimary from "../button/button-primary";
import ButtonSecondary from "../button/button-secondary";
import AlertModal from "../common/alert-modal";
import LoadingCard from "../common/loading-card";
import { eventTracker } from "../../utils/gaEventTracker";

function PropertyCard({
  data,
  editable,
  editProperty,
  disabled,
  deleteProperty,
  stopProperty,
  repostProperty,
}) {
  const renderPropertyIntro = () => {
    if (data.property_type === 0)
      return `${data.estate_name} · ${data.number_of_room} · ${data.saleable_feet}呎`;
    else if (data.property_type === 2) {
      return `${data.estate_name} · ${data.parking_location}`;
    }
  };

  const renderSliderGallery = () => {
    return (
      <div className="relative w-full">
        {data.photos && (
          <GallerySlider
            uniqueID={`StayCard_${data.property_id}`}
            ratioClass="aspect-w-4 aspect-h-3 "
            galleryImgs={data.photos}
            href={`/property/${data.property_id}`}
          />
        )}

        {data.status === appConfig.PROPERTY_STATUS["STOPPED"] && (
          <div className="absolute left-3 top-3 flex items-center justify-center text-xs py-1 px-4 bg-red-700 text-red-50 rounded-full">
            已暫停
          </div>
        )}
      </div>
    );
  };

  const renderContent = () => {
    return (
      <div className={`p-4 space-y-4 ${disabled ? "" : "cursor - pointer"}`}>
        <div className="space-y-2">
          <span className="text-sm text-neutral-500 dark:text-neutral-400">
            {renderPropertyIntro()}
          </span>
          <div className="flex items-center space-x-2">
            <h2 className="font-medium capitalize text-lg">
              <span className="line-clamp-1">{data.title}</span>
            </h2>
          </div>
          <div className="flex items-center text-neutral-500 dark:text-neutral-400 text-sm space-x-2">
            <i className=" text-xl fa-regular fa-location-dot"></i>
            <span className="">
              {data.estate_address
                ? data.estate_address
                : `${data.district_name} ${data.estate_name}`}
            </span>
          </div>
        </div>
        <div className="w-14 border-b border-neutral-100 dark:border-neutral-800" />
        <div className="flex justify-between items-center">
          <span className="text-base font-semibold">
            ${convertNumbThousand(data.price)}
            {` `}
            <span className="text-sm text-neutral-500 dark:text-neutral-400 font-normal">
              /月
            </span>
          </span>

          <Badge
            name={
              <div className="flex items-center">
                <i
                  className={`text-sm ${
                    appConfig.PROPERTY_ICON[data.property_type]
                  }`}
                ></i>
                <span className="ml-1">
                  {data.property_type === appConfig.PROPERTY_TYPE["車位"]
                    ? "車位"
                    : appConfig.propertyTypeMapping[data.estate_type]}
                </span>
              </div>
            }
            color="gray"
          />
        </div>
      </div>
    );
  };

  const renderEditRow = () => {
    return (
      <div className="p-4 pt-0 space-y-4 cursor-pointer">
        <ButtonPrimary
          onClick={() => editProperty(data)}
          className="mr-4 px-4 sm:px-4 py-2"
          fontSize="sm:text-sm"
        >
          修改租盤
        </ButtonPrimary>

        <NcModal
          containerClass="inline"
          renderTrigger={(openModal) => (
            <ButtonSecondary
              onClick={openModal}
              className="mr-4 px-4 sm:px-4 py-2 bg-red-500 hover:bg-red-600"
              fontSize="sm:text-sm text-white"
            >
              刪除租盤
            </ButtonSecondary>
          )}
          renderContent={(closeModal) => (
            <AlertModal
              title="你確定要刪除這個租盤嗎？"
              onConfirm={() => deleteProperty(data.property_id)}
              closeModal={closeModal}
            />
          )}
          modalTitle="提示"
          contentExtraClass="w-6/12 min-h-0"
        />

        {data.status !== appConfig.PROPERTY_STATUS["STOPPED"] && (
          <ButtonSecondary
            className="mr-4 px-4 sm:px-4 py-2 bg-neutral-500 hover:bg-neutral-600"
            fontSize="sm:text-sm text-white"
            onClick={() => stopProperty(data.property_id)}
          >
            暫停刊登
          </ButtonSecondary>
        )}

        {data.status === appConfig.PROPERTY_STATUS["STOPPED"] && (
          <ButtonSecondary
            className="mr-4 px-4 sm:px-4 py-2 bg-neutral-500 hover:bg-neutral-600"
            fontSize="sm:text-sm text-white"
            onClick={() => repostProperty(data.property_id)}
          >
            重新刊登
          </ButtonSecondary>
        )}
      </div>
    );
  };

  const handleClick = (title, propertyId) => {
    eventTracker({
      action: `${title}`,
      category: "property",
      label: `property_${propertyId}`,
      value: 1,
    });
  };

  if (data.loading) return <LoadingCard />;
  else
    return (
      <div className="nc-StayCard group relative bg-white border border-neutral-100 rounded-2xl overflow-hidden will-change-transform hover:shadow-xl transition-shadow">
        {renderSliderGallery()}
        {!disabled && (
          <Link href={`/property/${data.property_id}`}>
            <a
              target="_blank"
              rel="noopener noreferrer"
              onClick={() => handleClick(data.title, data.property_id)}
            >
              {renderContent()}
            </a>
          </Link>
        )}
        {disabled && renderContent()}
        {editable && renderEditRow()}
      </div>
    );
}

export default PropertyCard;
