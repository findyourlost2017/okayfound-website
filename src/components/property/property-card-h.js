import Link from "next/link";
import convertNumbThousand from "../../utils/convertNumbThousand";
import appConfig from "../../utils/appConfig";
import { Badge, GallerySlider } from "../common";

function PropertyCardH({ item, index, className }) {
  const renderSliderGallery = () => {
    return (
      <div className="flex-shrink-0 p-3 w-full sm:w-64 ">
        <GallerySlider
          ratioClass="aspect-w-1 aspect-h-1"
          galleryImgs={item.photos}
          className="w-full h-full rounded-2xl overflow-hidden will-change-transform"
          uniqueID={`PropertyCardH_${item.property_id}`}
        />

        {item.prepay_discount && (
          <div
            className={`nc-SaleOffBadge flex items-center justify-center text-xs py-0.5 px-3 text-red-50 rounded-full absolute left-5 top-5`}
            style={{ backgroundColor: "#00826E", borderRadius: 4 }}
            data-nc-id="SaleOffBadge"
          >
            ${convertNumbThousand(item.prepay_discount)} (預繳$
            {item.prepay_month}個月)
          </div>
        )}
      </div>
    );
  };

  const renderTienIch = () => {
    return (
      <div className="inline-grid grid-cols-3 gap-2">
        {item.property_type === appConfig.PROPERTY_TYPE["住宅"] && (
          <div className="flex items-center space-x-2">
            <span className="hidden sm:inline-block">
              <i className="las la-bed text-lg"></i>
            </span>
            <span className="text-xs text-neutral-500 dark:text-neutral-400">
              {item.number_of_room}
            </span>
          </div>
        )}

        {item.property_type === appConfig.PROPERTY_TYPE["住宅"] && (
          <div className="flex items-center space-x-2">
            <span className="hidden sm:inline-block">
              <i className="las la-bath text-lg"></i>
            </span>
            <span className="text-xs text-neutral-500 dark:text-neutral-400">
              {item.number_of_bath_room} 廁
            </span>
          </div>
        )}

        {item.property_type === appConfig.PROPERTY_TYPE["住宅"] && (
          <div className="flex items-center space-x-2">
            <span className="hidden sm:inline-block">
              <i className="las la-expand-arrows-alt text-lg"></i>
            </span>
            <span className="text-xs text-neutral-500 dark:text-neutral-400">
              {item.saleable_feet} 呎
            </span>
          </div>
        )}

        {/* {item.property_type === appConfig.PROPERTY_TYPE["車位"] && (
          <div className="flex items-center space-x-2">
            <span className="hidden sm:inline-block">
              <i className="fa-solid fa-circle-parking"></i>
            </span>
            <span className="text-xs text-neutral-500 dark:text-neutral-400">
              {item.parking_location === "露天" && item.open_parking
                ? `${item.parking_location}(${item.open_parking})`
                : item.parking_location}
            </span>
          </div>
        )} */}
      </div>
    );
  };

  const renderContent = () => {
    return (
      <div className="flex-grow p-3 sm:pr-6 flex flex-col items-start">
        <div className="space-y-4 w-full">
          <div className="inline-flex space-x-3">
            <Badge
              name={
                <div className="flex items-center">
                  <i className="text-sm las la-map-marker-alt"></i>
                  <span className="ml-1">{item.district_name}</span>
                </div>
              }
            />
            {item.property_type !== appConfig.PROPERTY_TYPE["車位"] && (
              <Badge
                name={
                  <div className="flex items-center">
                    <i className="text-sm las la-home"></i>
                    <span className="ml-1">
                      {
                        appConfig.propertyTypeMapping[
                          item.estate_type ? item.estate_type : ""
                        ]
                      }
                    </span>
                  </div>
                }
                color="gray"
              />
            )}
            {item.property_type === appConfig.PROPERTY_TYPE["住宅"] && (
              <Badge
                name={
                  <div className="flex items-center">
                    <i className="text-sm las la-info-circle"></i>
                    <span className="ml-1">{item.floor}</span>
                  </div>
                }
                color="yellow"
              />
            )}
            {item.property_type === appConfig.PROPERTY_TYPE["車位"] && (
              <Badge
                name={
                  <div className="flex items-center">
                    <i className="text-sm las la-info-circle"></i>
                    <span className="ml-1">{item.parking_location}</span>
                  </div>
                }
                color="yellow"
              />
            )}
          </div>
          <div className="flex items-center space-x-2">
            {/* <Badge name="廣告" color="green" /> */}
            <h2 className="text-lg font-medium capitalize">
              <span className="line-clamp-2">{item.title}</span>
            </h2>
          </div>
          {renderTienIch()}
          {/* <div className="w-14 border-b border-neutral-100 dark:border-neutral-800 "></div> */}
          <div className="flex w-full justify-between items-end">
            <div className="inline-flex space-x-3">
              <Badge name={item.estate_name} color="green" />
              {/* <Badge
                name={appConfig.propertyTypeMapping[item.estate_type]}
                color="indigo"
              /> */}
            </div>

            <span className="flex items-center justify-center px-3 py-2 border border-secondary-500 rounded leading-none text-base font-medium text-secondary-500">
              {`$${convertNumbThousand(item.price)}`}
            </span>
          </div>
        </div>
      </div>
    );
  };

  const handleClick = (title, propertyId) => {
    eventTracker({
      action: `${title}`,
      category: "property",
      label: `property_${propertyId}`,
      value: 1,
    });
  };

  return (
    <div
      className={`nc-PropertyCardH group relative bg-white dark:bg-neutral-900 border border-neutral-100 dark:border-neutral-800 rounded-3xl overflow-hidden hover:shadow-xl transition-shadow ${className}`}
      data-nc-id="PropertyCardH"
    >
      <Link href={`/property/${item.property_id}`} target="_blank">
        <a
          className="absolute inset-0"
          target="_blank"
          onClick={() => handleClick(item.title, item.property_id)}
        />
      </Link>
      <div className="h-full w-full flex flex-col sm:flex-row sm:items-center">
        {renderSliderGallery()}
        {renderContent()}
      </div>
    </div>
  );
}

export default PropertyCardH;
