import moment from "moment";
import convertNumbThousand from "../../utils/convertNumbThousand";
import ButtonPrimary from "../button/button-primary";

function PropertySidebar({ data, onShowChat, member }) {
  return (
    <div className="hidden lg:block flex-grow mt-14 lg:mt-0">
      <div className="sticky top-28">
        <div className="listingSectionSidebar__wrap shadow-xl">
          <div className="flex justify-between">
            <span className="text-3xl font-semibold">
              ${convertNumbThousand(data.price)}
              <span className="ml-1 text-base font-normal text-neutral-500 dark:text-neutral-400">
                /月
              </span>
            </span>
          </div>

          <div>
            <div className="flex items-center space-x-3">
              <i className="fa-solid fa-building"></i>
              <span>{data.estate_name}</span>
            </div>

            <div className="flex items-center space-x-3 mt-1">
              <i className="fa-solid fa-building"></i>
              <span>
                {new Date().getFullYear() - data.estate_entrance_year}年樓
              </span>
            </div>

            {data.man_company && (
              <div className="flex items-center space-x-3 mt-1">
                <i className="fa-solid fa-screwdriver-wrench"></i>
                <span>{data.man_company}</span>
              </div>
            )}

            {data.developer && (
              <div className="flex items-center space-x-3 mt-1">
                <i className="fa-solid fa-hammer"></i>
                <span>{data.developer}</span>
              </div>
            )}

            {data.estate_facility && (
              <div className="flex items-center space-x-3 mt-1 max-w-xs	">
                <i className="fa-solid fa-dumbbell" />
                <span>{data.estate_facility}</span>
              </div>
            )}

            <div className="flex items-center space-x-3 mt-1">
              <i className="fa-solid fa-clock" />
              <span>
                刊登於 {moment.unix(data.created_at).format("YYYY-MM-DD")}
              </span>
            </div>
            {data.minimal_start && (
              <div className="flex items-center space-x-3 mt-1">
                <i className="fa-solid fa-clock" />
                <span>
                  最快起租日 {moment(data.minimal_start).format("YYYY-MM-DD")}
                </span>
              </div>
            )}
          </div>

          {(!member || (member && member.member_id !== data.member_id)) && (
            <ButtonPrimary
              className="bg-orange-500 hover:bg-orange-500"
              onClick={onShowChat}
            >
              聯絡業主
            </ButtonPrimary>
          )}
        </div>
      </div>
    </div>
  );
}

export default PropertySidebar;
