import { useMemo, useEffect } from "react";
import _ from "lodash";
import { v4 as uuidv4 } from "uuid";
import Glide from "@glidejs/glide";
import { Heading, NcImage, NextPrev } from "../common";

function PropertyPetro({ petro }) {
  const UNIQUE_CLASS = "SectionSliderNewCategories__PETRO";

  let MY_GLIDEJS = useMemo(() => {
    return new Glide(`.${UNIQUE_CLASS}`, {
      perView: 5,
      gap: 32,
      bound: true,
      breakpoints: {
        1280: {
          perView: 5 - 1,
        },
        1024: {
          gap: 20,
          perView: 5 - 1,
        },
        768: {
          gap: 20,
          perView: 5 - 2,
        },
        640: {
          gap: 20,
          perView: 5 - 3,
        },
        500: {
          gap: 20,
          perView: 1.3,
        },
      },
    });
  }, [UNIQUE_CLASS]);

  useEffect(() => {
    setTimeout(() => {
      if (MY_GLIDEJS) MY_GLIDEJS.mount();
    }, 500);
  }, [MY_GLIDEJS, UNIQUE_CLASS]);

  const renderCard = (item) => {
    return (
      <div className="nc-CardCategory5 flex flex-col">
        <div className="flex-shrink-0 relative w-full aspect-w-4 aspect-h-3 h-0 rounded-2xl overflow-hidden group">
          <NcImage
            src={item.thumbnail}
            className="object-cover w-full h-full rounded-2xl"
          />
          <span className="opacity-0 group-hover:opacity-100 absolute inset-0 bg-black bg-opacity-10 transition-opacity"></span>
        </div>
        <div className="mt-4 px-3 truncate">
          <h2
            className={`text-base sm:text-lg text-neutral-900 dark:text-neutral-100 font-medium truncate`}
          >
            {item.brand}
          </h2>
          <span
            className={`block mt-2 text-sm text-neutral-6000 dark:text-neutral-400`}
          >
            {item.address}
          </span>
          <span
            className={`block mt-2 text-sm text-neutral-6000 dark:text-neutral-400`}
          >
            {item.distance}米
          </span>
        </div>
      </div>
    );
  };

  return (
    <div className="container py-24 lg:py-32">
      <div className="relative py-16">
        <div
          className="nc-BackgroundSection absolute inset-y-0 w-screen xl:max-w-[1340px] 2xl:max-w-screen-2xl left-1/2 transform -translate-x-1/2 xl:rounded-[40px] z-0 bg-neutral-100 dark:bg-black dark:bg-opacity-20"
          data-nc-id="BackgroundSection"
        />

        <div className="nc-SectionSliderNewCategories">
          <div className={`flow-root ${UNIQUE_CLASS}`}>
            <Heading
              desc={`超過${petro.length}間油站`}
              hasNextPrev={false}
              isCenter={true}
            >
              探索附近油站
            </Heading>

            <div className="glide__track" data-glide-el="track">
              <ul className="glide__slides">
                {petro.map((item, index) => (
                  <li key={`${index}_petro_glide`} className="glide__slide">
                    {renderCard(item, index)}
                  </li>
                ))}
              </ul>
            </div>

            <NextPrev className="justify-center mt-16" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default PropertyPetro;
