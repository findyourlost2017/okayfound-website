import GoogleMapReact from "google-map-react";

const Marker = (props) => (
  <div className="text-orange-500">
    <i className="text-3xl fa-solid fa-location-smile"></i>
  </div>
);

function PropertyMap({ lat, lng }) {
  return (
    <div className="listingSection__wrap">
      <div>
        <h2 className="text-2xl font-semibold">位置</h2>
      </div>
      <div className="w-14 border-b border-neutral-200 dark:border-neutral-700" />
      <div className="aspect-w-5 aspect-h-5 sm:aspect-h-3">
        <div className="rounded-xl overflow-hidden">
          <GoogleMapReact
            bootstrapURLKeys={{
              key: "AIzaSyCgyiczotqFCuY4fwwOAVonaQdFsGTGu88",
            }}
            yesIWantToUseGoogleMapApiInternals
            defaultZoom={18}
            defaultCenter={{
              lat: lat,
              lng: lng,
            }}
          >
            <Marker lat={lat} lng={lng} />
          </GoogleMapReact>
        </div>
      </div>
    </div>
  );
}

export default PropertyMap;
