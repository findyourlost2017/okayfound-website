import { useState, useEffect, useRef } from "react";
import { v4 as uuidv4 } from "uuid";
import { NcImage } from "../common";
import ChatItem from "../messages/chat-item";
import API from "../../utils/api";
import appConfig from "../../utils/appConfig";

function PropertyChatModal({ data, closeModal, member, token }) {
  const [messages, setMessages] = useState([]);
  const [input, setInput] = useState("");
  const scrollContentRef = useRef();

  const onSendMessage = (input, messageType) => {
    const messageID = uuidv4();
    const data = {
      _id: messageID,
      text: messageType === appConfig.MESSAGE_TYPE.text && input,
      loading: true,
      message_type: messageType,
      created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
      user: {
        _id: member.member_id,
        member_name: member.member_name,
      },
      sender_id: member.member_id,
    };

    setList((state) =>
      state.map((item) => {
        if (item.friend_id === currentChat.friend_id) {
          item.message = data;
        }
        return { ...item };
      }),
    );

    setMessages((state) => [...state, data]);
    if (messageType === appConfig.MESSAGE_TYPE.text) {
      sendMessageText(input, messageID);
    } else if (messageType === appConfig.MESSAGE_TYPE.image) {
      sendMessagePhoto(input, messageID);
    }
  };

  //   send chat message for text
  const sendMessageText = async (input, messageID) => {
    const api = new API(token);
    const response = await api.sendChatMessage({
      _id: messageID,
      user: {
        _id: session.member.member_id,
        member_name: session.member.member_name,
      },
      friend_id: currentChat.friend_id,
      text: input,
      message_type: appConfig.MESSAGE_TYPE.text,
      sender_id: session.member.member_id,
      property_id: currentChat.property_id,
      property_title: currentChat.property.title,
      thumbnail: currentChat.property.thumbnail,
      receiver_id: `${currentChat.other_member_id}`,
      created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
    });
    if (response && response.ok) {
      setMessages((state) =>
        state.map((item) => {
          if (item._id === messageID) {
            item.loading = false;
          }
          return { ...item };
        }),
      );
    } else {
      toast.warn("網絡出現錯誤，無法傳送訊息", {
        position: "top-center",
      });
      setMessages((state) => _.filter(state, (item) => item._id != messageID));
    }
  };

  const fetchChatMessage = async (propertyID) => {
    const api = new API(token);
    const response = await api.fetchChatMessage({
      property_id: propertyID,
    });
    if (response && response.ok) {
      setMessages(response.data.messages);
    }
  };

  useEffect(() => {
    fetchChatMessage(data.property_id);
    // resetChatBadge();
  }, []);

  return (
    <div className="w-80 h-96 flex flex-col rounded border shadow-md bg-white fixed bottom-0 right-10 z-50">
      <div className="flex items-center justify-between border-b p-2">
        <div className="flex items-center">
          {/* <NcImage className="rounded w-10 h-10" src={data.thumbnail} /> */}
          <div className="pl-2">
            <div className="font-semibold">
              <a className="hover:underline" href="#">
                {data.title}
              </a>
            </div>
            <div className="text-xs text-gray-600">Online</div>
          </div>
        </div>
        <div>
          <button
            className="inline-flex hover:bg-indigo-50 rounded p-2"
            type="button"
            onClick={closeModal}
          >
            <i className="text-2xl fa-solid fa-xmark"></i>
          </button>
        </div>
      </div>
      <div
        id="messages"
        ref={scrollContentRef}
        className="flex-1 flex flex-col space-y-1 py-3 overflow-y-auto scrollbar-thumb-blue scrollbar-thumb-rounded scrollbar-track-blue-lighter scrollbar-w-2 scrolling-touch"
      >
        {messages &&
          messages.map((item, index) => {
            const previousMessage = messages[index - 1]
              ? messages[index - 1]
              : null;

            return (
              <ChatItem
                member={member}
                item={item}
                previousMessage={previousMessage}
                // handleOpenModal={handleOpenModal}
                key={`${item._id}_chat_message`}
              />
            );
          })}
      </div>
      <div className="flex items-center border-t p-2">
        <div className="w-full mx-2">
          <input
            className="w-full rounded border border-gray-200"
            type="text"
            value={input}
            onChange={(e) => setInput(e.target.value)}
            placeholder="請輸入訊息"
            autofocus
          />
        </div>

        <div>
          <button
            className="inline-flex hover:bg-orange-50 rounded-full p-2"
            type="button"
            onClick={() => {
              if (input && input.length > 0)
                onSendMessage(input, appConfig.MESSAGE_TYPE["text"]);
            }}
          >
            <i className="fa-solid fa-paper-plane-top"></i>
          </button>
        </div>
      </div>
    </div>
  );
}

export default PropertyChatModal;
