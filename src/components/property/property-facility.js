function PropertyFacility({ facilities }) {
  const facilityMapping = {
    電器: [
      "電視",
      "冷氣機",
      "洗衣機",
      "浴室寶",
      "熱水爐",
      "煮食爐",
      "雪櫃",
      "微波爐",
      "抽油煙機",
      "焗爐",
    ],
    傢俬: ["地台", "床", "衣櫃", "鞋櫃", "組合櫃", "飯枱", "書枱"],
    其他: ["Wifi"],
  };

  const iconMapping = {
    電視: "fa-solid fa-tv-retro",
    冷氣機: "fa-solid fa-air-conditioner",
    洗衣機: "fa-solid fa-washing-machine",
    浴室寶: "fa-solid fa-vent-damper",
    熱水爐: "fa-solid fa-temperature-arrow-up",
    煮食爐: "fa-regular fa-fire-burner",
    雪櫃: "fa-solid fa-refrigerator",
    微波爐: "fa-solid fa-microwave",
    抽油煙機: "fa-solid fa-circle-info",
    焗爐: "fa-solid fa-oven",
    地台: "fa-solid fa-objects-column",
    床: "fa-solid fa-bed-front",
    衣櫃: "fa-solid fa-clothes-hanger",
    鞋櫃: "fa-solid fa-cabinet-filing",
    組合櫃: "fa-solid fa-cabinet-filing",
    飯枱: "fa-solid fa-table-picnic",
    書枱: "fa-solid fa-table-picnic",
    Wifi: "fa-solid fa-wifi",
  };

  const checkFacilityExist = (title) => {
    let exist = false;
    facilities &&
      facilities.map((item) => {
        if (facilityMapping[title].indexOf(item) > -1) exist = true;
      });
    return exist;
  };

  const renderFacility = (title) => {
    return (
      <div>
        <span className="block mt-2 text-neutral-500 dark:text-neutral-400">
          {title}
        </span>
        <div className="mt-2 w-14 border-b border-neutral-200 dark:border-neutral-700" />
        <div className="mt-5 grid grid-cols-1 xl:grid-cols-3 gap-6 text-sm text-neutral-700 dark:text-neutral-300 ">
          {facilities.map((item) => {
            if (facilityMapping[title].indexOf(item) > -1)
              return (
                <div key={item} className="flex items-center space-x-3">
                  {/* <i className={`text-3xl las la-check-circle`}></i> */}

                  <i className={`${iconMapping[item]}`}></i>
                  <span className=" ">{item}</span>
                </div>
              );
          })}
        </div>
      </div>
    );
  };

  return (
    <div className="listingSection__wrap">
      <div>
        <h2 className="text-2xl font-semibold">單位設備</h2>
        {/* <span className="block mt-2 text-neutral-500 dark:text-neutral-400">
          業主提供的設備
        </span> */}
      </div>

      {checkFacilityExist("電器") && renderFacility("電器")}
      {checkFacilityExist("傢俬") && renderFacility("傢俬")}
      {checkFacilityExist("其他") && renderFacility("其他")}
    </div>
  );
}

export default PropertyFacility;
