function PropertyNearFacility({ shop, mall, transport }) {
  const renderFacility = (title, list, icon) => {
    return (
      <div>
        <span className="block mt-2 text-neutral-500 dark:text-neutral-400">
          {title}
        </span>
        <div className="mt-2 w-14 border-b border-neutral-200 dark:border-neutral-700" />
        <div className="mt-5 grid grid-cols-1 xl:grid-cols-3 gap-6 text-sm text-neutral-700 dark:text-neutral-300 ">
          {list.map((item) => (
            <div key={item.name} className="flex items-center space-x-3">
              <i className={`${icon}`}></i>
              <span className=" ">
                {item.name} ({item.distance}米)
              </span>
            </div>
          ))}
        </div>
      </div>
    );
  };

  return (
    <div className="listingSection__wrap">
      <div>
        <h2 className="text-2xl font-semibold">附近設施</h2>
      </div>
      {transport &&
        transport.length > 0 &&
        renderFacility("港鐵站", transport, "fa-solid fa-train-subway")}
      {mall &&
        mall.length > 0 &&
        renderFacility("商場", mall, "fa-solid fa-city")}
      {shop &&
        shop.length > 0 &&
        renderFacility("店鋪", shop, "fa-solid fa-store")}
    </div>
  );
}

export default PropertyNearFacility;
