import { useRef } from "react";
import { toast } from "react-toastify";
import { useRouter } from "next/router";

function ShareModal({ data, closeModal }) {
  const textAreaRef = useRef(null);
  const router = useRouter();

  const handleFocus = (event) => {
    event.target.select();
  };

  const copyProperty = () => {
    toast.success("已複製租盤");
    textAreaRef.current.select();
    document.execCommand("copy");
  };

  const sharePropertyWhatsapp = () => {
    router.push(
      `whatsapp://send?text=${data.title}\n- 分享自 免佣租屋台\nhttps://okayfound.com/property/${data.property_id}`,
    );
  };

  return (
    <div className="pt-4 pb-4">
      <textarea
        readonly
        value={`${data.title}\n- 分享自 免佣租屋台\nhttps://okayfound.com/property/${data.property_id}`}
        className="rounded h-20"
        row={12}
        ref={textAreaRef}
        onFocus={handleFocus}
      />

      <div className="flex mt-5">
        <button
          type="button"
          onClick={copyProperty}
          className="text-white border w-12 h-12 bg-neutral-500 hover:bg-neutral-500 hover:text-white focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-full text-sm p-2.5 text-center inline-flex items-center justify-center mr-3"
        >
          <i className=" fa-solid fa-copy text-xl items-center"></i>
        </button>
        <button
          type="button"
          onClick={sharePropertyWhatsapp}
          className="text-white border w-12 h-12 bg-green-500 hover:bg-green-500 hover:text-white focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-full text-sm p-2.5 text-center inline-flex items-center justify-center"
        >
          <i className=" fa-brands fa-whatsapp text-3xl"></i>
        </button>
      </div>
    </div>
  );
}

export default ShareModal;
