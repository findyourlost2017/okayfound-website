import Heading from "./heading";
import Nav from "./nav";
import NavItem from "./nav-item";
import NextPrev from "./next-prev";
import Badge from "./badge";
import GallerySlider from "./gallery-slider";
import NcImage from "./nc-image";
import NavLink from "./nav-link";
import NcModal from "./nc-modal";

export {
  Heading,
  Nav,
  NavItem,
  NextPrev,
  Badge,
  GallerySlider,
  NcImage,
  NavLink,
  NcModal,
};
