import appConfig from "../../utils/appConfig";
// import Image from "next/image";

function NotificationMessage({ item }) {
  return (
    <div
      id="toast-notification"
      className="p-1 w-full max-w-xs text-gray-900 bg-white rounded-lg "
      role="alert"
    >
      <div className="flex items-center mb-3">
        <span className="mb-1 text-sm font-semibold text-gray-900 dark:text-white">
          新通知
        </span>
      </div>
      <div className="flex items-center">
        <div className="inline-block relative shrink-0">
          {/* <NcImage
            className="w-12 h-12 rounded"
            alt="Notification Image"
            src={item?.thumbnail}
          /> */}
          <img
            src={item?.thumbnail}
            height={58}
            width={58}
            className="rounded"
            objectFit="cover"
          />
        </div>
        <div className="ml-3 text-sm font-normal">
          <div className="text-sm font-semibold text-gray-900 dark:text-white">
            {item?.property_title}
          </div>
          <div className="text-sm font-normal">
            {item?.message_type === appConfig.MESSAGE_TYPE["image"]
              ? "圖片"
              : item?.text}
          </div>
          <span className="text-xs font-medium text-blue-600 dark:text-blue-500">
            幾秒鐘前
          </span>
        </div>
      </div>
    </div>
  );
}

export default NotificationMessage;
