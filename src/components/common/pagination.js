import _ from "lodash";
import ReactPaginate from "react-paginate";
import twFocusClass from "../../utils/twFocusClass";

const Pagination = ({ className = "", page, onChangePage, total }) => {
  const handlePageClick = (e) => {
    onChangePage(e.selected + 1);
  };

  // const renderItem = (pag, index) => {
  //   if (index === 0) {
  //     // RETURN ACTIVE PAGINATION
  //     return (
  //       <span
  //         key={index}
  //         className={`inline-flex w-11 h-11 items-center justify-center rounded-full bg-orange-500 text-white ${twFocusClass()}`}
  //       >
  //         {pag.label}
  //       </span>
  //     );
  //   }
  //   // RETURN UNACTIVE PAGINATION
  //   return (
  //     <a
  //       key={index}
  //       className={`inline-flex w-11 h-11 items-center justify-center rounded-full bg-white hover:bg-neutral-100 border border-neutral-200 text-neutral-6000 ${twFocusClass()}`}
  //       href={pag.href}
  //     >
  //       {pag.label}
  //     </a>
  //   );
  // };

  // return (
  //   <nav
  //     className={`nc-Pagination inline-flex space-x-1 text-base font-medium ${className}`}
  //   >
  //     {DEMO_PAGINATION.map(renderItem)}
  //   </nav>
  // );

  return (
    <nav
      className={`nc-Pagination inline-flex space-x-1 text-base font-medium ${className}`}
    >
      <ReactPaginate
        breakLabel="..."
        onPageChange={handlePageClick}
        pageRangeDisplayed={3}
        pageCount={total}
        forcePage={page - 1}
        pageLinkClassName={`inline-flex w-11 h-11 items-center justify-center rounded-full bg-white hover:bg-orange-600 border border-neutral-200 ${twFocusClass()}`}
        activeLinkClassName={`inline-flex w-11 h-11 items-center justify-center rounded-full bg-orange-500 text-white ${twFocusClass()}`}
        className={`nc-Pagination inline-flex space-x-1 text-base font-medium`}
        breakClassName="inline-flex items-center justify-center"
        breakLinkClassName="pb-3"
        nextLabel=""
        previousLabel=""
        renderOnZeroPageCount={null}
      />
    </nav>
  );
};

export default Pagination;
