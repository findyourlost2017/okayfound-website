function AlertModal({ closeModal, title, onConfirm }) {
  return (
    <div className="relative bg-white rounded-lg">
      <div className="pb-4 text-center">
        <i className="text-4xl mb-4 fa-duotone fa-circle-info" />

        <h3 className="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">
          {title}
        </h3>
        <button
          data-modal-toggle="popup-modal"
          type="button"
          onClick={() => {
            onConfirm && onConfirm();
            closeModal();
          }}
          className="text-white bg-red-600 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 dark:focus:ring-red-800 font-medium rounded-lg text-sm inline-flex items-center px-5 py-2.5 text-center mr-2"
        >
          我確定
        </button>
        <button
          data-modal-toggle="popup-modal"
          type="button"
          onClick={closeModal}
          className="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-gray-200 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600"
        >
          再考慮一下
        </button>
      </div>
    </div>
  );
}

export default AlertModal;
