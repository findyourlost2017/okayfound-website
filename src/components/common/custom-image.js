function CustomImage(props) {
  const list = ["source.okayfound.com", "source.car4goal.com"];

  function checkExist(val) {
    let exist = false;
    val &&
      list?.map((item) => {
        if (val.indexOf(item) > -1) {
          exist = true;
        }
      });

    return exist;
  }
  if (checkExist(props.src))
    return <img {...props} src={`/api/imageProxy?imageUrl=${props.src}`} />;
  else {
    return <img {...props} />;
  }
}

export default CustomImage;
