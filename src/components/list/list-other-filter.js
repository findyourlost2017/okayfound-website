import { Fragment, useState, useEffect } from "react";
import { Popover, Transition } from "@headlessui/react";
import ClearDataButton from "../button/clear-data-button";
import ButtonThird from "../button/button-third";
import ButtonPrimary from "../button/button-primary";
import appConfig from "../../utils/appConfig";
import Checkbox from "../input/checkbox";

function ListOtherFilter({
  room,
  area,
  estateID,
  estateName,
  estateOptions,
  setEstateID,
  setEstateName,
  onChangeRoom,
  onChangeArea,
  onChangeEstate,
  onSelectEstate,
}) {
  const [tempRoom, setTempRoom] = useState(room);
  const [tempArea, setTempArea] = useState(area);

  useEffect(() => {
    setTempRoom(room);
  }, [room]);

  const handleSelectLocation = (item) => {
    onSelectEstate(item);
  };

  const renderSearchValue = (close) => {
    return (
      <>
        {estateOptions &&
          estateOptions.map((item) => (
            <span
              onClick={() => {
                handleSelectLocation(item);
                close();
              }}
              key={item.estate_id}
              className="flex px-4 sm:px-8 items-center space-x-3 sm:space-x-4 py-4 sm:py-5 hover:bg-neutral-100 dark:hover:bg-neutral-700 cursor-pointer nc-hero-field-padding"
            >
              <span className="block text-neutral-400">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-4 w-4 sm:h-6 sm:w-6"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={1.5}
                    d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z"
                  />
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={1.5}
                    d="M15 11a3 3 0 11-6 0 3 3 0 016 0z"
                  />
                </svg>
              </span>
              <span className="block font-medium text-neutral-700 dark:text-neutral-200">
                {item.full_name}
              </span>
            </span>
          ))}
      </>
    );
  };

  const renderXClear = () => {
    return (
      <span className="w-4 h-4 rounded-full bg-orange-500 text-white flex items-center justify-center ml-3 cursor-pointer">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-3 w-3"
          viewBox="0 0 20 20"
          fill="currentColor"
        >
          <path
            fillRule="evenodd"
            d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
            clipRule="evenodd"
          />
        </svg>
      </span>
    );
  };

  const renderEstate = () => {
    return (
      <Popover className="relative">
        {({ open, close }) => (
          <>
            <Popover.Button
              className={`flex items-center justify-center px-4 py-2 text-sm rounded-full border border-neutral-300 focus:outline-none ${
                open ? "!border-orange-500 " : ""
              } ${
                estateID && estateName !== null && estateName !== undefined
                  ? "border-orange-500 bg-orange-50 text-orange-700"
                  : ""
              }`}
            >
              <span>
                {estateID && estateName ? `${estateName}` : "屋苑名稱"}
              </span>
              {estateName ? (
                renderXClear()
              ) : (
                <i className="las la-angle-down ml-2"></i>
              )}
            </Popover.Button>
            <Transition
              as={Fragment}
              enter="transition ease-out duration-200"
              enterFrom="opacity-0 translate-y-1"
              enterTo="opacity-100 translate-y-0"
              leave="transition ease-in duration-150"
              leaveFrom="opacity-100 translate-y-0"
              leaveTo="opacity-0 translate-y-1"
            >
              <Popover.Panel className="absolute z-10 w-screen max-w-sm px-4 mt-3 left-0 sm:px-0 lg:max-w-md">
                <div className="overflow-hidden rounded-2xl shadow-xl bg-white border border-neutral-200">
                  {/* input */}
                  <div className="flex flex-1 relative [ nc-hero-field-padding ] flex-shrink-0 items-center space-x-3 cursor-pointer focus:outline-none text-left">
                    <div className="text-neutral-300 dark:text-neutral-400">
                      <i className="fa-solid fa-pen-field"></i>
                    </div>
                    <div className="flex-grow">
                      <input
                        className={`block w-full bg-transparent border-none focus:ring-0 p-0 focus:outline-none focus:placeholder-neutral-300 xl:text-lg font-semibold placeholder-neutral-800 dark:placeholder-neutral-200 truncate`}
                        placeholder={""}
                        value={estateName}
                        autoFocus={true}
                        onChange={(e) => {
                          setEstateID();
                          setEstateName(e.currentTarget.value);
                          onChangeEstate &&
                            onChangeEstate(e.currentTarget.value);
                        }}
                        // ref={inputRef}
                      />
                      <span className="block mt-0.5 text-sm text-neutral-400 font-light ">
                        <span className="line-clamp-1">請輸入屋苑名稱</span>
                      </span>
                      <ClearDataButton
                        onClick={() => {
                          setEstateID();
                          setEstateName("");
                        }}
                      />
                    </div>
                  </div>
                  {/* List */}
                  {renderSearchValue(close)}
                </div>
              </Popover.Panel>
            </Transition>
          </>
        )}
      </Popover>
    );
  };

  const renderRoom = () => {
    return (
      <Popover className="relative">
        {({ open, close }) => (
          <>
            <Popover.Button
              className={`flex items-center justify-center px-4 py-2 text-sm rounded-full border border-neutral-300 focus:outline-none ${
                open ? "!border-orange-500 " : ""
              } ${
                room !== null && room !== undefined
                  ? "border-orange-500 bg-orange-50 text-orange-700"
                  : ""
              }`}
            >
              <span>
                {room
                  ? `房間: ${
                      _.find(appConfig.roomItems, { value: room })?.label
                    }`
                  : "房間數目"}
              </span>
              {room ? (
                renderXClear()
              ) : (
                <i className="las la-angle-down ml-2"></i>
              )}
            </Popover.Button>
            <Transition
              as={Fragment}
              enter="transition ease-out duration-200"
              enterFrom="opacity-0 translate-y-1"
              enterTo="opacity-100 translate-y-0"
              leave="transition ease-in duration-150"
              leaveFrom="opacity-100 translate-y-0"
              leaveTo="opacity-0 translate-y-1"
            >
              <Popover.Panel className="absolute z-10 w-screen max-w-sm px-4 mt-3 left-0 sm:px-0 lg:max-w-md">
                <div className="overflow-hidden rounded-2xl shadow-xl bg-white border border-neutral-200">
                  <div className="relative flex flex-col px-5 py-6 space-y-5">
                    {appConfig.roomItems.map((item) => (
                      <div key={item.value} className="">
                        <Checkbox
                          name={item.label}
                          label={item.label}
                          checked={tempRoom === item.value}
                          onChange={(selected) => {
                            if (selected) setTempRoom(item.value);
                            else setTempRoom();
                          }}
                        />
                      </div>
                    ))}
                  </div>
                  <div className="p-5 bg-neutral-50 dark:bg-neutral-900 dark:border-t dark:border-neutral-800 flex items-center justify-between">
                    <ButtonThird
                      onClick={() => {
                        close();
                        onChangeRoom();
                      }}
                      sizeClass="px-4 py-2 sm:px-5"
                    >
                      清除
                    </ButtonThird>
                    <ButtonPrimary
                      onClick={() => {
                        onChangeRoom(tempRoom);
                        close();
                      }}
                      className="hover:bg-orange-400"
                      sizeClass="px-4 py-2 sm:px-5"
                    >
                      提交
                    </ButtonPrimary>
                  </div>
                </div>
              </Popover.Panel>
            </Transition>
          </>
        )}
      </Popover>
    );
  };

  const renderArea = () => {
    return (
      <Popover className="relative">
        {({ open, close }) => (
          <>
            <Popover.Button
              className={`flex items-center justify-center px-4 py-2 text-sm rounded-full border border-neutral-300 focus:outline-none ${
                open ? "!border-orange-500 " : ""
              } ${
                area !== null && area !== undefined
                  ? "border-orange-500 bg-orange-50 text-orange-700"
                  : ""
              }`}
            >
              <span>
                {area
                  ? `${_.find(appConfig.areaItems, { value: area })?.label}`
                  : "單位面積"}
              </span>
              {area ? (
                renderXClear()
              ) : (
                <i className="las la-angle-down ml-2"></i>
              )}
            </Popover.Button>
            <Transition
              as={Fragment}
              enter="transition ease-out duration-200"
              enterFrom="opacity-0 translate-y-1"
              enterTo="opacity-100 translate-y-0"
              leave="transition ease-in duration-150"
              leaveFrom="opacity-100 translate-y-0"
              leaveTo="opacity-0 translate-y-1"
            >
              <Popover.Panel className="absolute z-10 w-screen max-w-sm px-4 mt-3 left-0 sm:px-0 lg:max-w-md">
                <div className="overflow-hidden rounded-2xl shadow-xl bg-white border border-neutral-200">
                  <div className="relative flex flex-col px-5 py-6 space-y-5">
                    {appConfig.areaItems.map((item) => (
                      <div key={item.value} className="">
                        <Checkbox
                          name={item.label}
                          label={item.label}
                          checked={tempArea === item.value}
                          onChange={(selected) => {
                            if (selected) setTempArea(item.value);
                            else setTempArea();
                          }}
                        />
                      </div>
                    ))}
                  </div>
                  <div className="p-5 bg-neutral-50 dark:bg-neutral-900 dark:border-t dark:border-neutral-800 flex items-center justify-between">
                    <ButtonThird
                      onClick={() => {
                        close();
                        onChangeArea();
                      }}
                      sizeClass="px-4 py-2 sm:px-5"
                    >
                      清除
                    </ButtonThird>
                    <ButtonPrimary
                      onClick={() => {
                        onChangeArea(tempArea);
                        close();
                      }}
                      className="hover:bg-orange-400"
                      sizeClass="px-4 py-2 sm:px-5"
                    >
                      提交
                    </ButtonPrimary>
                  </div>
                </div>
              </Popover.Panel>
            </Transition>
          </>
        )}
      </Popover>
    );
  };

  return (
    <div className="mb-8 lg:mb-11">
      <div className="flex lg:space-x-4">
        <div className="hidden lg:flex space-x-4">
          {renderEstate()}
          {renderRoom()}
          {renderArea()}
        </div>
      </div>
    </div>
  );
}

export default ListOtherFilter;
