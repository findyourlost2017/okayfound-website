import _ from "lodash";
import PropertyCard from "../property/property-card";
import LoadingCard from "../common/loading-card";

function ListContent({ data, loading }) {
  const renderEmpty = () => {
    return (
      <div className="grid grid-cols-1">
        <div className="flex flex-col min-h-screen md:px-10 py-10 px-4 mt-10">
          <div className="title-heading text-center">
            {/* <img src="assets/images/error.png" className="mx-auto" alt="" /> */}
            <h1 className="mt-3 mb-6 md:text-5xl text-3xl font-bold">
              找不到租盤
            </h1>
            <p className="text-slate-400">
              嘗試更改其他搜尋條件，或幫助我們推廣 租屋台 免佣平台，讓更多人認識
            </p>
          </div>
        </div>
      </div>
    );
  };

  if (!loading && (!data || (data && data.length === 0))) {
    return renderEmpty();
  }

  return (
    <div className="grid grid-cols-1 gap-6 md:gap-8 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
      {!loading &&
        data &&
        data.map((item) => (
          <PropertyCard key={`${item.property_id}_list`} data={item} />
        ))}

      {loading &&
        _.range(8).map((item) => <LoadingCard key={`${item}_loading_list`} />)}
    </div>
  );
}

export default ListContent;
