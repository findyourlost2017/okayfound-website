import { PropertySearchForm } from "../property";

function ListHeader({
  price,
  setPrice,
  propertyType,
  setPropertyType,
  subPropertyType,
  setSubPropertyType,
  floor,
  setFloor,
  room,
  setRoom,
  area,
  setArea,
  propertyDistrict,
  setPropertyDistrict,
  searchProperty,
}) {
  return (
    <div className="mb-12 lg:mb-16">
      <h2 className="text-4xl font-semibold">最新免佣租盤</h2>
      <PropertySearchForm
        price={price}
        setPrice={setPrice}
        propertyType={propertyType}
        setPropertyType={setPropertyType}
        subPropertyType={subPropertyType}
        setSubPropertyType={setSubPropertyType}
        propertyDistrict={propertyDistrict}
        setPropertyDistrict={setPropertyDistrict}
        searchProperty={searchProperty}
      />
    </div>
  );
}

export default ListHeader;
