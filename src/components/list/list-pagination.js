import Pagination from "../common/pagination";

function ListPagination({ page, onChangePage, total }) {
  return (
    <div className="flex mt-16 justify-center items-center">
      <Pagination page={page} onChangePage={onChangePage} total={total} />
    </div>
  );
}

export default ListPagination;
