import { Fragment, useEffect, useState } from "react";
import SocketIOClient from "socket.io-client";
import { toast } from "react-toastify";
import { useRouter } from "next/router";
import appConfig from "../../utils/appConfig";
import Header from "./header";
import Footer from "./footer";
import NotificationMessage from "../common/notification-message";
import API from "../../utils/api";

const Layout = ({ children, session, hideFooter, onReceiveMessage }) => {
  const [badgeCount, setBadgeCount] = useState(0);
  const router = useRouter();

  const fetchChatBadge = async () => {
    const api = new API(session?.token);
    const response = await api.fetchChatBadge();
    if (response && response.ok) {
      setBadgeCount(response.data.count);
    }
  };

  //   Connect socket
  useEffect(() => {
    if (!session || (session && !session.member)) return null;
    fetchChatBadge();

    const socket = SocketIOClient(appConfig.API_URL, {
      transports: ["websocket"],
    });

    socket.on("connect", () => {
      console.log("SOCKET CONNECTED!", socket.id);

      socket.emit("online", {
        member_id: session.member.member_id,
      });

      socket.on("new_chat", (data) => {
        onReceiveMessage && onReceiveMessage(data);
        if (!onReceiveMessage) {
          toast(<NotificationMessage item={data} />, {
            toastId: data._id,
            closeButton: true,
            hideProgressBar: true,
            onClick: () => router.push("/messages"),
          });
        }
        setBadgeCount((state) => state + 1);
      });
    });

    // socket disconnet onUnmount if exists
    if (socket) return () => socket.disconnect();
  }, []);

  return (
    <Fragment>
      <Header member={session && session.member} badgeCount={badgeCount} />
      {children}
      {!hideFooter && (
        <Footer
          member={session && session.member}
          token={session && session.token}
        />
      )}
    </Fragment>
  );
};

export default Layout;
