import Link from "next/link";
import Image from "next/image";

function Footer({ token }) {
  return (
    <div className="nc-Footer relative py-24 lg:py-28 border-t border-neutral-200 dark:border-neutral-700">
      <div className="container grid grid-cols-2 gap-y-10 gap-x-5 sm:gap-x-8 md:grid-cols-4 lg:grid-cols-5 lg:gap-x-10 ">
        {/* Logo */}
        <div className="grid grid-cols-4 gap-5 col-span-2 md:col-span-4 lg:md:col-span-1 lg:flex lg:flex-col">
          <div className="col-span-2 md:col-span-1">
            <Image src="/images/icon.png" width={54} height={54} />
          </div>
        </div>
        {/* Content */}
        <div className="text-sm">
          <h2 className="font-semibold text-neutral-700 dark:text-neutral-200">
            租盤
          </h2>
          <ul className="mt-5 space-y-4">
            <li>
              <Link href="/list?property_type=0">
                <a className="text-neutral-6000 dark:text-neutral-300 hover:text-black dark:hover:text-white">
                  租樓
                </a>
              </Link>
            </li>
            <li>
              <Link href="/list?property_type=2">
                <a className="text-neutral-6000 dark:text-neutral-300 hover:text-black dark:hover:text-white">
                  租車位
                </a>
              </Link>
            </li>
          </ul>
        </div>
        <div className="text-sm">
          <h2 className="font-semibold text-neutral-700 dark:text-neutral-200">
            會員中心
          </h2>
          <ul className="mt-5 space-y-4">
            {!token && (
              <li>
                <Link href="/login">
                  <a className="text-neutral-6000 dark:text-neutral-300 hover:text-black dark:hover:text-white">
                    登入帳號
                  </a>
                </Link>
              </li>
            )}
            {token && (
              <li>
                <Link href="/account">
                  <a className="text-neutral-6000 dark:text-neutral-300 hover:text-black dark:hover:text-white">
                    帳號資料
                  </a>
                </Link>
              </li>
            )}
            {token && (
              <li>
                <Link href="/account">
                  <a className="text-neutral-6000 dark:text-neutral-300 hover:text-black dark:hover:text-white">
                    我的租盤
                  </a>
                </Link>
              </li>
            )}
          </ul>
        </div>
        <div className="text-sm">
          <h2 className="font-semibold text-neutral-700 dark:text-neutral-200">
            其他
          </h2>
          <ul className="mt-5 space-y-4">
            <li>
              <Link href="/agreement">
                <a className="text-neutral-6000 dark:text-neutral-300 hover:text-black dark:hover:text-white">
                  租約範本
                </a>
              </Link>
            </li>
            <li>
              <Link href="/list?property_type=0">
                <a className="text-neutral-6000 dark:text-neutral-300 hover:text-black dark:hover:text-white">
                  查詢兇宅
                </a>
              </Link>
            </li>
            <li>
              <Link href="/about">
                <a className="text-neutral-6000 dark:text-neutral-300 hover:text-black dark:hover:text-white">
                  關於我們
                </a>
              </Link>
            </li>
            <li>
              <Link href="/privacy">
                <a className="text-neutral-6000 dark:text-neutral-300 hover:text-black dark:hover:text-white">
                  私隱條款
                </a>
              </Link>
            </li>
          </ul>
        </div>
        {/* End Content */}
      </div>
    </div>
  );
}

export default Footer;
