import React from "react";
import Link from "next/link";
import Image from "next/image";
import { Disclosure } from "@headlessui/react";
import { ChevronDownIcon } from "@heroicons/react/24/solid";
import { NAVIGATION } from "../../utils/getNavigationList";
import ButtonClose from "../button/button-close";
import ButtonPrimary from "../button/button-primary";

const NavMobile = ({ data = NAVIGATION, onClickClose }) => {
  const _renderMenuChild = (item) => {
    return (
      <ul className="nav-mobile-sub-menu pl-6 pb-1 text-base">
        {item.children?.map((i, index) => (
          <Disclosure key={i.href + index} as="li">
            <Link href={i.href || undefined}>
              <a className="flex px-4 text-neutral-900 dark:text-neutral-200 text-sm font-medium rounded-lg hover:bg-neutral-100 dark:hover:bg-neutral-800 mt-0.5">
                <span
                  className={`py-2.5 pr-3 ${!i.children ? "block w-full" : ""}`}
                >
                  {i.name}
                </span>
                {i.children && (
                  <span
                    className="flex-1 flex"
                    onClick={(e) => e.preventDefault()}
                  >
                    <Disclosure.Button
                      as="span"
                      className="py-2.5 flex justify-end flex-1"
                    >
                      <ChevronDownIcon
                        className="ml-2 h-4 w-4 text-neutral-500"
                        aria-hidden="true"
                      />
                    </Disclosure.Button>
                  </span>
                )}
              </a>
            </Link>
            {i.children && (
              <Disclosure.Panel>{_renderMenuChild(i)}</Disclosure.Panel>
            )}
          </Disclosure>
        ))}
      </ul>
    );
  };

  const _renderItem = (item, index) => {
    return (
      <Disclosure
        key={item.id}
        as="li"
        className="text-neutral-900 dark:text-white"
      >
        <Link href={item.href || undefined} activeClassName="text-secondary">
          <a className="flex w-full px-4 font-medium uppercase tracking-wide text-sm hover:bg-neutral-100 dark:hover:bg-neutral-800 rounded-lg">
            <span
              className={`py-2.5 pr-3 ${!item.children ? "block w-full" : ""}`}
            >
              {item.name}
            </span>
            {item.children && (
              <span className="flex-1 flex" onClick={(e) => e.preventDefault()}>
                <Disclosure.Button
                  as="span"
                  className="py-2.5 flex items-center justify-end flex-1 "
                >
                  <ChevronDownIcon
                    className="ml-2 h-4 w-4 text-neutral-500"
                    aria-hidden="true"
                  />
                </Disclosure.Button>
              </span>
            )}
          </a>
        </Link>
        {item.children && (
          <Disclosure.Panel>{_renderMenuChild(item)}</Disclosure.Panel>
        )}
      </Disclosure>
    );
  };

  return (
    <div className="overflow-y-auto w-full h-screen py-2 transition transform shadow-lg ring-1 dark:ring-neutral-700 bg-white dark:bg-neutral-900 divide-y-2 divide-neutral-100 dark:divide-neutral-800">
      <div className="py-6 px-5">
        <Link href="/">
          <a className="ttnc-logo inline-block text-primary-6000 focus:outline-none focus:ring-0 w-16">
            <Image src="/images/icon.png" width={54} height={54} />
          </a>
        </Link>
        <div className="flex flex-col mt-5 text-neutral-700 dark:text-neutral-300 text-sm">
          <span>
            幫你慳返半個月租代理佣金！租屋台俾你係毋須地產代理嘅協助下，雙方直接聯絡及交易！你可以通過線上睇樓，揀岩心水樓盤直接DM業主約睇樓時間
          </span>
        </div>
        <span className="absolute right-2 top-2 p-1">
          <ButtonClose onClick={onClickClose} />
        </span>
      </div>
      <ul className="flex flex-col py-6 px-2 space-y-1">
        {data.map(_renderItem)}
      </ul>
      <div className="items-end p-4">
        <ButtonPrimary
          className="bg-orange-500 hover:bg-yellow-700"
          href="/login"
        >
          登入
        </ButtonPrimary>
      </div>
    </div>
  );
};

export default NavMobile;
