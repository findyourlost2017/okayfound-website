import Link from "next/link";
import Image from "next/image";
import { NAVIGATION } from "../../utils/getNavigationList";
import NavigationItem from "./navigation-item";
import ButtonPrimary from "../button/button-primary";
import MenuBar from "./menu-bar";

const SiteHeader = ({ member, badgeCount }) => {
  return (
    <>
      <div className="nc-Header sticky top-0 w-full left-0 right-0 z-40 nc-header-bg shadow-sm dark:border-b dark:border-neutral-700">
        <div className="nc-MainNav1 relative z-10">
          <div className="px-4 lg:container py-4 lg:py-5 relative flex justify-between items-center">
            <div className="flex justify-start flex-1 items-center space-x-4 sm:space-x-10">
              <Link href="/">
                <a className="ttnc-logo inline-block text-primary-6000 focus:outline-none focus:ring-0 w-16">
                  <Image src="/images/icon.png" width={54} height={54} />
                </a>
              </Link>

              <ul className="nc-Navigation hidden lg:flex lg:flex-wrap lg:items-center lg:space-x-1 relative">
                {NAVIGATION.map((item) => (
                  <NavigationItem key={item.id} menuItem={item} />
                ))}
              </ul>
            </div>

            <div className="hidden md:flex flex-shrink-0 items-center justify-end flex-1 lg:flex-none text-neutral-700">
              {member && (
                <Link href="/add-property">
                  <a
                    className="
                mr-3 text-opacity-90
                group px-4 py-2 border border-neutral-300 hover:border-neutral-400 rounded-full inline-flex items-center text-sm text-gray-700 font-medium hover:text-opacity-100 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75"
                  >
                    <i className="fa-solid fa-plus pr-1" /> 刊登租盤
                  </a>
                </Link>
              )}

              {member && (
                <div className="pl-2 pr-6">
                  <Link href="/messages">
                    <a className="relative rounded-full text-neutral-700  hover:bg-neutral-100 focus:outline-none flex items-center justify-center">
                      <i className="text-xl fa-light fa-comment"></i>
                      {badgeCount > 0 && (
                        <span className="w-2 h-2 bg-orange-500 absolute top-0 right-0 rounded-full" />
                      )}
                    </a>
                  </Link>
                </div>
              )}

              {member && (
                <div className="pr-4">
                  <Link href="/calendar">
                    <a className="relative rounded-full text-neutral-700  hover:bg-neutral-100 focus:outline-none flex items-center justify-center">
                      <i className="text-xl  fa-light fa-calendar-days"></i>
                    </a>
                  </Link>
                </div>
              )}

              {!member && (
                <div className="items-center space-x-0.5">
                  <ButtonPrimary
                    className="bg-orange-500 hover:bg-yellow-700"
                    href="/login"
                  >
                    登入
                  </ButtonPrimary>
                </div>
              )}

              {member && (
                <Link href="/account">
                  <a className="inline-flex overflow-hidden relative justify-center items-center w-10 h-10 bg-gray-100 rounded-full dark:bg-gray-600">
                    <span className="font-medium text-gray-600 dark:text-gray-300">
                      {member.member_name && member.member_name.substring(0, 1)}
                    </span>
                  </a>
                </Link>
              )}
            </div>

            <div className="flex xl:hidden md:hidden items-center">
              <MenuBar />
            </div>

            {/* <div className="hidden md:flex flex-shrink-0 items-center justify-end flex-1 lg:flex-none text-neutral-700 dark:text-neutral-100">
          <div className="hidden xl:flex items-center space-x-0.5">
            <SwitchDarkMode />
            <SearchDropdown />
            <div className="px-1" />
            <ButtonPrimary href="/login">Sign up</ButtonPrimary>
          </div>
          <div className="flex xl:hidden items-center">
            <SwitchDarkMode />
            <div className="px-0.5" />
            <MenuBar />
          </div>
        </div> */}
          </div>
        </div>
      </div>
      {/* <div ref={anchorRef} className="h-1 absolute invisible"></div> */}
    </>
  );
};

export default SiteHeader;
