import apisauce from "apisauce";

export default class PDFApi {
  constructor() {
    this.api = apisauce.create({
      baseURL: "https://pdf.okayfound.com",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      timeout: 25000,
    });

    this.api.axiosInstance.interceptors.response.use(
      function (response) {
        return response;
      },
      function (error) {
        return Promise.reject(error);
      },
    );
  }

  async generateContractTemplate(data) {
    const url = "/generatePDF";
    const res = await this.api.post(url, data);
    return res;
  }
}
