export default {
  API_URL: "https://api.okayfound.com",
  PROPERTY_TYPE: {
    住宅: 0,
    工商: 1,
    車位: 2,
  },
  PROPERTY_TYPE_MAP: {
    0: "住宅",
    1: "工商",
    2: "車位",
  },
  PROPERTY_ICON: {
    0: "fa-regular fa-house",
    1: "",
    2: "fa-regular fa-square-parking",
  },
  PROPERTY_STATUS: {
    PENDING: 0,
    APPROVAL: 1,
    STOPPED: 2,
    DELETED: 3,
  },
  propertyTypeMapping: {
    "": "私樓",
    101: "居屋",
    "-1": "私樓",
    104: "公屋",
    120: "工商",
    121: "寫字樓",
  },
  propertyType: [
    {
      label: "住宅",
      value: 0,
      subLabel: "居屋,公屋及私樓",
      groups: [
        { label: "居屋", value: 101 },
        { label: "公屋", value: 104 },
        { label: "私樓", value: -1 },
      ],
    },
    // {
    //   label: "工商",
    //   value: "工商",
    //   subLabel: "工商大廈及寫字樓",
    //   groups: [
    //     { label: "工商大廈", value: 120 },
    //     { label: "寫字樓", value: 121 },
    //   ],
    // },
    { label: "車位", value: 2 },
  ],
  SUB_PROPERTY_MAPPING: {
    101: "居屋",
    104: "公屋",
    "-1": "私樓",
  },
  districtTypeMapping: {
    "-1": "香港島",
    "-2": "九龍",
    "-3": "新界",
    "-4": "離島",
  },
  districtType: [
    {
      label: "香港島",
      value: "香港島",
      mapKey: -1,
      subLabel: "中西區、灣仔區、東區、南區等",
    },
    {
      label: "九龍",
      value: "九龍",
      mapKey: -2,
      subLabel: "葵青、深水埗、油尖旺、九龍城、黃大仙、觀塘區等",
    },
    {
      label: "新界",
      value: "新界",
      mapKey: -3,
      subLabel: "北區、大埔、沙田、西貢、元朗、屯門等",
    },
    {
      label: "離島",
      value: "離島",
      mapKey: -4,
      subLabel: "大嶼山、長洲、南丫島等",
    },
  ],
  roomItems: [
    { label: "開放式", value: 0 },
    { label: "一房", value: 1 },
    { label: "兩房", value: 2 },
    { label: "三房", value: 3 },
    { label: "四房", value: 4 },
    { label: "五房", value: 5 },
    // { label: "6房", value: 6 },
  ],
  bathRoomItems: [
    { label: "一個", value: 1 },
    { label: "兩個", value: 2 },
    { label: "三個", value: 3 },
  ],
  areaItems: [
    { label: "280呎以下", value: 0, min: 0, max: 280 },
    { label: "280 - 400 呎", value: 1, min: 280, max: 400 },
    { label: "400 - 700 呎", value: 2, min: 400, max: 700 },
    { label: "700 - 1000 呎", value: 3, min: 700, max: 1000 },
    { label: "1000 - 2000 呎", value: 4, min: 1000, max: 2000 },
    { label: "2000 呎以上", value: 5, min: 2000, max: 100000 },
  ],
  districtID: {
    香港島: [
      -1, 42, 41, 134, 18, 19, 16, 20, 15, 22, 21, 23, 24, 26, 40, 28, 27, 30,
      39, 29, 31, 32, 38, 34, 35, 36, 133, 12, 45, 44, 14,
    ],
    九龍: [
      -2, 52, 131, 54, 81, 56, 122, 79, 129, 77, 58, 82, 55, 57, 59, 60, 78, 62,
      61, 64, 63, 65, 80, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76,
    ],
    新界: [
      -3, 100, 136, 125, 23608, 101, 102, 124, 126, 103, 130, 104, 105, 107,
      108, 110, 111, 112, 113, 122, 114, 132, 123,
    ],
    離島: [-4, 115, 117, 116, 118, 135, 119, 128, 120, 121],
  },
  districtOption: {
    香港島: [
      { name: "不限", id: [-1] },
      { name: "堅尼地城", id: [42] },
      { name: "西營盤,石塘咀", id: [41, 134] },
      { name: "中上環", id: [18, 19] },
      { name: "中西半山", id: [16, 20, 15] },
      { name: "灣仔,金鐘", id: [22, 21] },
      { name: "銅鑼灣,跑馬地", id: [23, 24, 26] },
      { name: "天后,大坑", id: [40, 28, 27] },
      { name: "北角,炮台山", id: [30, 39] },
      { name: "北角半山", id: [29] },
      { name: "鰂魚涌", id: [31] },
      { name: "太古城", id: [32] },
      { name: "西灣河", id: [38] },
      { name: "筲箕灣,杏花村", id: [34, 35] },
      { name: "柴灣,小西灣,石澳", id: [36, 133] }, // 無小西灣
      { name: "香港仔,鴨脷洲,黃竹坑", id: [12, 45, 44] },
      { name: "南區", id: [14] },
    ],
    九龍: [
      { name: "不限", id: [-2] },
      { name: "藍田", id: [52] },
      { name: "油塘", id: [131] },
      { name: "觀塘,牛頭角", id: [54, 81] },
      { name: "九龍灣", id: [56] },
      { name: "牛池灣", id: [127] },
      { name: "鑽石山,樂富", id: [79, 129] },
      { name: "土瓜灣", id: [77] },
      { name: "九龍城", id: [58] },
      { name: "啟德", id: [82] },
      { name: "新蒲崗,黃大仙", id: [55, 57] },
      { name: "九龍塘", id: [59] },
      { name: "何文田", id: [60] },
      { name: "又一村", id: [78] },
      { name: "深水埗,石硤尾", id: [62, 61] },
      { name: "荔枝角", id: [64] },
      { name: "長沙灣", id: [63] },
      { name: "美孚", id: [65] },
      { name: "荔景", id: [80] },
      { name: "大角咀,奧運,九龍站", id: [67, 68, 69] },
      { name: "太子", id: [70] },
      { name: "旺角", id: [71] },
      { name: "油麻地", id: [72] },
      { name: "佐敦", id: [73] },
      { name: "尖沙咀", id: [74] },
      { name: "紅磡", id: [75] },
      { name: "黃埔", id: [76] },
    ],
    新界: [
      { name: "不限", id: [-3] },
      { name: "西貢,清水灣", id: [100, 136] },
      { name: "將軍澳", id: [125] },
      { name: "日出康城", id: [23608] },
      { name: "馬鞍山", id: [101] },
      { name: "沙田,大圍,火炭", id: [102, 124, 126] },
      { name: "大埔,太和", id: [103, 130] },
      { name: "粉嶺", id: [104] },
      { name: "上水", id: [105] },
      { name: "元朗", id: [107] },
      { name: "天水圍", id: [108] },
      { name: "屯門", id: [110] },
      { name: "深井", id: [111, 112] },
      { name: "荃灣,大窩口", id: [113, 122] },
      { name: "葵涌,葵芳", id: [114, 132] },
      { name: "青衣", id: [123] },
    ],

    離島: [
      { name: "不限", id: [-4] },
      { name: "馬灣", id: [115] },
      { name: "愉景灣", id: [117] },
      { name: "東涌", id: [116] },
      { name: "南大嶼山,大澳", id: [118, 135] },
      { name: "坪洲", id: [119] },
      { name: "南丫島", id: [128] },
      { name: "長洲,其他離島", id: [120, 121] },
    ],
  },
  schoolNet: [
    { label: "11 校網", value: 11 },
    { label: "12 校網", value: 12 },
    { label: "14 校網", value: 14 },
    { label: "16 校網", value: 16 },
    { label: "18 校網", value: 18 },
    { label: "31 校網", value: 31 },
    { label: "32 校網", value: 32 },
    { label: "34 校網", value: 34 },
    { label: "35 校網", value: 35 },
    { label: "40 校網", value: 40 },
    { label: "41 校網", value: 41 },
    { label: "43 校網", value: 43 },
    { label: "45 校網", value: 45 },
    { label: "46 校網", value: 46 },
    { label: "48 校網", value: 48 },
    { label: "62 校網", value: 62 },
    { label: "64 校網", value: 64 },
    { label: "65 校網", value: 65 },
    { label: "66 校網", value: 66 },
    { label: "70 校網", value: 70 },
    { label: "71 校網", value: 71 },
    { label: "72 校網", value: 72 },
    { label: "73 校網", value: 73 },
    { label: "74 校網", value: 74 },
    { label: "80 校網", value: 80 },
    { label: "81 校網", value: 81 },
    { label: "83 校網", value: 83 },
    { label: "84 校網", value: 84 },
    { label: "88 校網", value: 88 },
    { label: "89 校網", value: 89 },
    { label: "91 校網", value: 91 },
    { label: "95 校網", value: 95 },
    { label: "96 校網", value: 96 },
    { label: "97 校網", value: 97 },
    { label: "98 校網", value: 98 },
    { label: "99 校網", value: 99 },
  ],
  MESSAGE_TYPE: {
    text: 0,
    image: 1,
    voice: 2,
    sticker: 3,
    system_cust: 4,
  },
  formatMoney: (amount, decimalCount = 0, decimal = ".", thousands = ",") => {
    try {
      decimalCount = Math.abs(decimalCount);
      decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

      const negativeSign = amount < 0 ? "-" : "";

      let i = parseInt(
        (amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)),
      ).toString();
      let j = i.length > 3 ? i.length % 3 : 0;

      return (
        negativeSign +
        (j ? i.substr(0, j) + thousands : "") +
        i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) +
        (decimalCount
          ? decimal +
            Math.abs(amount - i)
              .toFixed(decimalCount)
              .slice(2)
          : "")
      );
    } catch (e) {
      console.log(e);
    }
  },
};
