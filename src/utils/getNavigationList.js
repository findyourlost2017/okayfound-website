function ncNanoId(prefix = "nc_") {
  return prefix + Date.now() + "_" + Math.floor(Math.random() * 9999999999);
}

export const NAVIGATION = [
  {
    id: ncNanoId(),
    href: "/",
    name: "主頁",
  },
  {
    id: ncNanoId(),
    href: "/list?property_type=0",
    name: "租樓",
  },
  {
    id: ncNanoId(),
    href: "/list?property_type=2",
    name: "租車位",
  },
  {
    id: ncNanoId(),
    href: "/agreement",
    name: "租約範本",
  },
  {
    id: ncNanoId(),
    href: "/about",
    name: "關於我們",
  },
];
