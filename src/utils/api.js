import apisauce from "apisauce";

export default class API {
  constructor(token) {
    this.api = apisauce.create({
      baseURL: "https://api.okayfound.com",
      // baseURL: "http://localhost:3001",
      headers: {
        Authorization: token ? `Bearer ${token}` : "",
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      timeout: 25000,
    });

    this.api.axiosInstance.interceptors.response.use(
      function (response) {
        return response;
      },
      function (error) {
        return Promise.reject(error);
      },
    );
  }

  async loginAccount(data) {
    const url = "/member/loginAccount";
    const res = await this.api.post(url, data);
    return res;
  }

  async fetchListByType(data) {
    const url = `/property/fetchListByType`;
    const res = await this.api.post(url, data);
    return res;
  }

  async fetchPropertyDetail(id) {
    const url = `/property/fetchPropertyDetail/${id}`;
    const res = await this.api.post(url, {});
    return res;
  }

  async fetchPropertyList(data, page) {
    const url = `/property/fetchPropertyList/${page}`;
    const res = await this.api.post(url, data);
    return res;
  }

  async fetchRestaurant(data) {
    const url = `/property/fetchRestaurant`;
    const res = await this.api.post(url, data);
    return res;
  }

  async fetchPetro(data) {
    const url = `/property/fetchPetro`;
    const res = await this.api.post(url, data);
    return res;
  }

  async updateProfile(data) {
    const url = "/member/updateProfile";
    const res = await this.api.post(url, data);
    return res;
  }

  async submitPropertyPhoto(formData) {
    this.api.setHeader("Content-Type", "multipart/form-data");
    const res = await this.api.post("/property/postRentPhoto", formData);
    return res;
  }

  async fetchEstate(data) {
    const url = `/property/fetchEstate`;
    const res = await this.api.post(url, data);
    return res;
  }

  async postRent(data) {
    const url = "/property/postRent";
    const res = await this.api.post(url, data);
    return res;
  }

  async fetchPropertyHistory() {
    const url = "/property/fetchWebsiteHistory";
    const res = await this.api.post(url, {});
    return res;
  }

  async fetchFriendList(data) {
    const url = `/chat/fetchFriendList`;
    const res = await this.api.post(url, data);
    return res;
  }

  async fetchChatMessage(data) {
    const url = `/chat/fetchChatMessage`;
    const res = await this.api.post(url, data);
    return res;
  }

  async sendChatMessage(data) {
    const url = `/chat/sendChatMessage`;
    const res = await this.api.post(url, data);
    return res;
  }

  async sendChatFile(data) {
    const url = "/chat/sendChatFile";
    this.api.setHeader("Content-Type", "multipart/form-data");
    const res = await this.api.post(url, data);
    return res;
  }

  async resetChatBadge(data) {
    const url = `/chat/resetChatBadge`;
    const res = await this.api.post(url, data);
    return res;
  }

  async fetchChatBadge() {
    const url = `/chat/fetchChatBadge`;
    const res = await this.api.post(url, {});
    return res;
  }

  async fetchTimetable(data) {
    const url = `/member/fetchTimetable`;
    const res = await this.api.post(url, data);
    return res;
  }

  async deleteTimetable(data) {
    const url = `/member/deleteTimetable`;
    const res = await this.api.post(url, data);
    return res;
  }

  async submitTimetable(data) {
    const url = `/member/submitTimetable`;
    const res = await this.api.post(url, data);
    return res;
  }

  async deleteRent(propertyID) {
    const url = `/property/deleteRent/${propertyID}`;
    const res = await this.api.post(url, {});
    return res;
  }

  async stopRent(propertyID) {
    const url = `/property/stopRent/${propertyID}`;
    const res = await this.api.post(url, {});
    return res;
  }

  async repostRent(propertyID) {
    const url = `/property/repostRent/${propertyID}`;
    const res = await this.api.post(url, {});
    return res;
  }

  async postAdminRent(data) {
    const url = `/admin/submitProperty`;
    const res = await this.api.post(url, data);
    return res;
  }
}
