/** @type {import('next').NextConfig} */

const path = require("path");

const nextConfig = {
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
  },
  images: {
    domains: [
      "source.okayfound.com.s3.ap-northeast-1.wasabisys.com",
      "s3.ap-northeast-1.wasabisys.com",
      "source.okayfound.com",
      "dummyimage.com",
      "static.stheadline.com",
      "static6.orstatic.com",
      "static7.orstatic.com",
      "static8.orstatic.com",
      "static5.orstatic.com",
    ],
  },
  serverRuntimeConfig: {
    // NEXTAUTH_URL: "https://okayfound.com",
    NEXTAUTH_URL: "http://localhost:3000",
  },
  env: {
    FACEBOOK_ID: 5711089418954202,
    FACEBOOK_SECRET: "f3767129c8749f1f90d480b3badb7af1",
    GOOGLE_CLIENT_ID:
      "416911107346-dubt2srk5oe2r2u4st583on6n9us92kc.apps.googleusercontent.com",
    GOOGLE_CLIENT_SECRET: "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",
  },
  reactStrictMode: true,
  swcMinify: false,
};

module.exports = nextConfig;
